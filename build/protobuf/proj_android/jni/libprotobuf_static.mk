LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libprotobuf_static
LOCAL_MODULE_FILENAME := libprotobuf_static
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -D__ANDROID__

LOCAL_CFLAGS += -Wall
LOCAL_CFLAGS += -Wno-enum-compare-switch
LOCAL_CFLAGS += -Wno-unknown-warning-option

LOCAL_CFLAGS += -DHAVE_PTHREAD=1
LOCAL_CFLAGS += -DHAVE_ZLIB=1
LOCAL_CFLAGS += -Wno-sign-compare
LOCAL_CFLAGS += -pthread

LOCAL_CXXFLAGS += -frtti
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/protobuf/src

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/zlib
########################################################################
LOCAL_SRC_FILES  += ../../../../src/protobuf/src/google/protobuf/compiler/importer.cc
LOCAL_SRC_FILES  += ../../../../src/protobuf/src/google/protobuf/compiler/parser.cc
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/protobuf/src
# MY_SOURCES_PATH += $(LOCAL_PATH)/android

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/compiler/%
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/testdata/%
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/testing/%

MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/any_test.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/arena_test_util.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/arena_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/arenastring_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/descriptor_database_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/descriptor_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/drop_unknown_fields_test.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/dynamic_message_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/extension_set_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/generated_message_reflection_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/lite_arena_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/lite_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/map_field_test.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/map_lite_test_util.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/map_test.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/map_test_util.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/message_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/no_field_presence_test.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/preserve_unknown_enum_test.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/proto3_arena_lite_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/proto3_arena_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/proto3_lite_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/reflection_ops_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/repeated_field_reflection_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/repeated_field_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/test_util.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/test_util_lite.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/text_format_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/unknown_field_set_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/well_known_types_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/wire_format_unittest.cc

MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/arenastring.cc

MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/io/coded_stream_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/io/gzip_stream_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/io/printer_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/io/tokenizer_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/io/zero_copy_stream_unittest.cc

MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/stubs/atomicops_internals_x86_msvc.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/stubs/bytestream_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/stubs/common_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/stubs/int128_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/stubs/once_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/stubs/status_test.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/stubs/statusor_test.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/stubs/stringpiece_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/stubs/stringprintf_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/stubs/structurally_valid_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/stubs/strutil_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/stubs/template_util_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/stubs/time_test.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/stubs/type_traits_unittest.cc

MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/util/internal/testdata%
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/util/internal/default_value_objectwriter_test.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/util/internal/json_objectwriter_test.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/util/internal/json_stream_parser_test.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/util/internal/protostream_objectsource_test.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/util/internal/protostream_objectwriter_test.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/util/internal/type_info_test_helper.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/util/delimited_message_util_test.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/util/field_comparator_test.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/util/field_mask_util_test.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/util/message_differencer_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/util/time_util_test.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/util/type_resolver_util_test.cc
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/util/json_util_test.cc

MY_SOURCES_FILTER_OUT += ../../../../src/protobuf/src/google/protobuf/util/internal/error_listener.cc

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################