LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libzzip_shared
LOCAL_MODULE_FILENAME := libzzip_shared
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -D__ANDROID__

LOCAL_CFLAGS += -Wall
########################################################################
LOCAL_LDLIBS += -fPIC
########################################################################
LOCAL_SHARED_LIBRARIES += libz_shared
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/zziplib
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/zziplib/zzip

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/zlib
########################################################################
LOCAL_SRC_FILES  := ../../../../src/zziplib/zzip/dir.c
LOCAL_SRC_FILES  += ../../../../src/zziplib/zzip/err.c
LOCAL_SRC_FILES  += ../../../../src/zziplib/zzip/file.c
LOCAL_SRC_FILES  += ../../../../src/zziplib/zzip/info.c
LOCAL_SRC_FILES  += ../../../../src/zziplib/zzip/plugin.c
LOCAL_SRC_FILES  += ../../../../src/zziplib/zzip/stat.c
LOCAL_SRC_FILES  += ../../../../src/zziplib/zzip/zip.c
LOCAL_SRC_FILES  += ../../../../src/zziplib/zzip/fetch.c
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/android

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################