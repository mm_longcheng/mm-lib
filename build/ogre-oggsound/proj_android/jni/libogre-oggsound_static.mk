LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libogre-oggsound_static
LOCAL_MODULE_FILENAME := libogre-oggsound_static
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -D__ANDROID__

LOCAL_CFLAGS += -Wall
LOCAL_CFLAGS += -Wno-undefined-var-template

LOCAL_CFLAGS += -DOGGSOUND_EXPORT
LOCAL_CFLAGS += -DHAVE_EFX=0

LOCAL_CXXFLAGS += -fexceptions 
LOCAL_CXXFLAGS += -frtti
LOCAL_CXXFLAGS += -std=c++11
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/ogre-oggsound/include

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../Ogre/include/android 
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Ogre/OgreMain/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../build/OpenAL/include/android
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/OpenAL/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/OpenAL/include/AL
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/libogg/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../libogg/include/android
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/libvorbis/include
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/ogre-oggsound/src
# MY_SOURCES_PATH += $(LOCAL_PATH)/android

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################