LOCAL_PATH := $(call my-dir)

MM_MAKE_HOME ?= $(MM_HOME)/mm-make

include $(LOCAL_PATH)/lib_prebuild.mk
include $(CLEAR_VARS)

include $(LOCAL_PATH)/libogre-oggsound_static.mk
include $(LOCAL_PATH)/libogre-oggsound_shared.mk