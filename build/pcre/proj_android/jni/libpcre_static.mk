LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libpcre_static
LOCAL_MODULE_FILENAME := libpcre_static
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -D__ANDROID__

LOCAL_CFLAGS += -Wall
LOCAL_CFLAGS += -Wno-unknown-warning-option
LOCAL_CFLAGS += -Wno-maybe-uninitialized
LOCAL_CFLAGS += -Wno-maybe-uninitialized
LOCAL_CFLAGS += -Wno-unused-variable
LOCAL_CFLAGS += -DHAVE_CONFIG_H
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/pcre
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/android
LOCAL_C_INCLUDES += $(LOCAL_PATH)/src
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/pcre
MY_SOURCES_PATH += $(LOCAL_PATH)/../../source/android
# MY_SOURCES_PATH += $(LOCAL_PATH)/android

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/pcre/sljit%
MY_SOURCES_FILTER_OUT += ../../../../src/pcre/pcre16%
MY_SOURCES_FILTER_OUT += ../../../../src/pcre/pcre32%
MY_SOURCES_FILTER_OUT += ../../../../src/pcre/pcre_scanner_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/pcre/pcre_stringpiece_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/pcre/pcrecpp_unittest.cc
MY_SOURCES_FILTER_OUT += ../../../../src/pcre/pcretest.c
MY_SOURCES_FILTER_OUT += ../../../../src/pcre/pcre_jit_test.c
MY_SOURCES_FILTER_OUT += ../../../../src/pcre/pcredemo.c
MY_SOURCES_FILTER_OUT += ../../../../src/pcre/pcregrep.c
MY_SOURCES_FILTER_OUT += ../../../../src/pcre/dftables.c
MY_SOURCES_FILTER_OUT += ../../../../src/pcre/pcre_printint.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################