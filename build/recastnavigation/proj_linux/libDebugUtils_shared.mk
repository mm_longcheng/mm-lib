LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)
########################################################################
LOCAL_MODULE := libDebugUtils_shared
LOCAL_MODULE_FILENAME := libDebugUtils
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall
########################################################################
LOCAL_LDLIBS += -fPIC
########################################################################
LOCAL_SHARED_LIBRARIES += libDetour_shared
LOCAL_SHARED_LIBRARIES += libRecast_shared
LOCAL_SHARED_LIBRARIES += libDetourTileCache_shared
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/recastnavigation/DebugUtils/Include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/recastnavigation/Detour/Include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/recastnavigation/Recast/Include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/recastnavigation/DetourTileCache/Include
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/recastnavigation/DebugUtils
MY_SOURCES_PATH += $(LOCAL_PATH)/$(TARGET_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
include $(SOURCE_RWILDCARD)
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################
# $(modules-dump-database)
