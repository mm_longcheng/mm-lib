APP_ABI := arm64-v8a armeabi-v7a # armeabi

APP_STL := gnustl_shared

APP_MODULES += libDebugUtils_static
APP_MODULES += libDebugUtils_shared

APP_MODULES += libDetour_static
APP_MODULES += libDetour_shared

APP_MODULES += libDetourCrowd_static
APP_MODULES += libDetourCrowd_shared

APP_MODULES += libDetourTileCache_static
APP_MODULES += libDetourTileCache_shared

APP_MODULES += libRecast_shared
APP_MODULES += libRecast_static