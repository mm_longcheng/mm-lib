LOCAL_PATH := $(call my-dir)

MM_MAKE_HOME ?= $(MM_HOME)/mm-make

include $(CLEAR_VARS)

include $(LOCAL_PATH)/libDebugUtils_static.mk
include $(LOCAL_PATH)/libDebugUtils_shared.mk

include $(LOCAL_PATH)/libDetour_static.mk
include $(LOCAL_PATH)/libDetour_shared.mk

include $(LOCAL_PATH)/libDetourCrowd_static.mk
include $(LOCAL_PATH)/libDetourCrowd_shared.mk

include $(LOCAL_PATH)/libDetourTileCache_static.mk
include $(LOCAL_PATH)/libDetourTileCache_shared.mk

include $(LOCAL_PATH)/libRecast_shared.mk
include $(LOCAL_PATH)/libRecast_static.mk