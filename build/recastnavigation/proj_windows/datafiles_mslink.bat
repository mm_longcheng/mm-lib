@echo off 

:: mkdir datafiles
IF NOT EXIST "bin/Debug"   MD "bin/Debug"
IF NOT EXIST "bin/Release" MD "bin/Release"

:: ln -s
call:ln-s /j "bin/Debug/Meshes" "../../../src/recastnavigation/RecastDemo/Bin/Meshes"
call:ln-s /j "bin/Debug/TestCases" "../../../src/recastnavigation/RecastDemo/Bin/TestCases"
call:ln-s /h "bin/Debug/DroidSans.ttf" "../../../src/recastnavigation/RecastDemo/Bin/DroidSans.ttf"

call:ln-s /j "bin/Release/Meshes" "../../../src/recastnavigation/RecastDemo/Bin/Meshes"
call:ln-s /j "bin/Release/TestCases" "../../../src/recastnavigation/RecastDemo/Bin/TestCases"
call:ln-s /h "bin/Release/DroidSans.ttf" "../../../src/recastnavigation/RecastDemo/Bin/DroidSans.ttf"

GOTO:EOF

:: ln-s mode target source
:ln-s  
IF NOT EXIST %~2 mklink %~1 "%~2" "%~3"
GOTO:EOF
