LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)
########################################################################
LOCAL_MODULE := x264_executable
LOCAL_MODULE_FILENAME := x264
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -std=gnu99
LOCAL_CFLAGS += -D_GNU_SOURCE
LOCAL_CFLAGS += -DARCH_X86_32=0
LOCAL_CFLAGS += -DARCH_X86_64=1
LOCAL_CFLAGS += -DHAVE_STRING_H
LOCAL_CFLAGS += -Wno-maybe-uninitialized
LOCAL_CFLAGS += -Wshadow
LOCAL_CFLAGS += -O3
LOCAL_CFLAGS += -ffast-math
LOCAL_CFLAGS += -m64
LOCAL_CFLAGS += -mpreferred-stack-boundary=6
LOCAL_CFLAGS += -fomit-frame-pointer
LOCAL_CFLAGS += -fno-tree-vectorize

LOCAL_ASMFLAGS += -DARCH_X86_32=0
LOCAL_ASMFLAGS += -DARCH_X86_64=1
# can not define PREFIX.
# LOCAL_ASMFLAGS += -DPREFIX 
########################################################################
LOCAL_LDLIBS += -ldl
LOCAL_LDLIBS += -lpthread
########################################################################
LOCAL_SHARED_LIBRARIES += 
LOCAL_STATIC_LIBRARIES += x264_static
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../include/$(TARGET_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../source/$(TARGET_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/x264
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/x264/encoder
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/x264/common/x86/
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/x264/common/
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/x264/filters
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/x264/filters/video
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/x264/input
########################################################################
#  
# config self source file path ,suffix.
MY_SOURCES_PATH := $(LOCAL_PATH)/../../../src/x264
MY_SOURCES_PATH += $(LOCAL_PATH)/../source/$(TARGET_PLATFORM)
MY_SOURCES_PATH += $(LOCAL_PATH)/$(TARGET_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c

MY_SOURCES_FILTER_OUT := ../../../src/x264/common/%
MY_SOURCES_FILTER_OUT += ../../../src/x264/common/%

MY_SOURCES_FILTER_OUT += ../../../src/x264/encoder/%
MY_SOURCES_FILTER_OUT += ../../../src/x264/encoder/%

MY_SOURCES_FILTER_OUT += ../../../src/x264/tools/%

MY_SOURCES_FILTER_OUT += ../source/$(TARGET_PLATFORM)/bit_depth_08/common/%
MY_SOURCES_FILTER_OUT += ../source/$(TARGET_PLATFORM)/bit_depth_10/common/%

MY_SOURCES_FILTER_OUT += ../source/$(TARGET_PLATFORM)/bit_depth_08/encoder/%
MY_SOURCES_FILTER_OUT += ../source/$(TARGET_PLATFORM)/bit_depth_10/encoder/%

MY_SOURCES_FILTER_OUT += ../../../src/x264/x264dll.c
MY_SOURCES_FILTER_OUT += ../../../src/x264/example.c

MY_SOURCES_FILTER_OUT += ../../../src/x264/output/mp4.c
MY_SOURCES_FILTER_OUT += ../../../src/x264/output/mp4_lsmash.c

MY_SOURCES_FILTER_OUT += ../../../src/x264/filters/video/depth.c
MY_SOURCES_FILTER_OUT += ../../../src/x264/filters/video/cache.c

MY_SOURCES_FILTER_OUT += ../../../src/x264/input/thread.c
MY_SOURCES_FILTER_OUT += ../../../src/x264/input/ffms.c
MY_SOURCES_FILTER_OUT += ../../../src/x264/input/lavf.c

MY_SOURCES_EXTENSION := .cpp .c .cc .S .asm
include $(SOURCE_RWILDCARD)
########################################################################
include $(BUILD_EXECUTABLE)
########################################################################
# $(modules-dump-database)
