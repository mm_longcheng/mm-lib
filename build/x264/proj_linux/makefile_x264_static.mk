LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)
########################################################################
LOCAL_MODULE := x264_static
LOCAL_MODULE_FILENAME := libx264
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -std=gnu99
LOCAL_CFLAGS += -D_GNU_SOURCE
LOCAL_CFLAGS += -DARCH_X86_32=0
LOCAL_CFLAGS += -DARCH_X86_64=1
LOCAL_CFLAGS += -DHAVE_STRING_H
LOCAL_CFLAGS += -Wno-maybe-uninitialized
LOCAL_CFLAGS += -Wshadow
LOCAL_CFLAGS += -O3
LOCAL_CFLAGS += -ffast-math
LOCAL_CFLAGS += -m64
LOCAL_CFLAGS += -mpreferred-stack-boundary=6
LOCAL_CFLAGS += -fomit-frame-pointer
LOCAL_CFLAGS += -fno-tree-vectorize

LOCAL_ASMFLAGS += -DARCH_X86_32=0
LOCAL_ASMFLAGS += -DARCH_X86_64=1
# can not define PREFIX.
# LOCAL_ASMFLAGS += -DPREFIX 
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../include/$(TARGET_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../source/$(TARGET_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/x264
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/x264/encoder
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/x264/common/x86/
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/x264/common/
########################################################################
# source
wildcard_source := $(call rwildcard,$(LOCAL_PATH)/../source/$(TARGET_PLATFORM)/bit_depth_08/,*.asm)
local_bit_depth_08_asm_source := $(wildcard_source:$(LOCAL_PATH)/%=%)

wildcard_source := $(call rwildcard,$(LOCAL_PATH)/../source/$(TARGET_PLATFORM)/bit_depth_10/,*.asm)
local_bit_depth_10_asm_source := $(wildcard_source:$(LOCAL_PATH)/%=%)

wildcard_source := $(call rwildcard,$(LOCAL_PATH)/../source/$(TARGET_PLATFORM)/bit_depth_08/,*.c)
local_bit_depth_08_c_source := $(wildcard_source:$(LOCAL_PATH)/%=%)

wildcard_source := $(call rwildcard,$(LOCAL_PATH)/../source/$(TARGET_PLATFORM)/bit_depth_10/,*.c)
local_bit_depth_10_c_source := $(wildcard_source:$(LOCAL_PATH)/%=%)

local_asm_cflags := -DSTACK_ALIGNMENT=64
local_asm_cflags += 

local_bit_depth_08_asm_cflags := -DBIT_DEPTH=8
local_bit_depth_08_asm_cflags += -Dprivate_prefix=x264_8
local_bit_depth_08_asm_cflags += -DHIGH_BIT_DEPTH=0
local_bit_depth_08_asm_cflags += $(local_asm_cflags)

local_bit_depth_10_asm_cflags := -DBIT_DEPTH=10
local_bit_depth_10_asm_cflags += -Dprivate_prefix=x264_10
local_bit_depth_10_asm_cflags += -DHIGH_BIT_DEPTH=1
local_bit_depth_10_asm_cflags += $(local_asm_cflags)

local_bit_depth_08_c_cflags := -DBIT_DEPTH=8
local_bit_depth_08_c_cflags += -DHIGH_BIT_DEPTH=0

local_bit_depth_10_c_cflags := -DBIT_DEPTH=10
local_bit_depth_10_c_cflags += -DHIGH_BIT_DEPTH=1

TARGET-process-src-files-tags += $(call set-src-files-target-cflags, $(local_bit_depth_08_asm_source), $(local_bit_depth_08_asm_cflags))
TARGET-process-src-files-tags += $(call set-src-files-target-cflags, $(local_bit_depth_10_asm_source), $(local_bit_depth_10_asm_cflags))

TARGET-process-src-files-tags += $(call set-src-files-target-cflags, $(local_bit_depth_08_c_source), $(local_bit_depth_08_c_cflags))
TARGET-process-src-files-tags += $(call set-src-files-target-cflags, $(local_bit_depth_10_c_source), $(local_bit_depth_10_c_cflags))

# $(info $(local_bit_depth_08_asm_source))
# $(info $(local_bit_depth_10_asm_source))
# $(info $(local_bit_depth_08_c_source))
# $(info $(local_bit_depth_10_c_source))

LOCAL_SRC_FILES  += ../../../src/x264/common/x86/cpu-a.asm

LOCAL_SRC_FILES  += ../../../src/x264/common/base.c
LOCAL_SRC_FILES  += ../../../src/x264/common/cpu.c
LOCAL_SRC_FILES  += ../../../src/x264/common/osdep.c
LOCAL_SRC_FILES  += ../../../src/x264/common/tables.c

LOCAL_SRC_FILES  += ../../../src/x264/encoder/api.c

LOCAL_SRC_FILES  += ../../../src/x264/extras/getopt.c
########################################################################
#  
# config self source file path ,suffix.
MY_SOURCES_PATH := $(LOCAL_PATH)/../source/$(TARGET_PLATFORM)
MY_SOURCES_PATH += $(LOCAL_PATH)/$(TARGET_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c

MY_SOURCES_FILTER_OUT := ../source/$(TARGET_PLATFORM)/bit_depth_08/common/x86/dct-32.asm
MY_SOURCES_FILTER_OUT += ../source/$(TARGET_PLATFORM)/bit_depth_10/common/x86/dct-32.asm

MY_SOURCES_FILTER_OUT += ../source/$(TARGET_PLATFORM)/bit_depth_08/common/x86/pixel-32.asm
MY_SOURCES_FILTER_OUT += ../source/$(TARGET_PLATFORM)/bit_depth_10/common/x86/pixel-32.asm

MY_SOURCES_FILTER_OUT += ../source/$(TARGET_PLATFORM)/bit_depth_08/filters/%
MY_SOURCES_FILTER_OUT += ../source/$(TARGET_PLATFORM)/bit_depth_10/filters/%

MY_SOURCES_FILTER_OUT += ../source/$(TARGET_PLATFORM)/bit_depth_08/input/%
MY_SOURCES_FILTER_OUT += ../source/$(TARGET_PLATFORM)/bit_depth_10/input/%

MY_SOURCES_EXTENSION := .cpp .c .cc .S .asm
include $(SOURCE_RWILDCARD)
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################
# $(modules-dump-database)
