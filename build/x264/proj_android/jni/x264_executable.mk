LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)
########################################################################
LOCAL_MODULE := x264_executable
LOCAL_MODULE_FILENAME := x264
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -D__ANDROID__

LOCAL_CFLAGS += -Wall
LOCAL_CFLAGS += -DHAVE_STRING_H
########################################################################
LOCAL_LDLIBS += -fPIC
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += libx264_static
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/android
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../source/android
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/x264
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/x264/encoder
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/x264/common
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/x264/filters
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/x264/filters/video
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/x264/input
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/x264
MY_SOURCES_PATH += $(LOCAL_PATH)/../../source/android
MY_SOURCES_PATH += $(LOCAL_PATH)/android

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT := ../../../../src/x264/common/%
MY_SOURCES_FILTER_OUT += ../../../../src/x264/common/%

MY_SOURCES_FILTER_OUT += ../../../../src/x264/encoder/%
MY_SOURCES_FILTER_OUT += ../../../../src/x264/encoder/%

MY_SOURCES_FILTER_OUT += ../../../../src/x264/tools/%

MY_SOURCES_FILTER_OUT += ../../source/android/bit_depth_08/common/%
MY_SOURCES_FILTER_OUT += ../../source/android/bit_depth_10/common/%

MY_SOURCES_FILTER_OUT += ../../source/android/bit_depth_08/encoder/%
MY_SOURCES_FILTER_OUT += ../../source/android/bit_depth_10/encoder/%

MY_SOURCES_FILTER_OUT += ../../../../src/x264/x264dll.c
MY_SOURCES_FILTER_OUT += ../../../../src/x264/example.c

MY_SOURCES_FILTER_OUT += ../../../../src/x264/output/mp4.c
MY_SOURCES_FILTER_OUT += ../../../../src/x264/output/mp4_lsmash.c

MY_SOURCES_FILTER_OUT += ../../../../src/x264/filters/video/depth.c
MY_SOURCES_FILTER_OUT += ../../../../src/x264/filters/video/cache.c

MY_SOURCES_FILTER_OUT += ../../../../src/x264/input/thread.c
MY_SOURCES_FILTER_OUT += ../../../../src/x264/input/ffms.c
MY_SOURCES_FILTER_OUT += ../../../../src/x264/input/lavf.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_EXECUTABLE)
########################################################################
