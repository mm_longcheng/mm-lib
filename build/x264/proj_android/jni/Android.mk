LOCAL_PATH := $(call my-dir)

MM_MAKE_HOME ?= $(MM_HOME)/mm-make

include $(CLEAR_VARS)

include $(LOCAL_PATH)/libx264_static.mk
include $(LOCAL_PATH)/libx264_shared.mk
include $(LOCAL_PATH)/x264_executable.mk