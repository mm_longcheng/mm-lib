LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libx264_static
LOCAL_MODULE_FILENAME := libx264_static
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -D__ANDROID__

LOCAL_CFLAGS += -Wall
LOCAL_CFLAGS += -DHAVE_STRING_H
LOCAL_CFLAGS += -D__USE_FILE_OFFSET64
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/android
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../source/android
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/x264
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/x264/encoder
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/x264/common
########################################################################
rwildcard=$(wildcard $1$2) $(foreach d,$(wildcard $1*),$(call rwildcard,$d/,$2))

# source
wildcard_source := $(call rwildcard,$(LOCAL_PATH)/../../source/android/bit_depth_08/,*.S)
local_bit_depth_08_asm_source := $(wildcard_source:$(LOCAL_PATH)/%=%)

wildcard_source := $(call rwildcard,$(LOCAL_PATH)/../../source/android/bit_depth_10/,*.S)
local_bit_depth_10_asm_source := $(wildcard_source:$(LOCAL_PATH)/%=%)

wildcard_source := $(call rwildcard,$(LOCAL_PATH)/../../source/android/bit_depth_08/,*.c)
local_bit_depth_08_c_source := $(wildcard_source:$(LOCAL_PATH)/%=%)

wildcard_source := $(call rwildcard,$(LOCAL_PATH)/../../source/android/bit_depth_10/,*.c)
local_bit_depth_10_c_source := $(wildcard_source:$(LOCAL_PATH)/%=%)

local_asm_cflags := -mfpu=neon
local_asm_cflags += -DPIC

local_bit_depth_08_asm_cflags := -DBIT_DEPTH=8
local_bit_depth_08_asm_cflags += -Dprivate_prefix=x264_8
local_bit_depth_08_asm_cflags += -DHIGH_BIT_DEPTH=0
local_bit_depth_08_asm_cflags += $(local_asm_cflags)

local_bit_depth_10_asm_cflags := -DBIT_DEPTH=10
local_bit_depth_10_asm_cflags += -Dprivate_prefix=x264_10
local_bit_depth_10_asm_cflags += -DHIGH_BIT_DEPTH=1
local_bit_depth_10_asm_cflags += $(local_asm_cflags)

local_bit_depth_08_c_cflags := -DBIT_DEPTH=8
local_bit_depth_08_c_cflags += -DHIGH_BIT_DEPTH=0

local_bit_depth_10_c_cflags := -DBIT_DEPTH=10
local_bit_depth_10_c_cflags += -DHIGH_BIT_DEPTH=1

TARGET-process-src-files-tags += $(call set-src-files-target-cflags, $(local_bit_depth_08_asm_source), $(local_bit_depth_08_asm_cflags))
TARGET-process-src-files-tags += $(call set-src-files-target-cflags, $(local_bit_depth_10_asm_source), $(local_bit_depth_10_asm_cflags))

TARGET-process-src-files-tags += $(call set-src-files-target-cflags, $(local_bit_depth_08_c_source), $(local_bit_depth_08_c_cflags))
TARGET-process-src-files-tags += $(call set-src-files-target-cflags, $(local_bit_depth_10_c_source), $(local_bit_depth_10_c_cflags))

# $(info $(local_bit_depth_08_asm_source))
# $(info $(local_bit_depth_10_asm_source))
# $(info $(local_bit_depth_08_c_source))
# $(info $(local_bit_depth_10_c_source))

ifeq ($(TARGET_ARCH_ABI),arm64-v8a)
	LOCAL_SRC_FILES  += 
else
	LOCAL_SRC_FILES  += ../../../../src/x264/common/arm/cpu-a.S
endif

LOCAL_SRC_FILES  += ../../../../src/x264/common/base.c
LOCAL_SRC_FILES  += ../../../../src/x264/common/cpu.c
LOCAL_SRC_FILES  += ../../../../src/x264/common/osdep.c
LOCAL_SRC_FILES  += ../../../../src/x264/common/tables.c

LOCAL_SRC_FILES  += ../../../../src/x264/encoder/api.c

LOCAL_SRC_FILES  += ../../../../src/x264/extras/getopt.c
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../source/android
# MY_SOURCES_PATH += $(LOCAL_PATH)/android

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
ifeq ($(TARGET_ARCH_ABI),arm64-v8a)
	MY_SOURCES_FILTER_OUT += ../../source/android/bit_depth_08/common/arm%
	MY_SOURCES_FILTER_OUT += ../../source/android/bit_depth_10/common/arm%
else
	MY_SOURCES_FILTER_OUT += ../../source/android/bit_depth_08/common/aarch64%
	MY_SOURCES_FILTER_OUT += ../../source/android/bit_depth_10/common/aarch64%
endif

MY_SOURCES_FILTER_OUT += ../../source/android/bit_depth_08/filters/%
MY_SOURCES_FILTER_OUT += ../../source/android/bit_depth_10/filters/%

MY_SOURCES_FILTER_OUT += ../../source/android/bit_depth_08/input/%
MY_SOURCES_FILTER_OUT += ../../source/android/bit_depth_10/input/%

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################