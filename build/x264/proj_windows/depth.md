asm
ARCH_X86_32=1
ARCH_X86_64=0
PREFIX

c
_CRT_SECURE_NO_WARNINGS
ARCH_X86_32=1
ARCH_X86_64=0
HAVE_STRING_H
/////////////////////////////////////////////////
栈对齐 linux ARCH_X86_64 可以选择使用 64 对齐,提升运行效率,windows上不能超过 8 对齐,否则将可能出现会崩溃.
ARCH_X86_32=1
ARCH_X86_64=0
#define STACK_ALIGNMENT 8

ARCH_X86_32=0
ARCH_X86_64=1
#define STACK_ALIGNMENT 64
/////////////////////////////////////////////////
asm
DEPTH 08
BIT_DEPTH=8
private_prefix=x264_8

DEPTH 10
BIT_DEPTH=10
private_prefix=x264_10

c
DEPTH 08
BIT_DEPTH=8
HIGH_BIT_DEPTH=0

DEPTH 10
BIT_DEPTH=10
HIGH_BIT_DEPTH=1
/////////////////////////////////////////////////
gcc -Wno-maybe-uninitialized -Wshadow -O3 -ffast-math -m64  -Wall -I. -I. -std=gnu99 -D_GNU_SOURCE -mpreferred-stack-boundary=6  -I/usr/local/include    -I/usr/local/include   -fomit-frame-pointer -fno-tree-vectorize -c common/set.c -o common/set-10.o -DHIGH_BIT_DEPTH=1 -DBIT_DEPTH=10
gcc -Wno-maybe-uninitialized -Wshadow -O3 -ffast-math -m64  -Wall -I. -I. -std=gnu99 -D_GNU_SOURCE -mpreferred-stack-boundary=6  -I/usr/local/include    -I/usr/local/include   -fomit-frame-pointer -fno-tree-vectorize -c common/set.c -o common/set-8.o -DHIGH_BIT_DEPTH=0 -DBIT_DEPTH=8

nasm -I. -I. -DARCH_X86_64=1 -I./common/x86/ -f elf64 -DSTACK_ALIGNMENT=64 -o common/x86/cpu-a.o common/x86/cpu-a.asm

nasm -I. -I. -DARCH_X86_64=1 -I./common/x86/ -f elf64 -DSTACK_ALIGNMENT=64 -o common/x86/dct-64-8.o common/x86/dct-64.asm -DBIT_DEPTH=8 -Dprivate_prefix=x264_8
nasm -I. -I. -DARCH_X86_64=1 -I./common/x86/ -f elf64 -DSTACK_ALIGNMENT=64 -o common/x86/dct-64-10.o common/x86/dct-64.asm -DBIT_DEPTH=10 -Dprivate_prefix=x264_10

nasm -o E:\mm\mm-lib\build\x264\proj_windows\obj\libx264\Debug\source\bit_depth_08\common\x86\dct-64.obj -f win32 -I../../../src/x264/common/x86/ -DBIT_DEPTH=8 -DHIGH_BIT_DEPTH=0 -DSTACK_ALIGNMENT=64 -DARCH_X86_32=1 -DARCH_X86_64=0 -DPREFIX  E:\mm\mm-lib\build\x264\source\windows\bit_depth_08\common\x86\dct-64.asm


gcc -Wno-maybe-uninitialized -Wshadow -O3 -ffast-math -m64  -Wall -I. -I. -std=gnu99 -D_GNU_SOURCE -mpreferred-stack-boundary=6  -I/usr/local/include    -I/usr/local/include   -fomit-frame-pointer -fno-tree-vectorize -c common/x86/predict-c.c -o common/x86/predict-c-8.o -DHIGH_BIT_DEPTH=0 -DBIT_DEPTH=8
gcc -Wno-maybe-uninitialized -Wshadow -O3 -ffast-math -m64  -Wall -I. -I. -std=gnu99 -D_GNU_SOURCE -mpreferred-stack-boundary=6  -I/usr/local/include    -I/usr/local/include   -fomit-frame-pointer -fno-tree-vectorize -c common/x86/predict-c.c -o common/x86/predict-c-10.o -DHIGH_BIT_DEPTH=1 -DBIT_DEPTH=10

gcc -MMD -MP -MF obj/x264_static/__/source/linux/bit_depth_08/common/x86/predict-c.o.d   -DBIT_DEPTH=8 -DHIGH_BIT_DEPTH=0 -I./../include/linux -I./../source/linux -I./../../../src/x264 -I./../../../src/x264/encoder -I./../../../src/x264/common -I./../../../src/x264/common/x86/ -I.     -std=gnu99 -D_GNU_SOURCE -DARCH_X86_32=0 -DARCH_X86_64=1 -DHAVE_STRING_H -Wno-maybe-uninitialized -Wshadow -O3 -ffast-math -m64 -mpreferred-stack-boundary=6 -fomit-frame-pointer -fno-tree-vectorize  --sysroot /  -c  ./../source/linux/bit_depth_08/common/x86/predict-c.c -o obj/x264_static/__/source/linux/bit_depth_08/common/x86/predict-c.o

gcc -Wno-maybe-uninitialized -Wshadow -O3 -ffast-math -m64  -Wall -std=gnu99 -D_GNU_SOURCE -mpreferred-stack-boundary=6  -I./../include/linux -I./../source/linux -I./../../../src/x264 -I./../../../src/x264/encoder -I./../../../src/x264/common -I./../../../src/x264/common/x86/ -I. -fomit-frame-pointer -fno-tree-vectorize -c ./../source/linux/bit_depth_08/common/x86/predict-c.c -o obj/x264_static/__/source/linux/bit_depth_08/common/x86/predict-c.o -DHIGH_BIT_DEPTH=0 -DBIT_DEPTH=8

gcc -Wno-maybe-uninitialized -Wshadow -O3 -ffast-math -m64  -Wall -I. -I. -std=gnu99 -D_GNU_SOURCE -mpreferred-stack-boundary=6  -I/usr/local/include    -I/usr/local/include   -fomit-frame-pointer -fno-tree-vectorize -c common/opencl.c -o common/opencl-8.o -DHIGH_BIT_DEPTH=0 -DBIT_DEPTH=8

nasm -I./../include/linux -I./../source/linux -I./../../../src/x264 -I./../../../src/x264/encoder -I./../../../src/x264/common/x86/ -I./../../../src/x264/common/ -I.  -DBIT_DEPTH=8 -Dprivate_prefix=x264_8 -DHIGH_BIT_DEPTH=0 -DPIC=1 -DSTACK_ALIGNMENT=64 -DBIT_DEPTH=8 -Dprivate_prefix=x264_8 -DHIGH_BIT_DEPTH=0 -DPIC=1 -DSTACK_ALIGNMENT=64 -DARCH_X86_32=0 -DARCH_X86_64=1 -DPREFIX  -I //usr/include   -f elf64 ./../source/linux/bit_depth_08/common/x86/trellis-64.asm -o obj/x264_shared/__/source/linux/bit_depth_08/common/x86/trellis-64.o
/////////////////////////////////////////////////
win32thread.c

ARCH_X86_32=1
ARCH_X86_64=0
common/x86/dct-32.asm
common/x86/pixel-32-08.o

ARCH_X86_32=0
ARCH_X86_64=1
common/x86/dct-64.asm
common/x86/trellis-64.asm
/////////////////////////////////////////////////
sad16-a.obj : error LNK2005: _x264_pixel_sad_16x16_mmx2 已经在 sad-a.obj 中定义

pixel.obj : error LNK2019: 无法解析的外部符号 _x264_8_pixel_sad_16x16_mmx2，该符号在函数 _x264_8_pixel_init 中被引用
pixel.obj : error LNK2019: 无法解析的外部符号 _x264_10_pixel_sad_16x16_mmx2，该符号在函数 _x264_10_pixel_init 中被引用
private_prefix

mm-lib\build\x264\source\windows\bit_depth_10\common\quant.c(60): warning C4146
/////////////////////////////////////////////////
common/x86/cpu-a.o

common/base.o
common/cpu.o
common/osdep.o
common/tables.o

encoder/api.o

filters/video/crop.o
filters/video/fix_vfr_pts.o
filters/video/internal.o
filters/video/resize.o
filters/video/select_every.o
filters/video/source.o
filters/video/video.o

filters/filters.o

input/avs.o
input/input.o
input/lavf.o
input/raw.o
input/timecode.o
input/y4m.o

output/flv.o
output/flv_bytestream.o
output/matroska.o
output/matroska_ebml.o
output/raw.o

x264.o
/////////////////////////////////////////////////
common/x86/sad-a-08.o
/////////////////////////////////////////////////
common/x86/sad16-a-10.o
/////////////////////////////////////////////////
common/x86/bitstream-a-08.o
common/x86/bitstream-a-10.o

common/x86/cabac-a-08.o
common/x86/cabac-a-10.o

common/x86/const-a-08.o
common/x86/const-a-10.o

common/x86/dct-64-08.o
common/x86/dct-64-10.o

common/x86/dct-a-08.o
common/x86/dct-a-10.o

common/x86/deblock-a-08.o
common/x86/deblock-a-10.o

common/x86/mc-a2-08.o
common/x86/mc-a2-10.o

common/x86/mc-a-08.o
common/x86/mc-a-10.o

common/x86/mc-c-08.o
common/x86/mc-c-10.o

common/x86/pixel-a-08.o
common/x86/pixel-a-10.o

common/x86/predict-a-08.o
common/x86/predict-a-10.o

common/x86/predict-c-08.o
common/x86/predict-c-10.o

common/x86/quant-a-08.o
common/x86/quant-a-10.o

common/x86/trellis-64-08.o
common/x86/trellis-64-10.o

common/bitstream-08.o
common/bitstream-10.o

common/cabac-08.o
common/cabac-10.o

common/common-08.o
common/common-10.o

common/dct-08.o
common/dct-10.o

common/deblock-08.o
common/deblock-10.o

common/frame-08.o
common/frame-10.o

common/macroblock-08.o
common/macroblock-10.o

common/mc-08.o
common/mc-10.o

common/mvpred-08.o
common/mvpred-10.o

common/pixel-08.o
common/pixel-10.o

common/predict-08.o
common/predict-10.o

common/quant-08.o
common/quant-10.o

common/rectangle-08.o
common/rectangle-10.o

common/set-08.o
common/set-10.o

common/threadpool-08.o
common/threadpool-10.o

common/vlc-08.o
common/vlc-10.o

common/opencl-08.o
common/opencl-10.o

encoder/slicetype-cl-08.o
encoder/slicetype-cl-10.o

encoder/analyse-08.o
encoder/analyse-10.o

encoder/cabac-08.o
encoder/cabac-10.o

encoder/cavlc-08.o
encoder/cavlc-10.o

encoder/encoder-08.o
encoder/encoder-10.o

encoder/lookahead-08.o
encoder/lookahead-10.o

encoder/macroblock-08.o
encoder/macroblock-10.o

encoder/me-08.o
encoder/me-10.o

encoder/ratecontrol-08.o
encoder/ratecontrol-10.o

encoder/set-08.o
encoder/set-10.o

filters/video/cache-08.o
filters/video/cache-10.o

filters/video/depth-08.o
filters/video/depth-10.o

input/thread-08.o
input/thread-10.o
/////////////////////////////////////////////////
EXTERN_PREFIX="_" AR="ar" NM="nm -g" makedef libx264.ver common/osdep.o common/base.o common/cpu.o common/tables.o encoder/api.o common/win32thread.o common/mc-8.o common/predict-8.o common/pixel-8.o common/macroblock-8.o common/frame-8.o common/dct-8.o common/cabac-8.o common/common-8.o common/rectangle-8.o common/set-8.o common/quant-8.o common/deblock-8.o common/vlc-8.o common/mvpred-8.o common/bitstream-8.o encoder/analyse-8.o encoder/me-8.o encoder/ratecontrol-8.o encoder/set-8.o encoder/macroblock-8.o encoder/cabac-8.o encoder/cavlc-8.o encoder/encoder-8.o encoder/lookahead-8.o common/threadpool-8.o common/x86/mc-c-8.o common/x86/predict-c-8.o common/opencl-8.o encoder/slicetype-cl-8.o common/mc-10.o common/predict-10.o common/pixel-10.o common/macroblock-10.o common/frame-10.o common/dct-10.o common/cabac-10.o common/common-10.o common/rectangle-10.o common/set-10.o common/quant-10.o common/deblock-10.o common/vlc-10.o common/mvpred-10.o common/bitstream-10.o encoder/analyse-10.o encoder/me-10.o encoder/ratecontrol-10.o encoder/set-10.o encoder/macroblock-10.o encoder/cabac-10.o encoder/cavlc-10.o encoder/encoder-10.o encoder/lookahead-10.o common/threadpool-10.o common/x86/mc-c-10.o common/x86/predict-c-10.o  common/x86/cpu-a.o common/x86/dct-32-8.o common/x86/pixel-32-8.o common/x86/bitstream-a-8.o common/x86/const-a-8.o common/x86/cabac-a-8.o common/x86/dct-a-8.o common/x86/deblock-a-8.o common/x86/mc-a-8.o common/x86/mc-a2-8.o common/x86/pixel-a-8.o common/x86/predict-a-8.o common/x86/quant-a-8.o common/x86/sad-a-8.o common/x86/dct-32-10.o common/x86/pixel-32-10.o common/x86/bitstream-a-10.o common/x86/const-a-10.o common/x86/cabac-a-10.o common/x86/dct-a-10.o common/x86/deblock-a-10.o common/x86/mc-a-10.o common/x86/mc-a2-10.o common/x86/pixel-a-10.o common/x86/predict-a-10.o common/x86/quant-a-10.o common/x86/sad16-a-10.o > libx264.def