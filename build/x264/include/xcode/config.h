///////////////////////////////////////
// ABI Macro definition.
#if (defined __arm__) || (defined __aarch64__)
#   define ARCH_HAVE_MMX 0
#	ifdef __arm__
#		define ARCH_ARM_32 1
#		define ARCH_ARM_64 0
#		define ARCH_X86_32 0
#		define ARCH_X86_64 0
#	else
#		define ARCH_ARM_32 0
#		define ARCH_ARM_64 1
#		define ARCH_X86_32 0
#		define ARCH_X86_64 0
#	endif
#elif (defined __i386__) || (defined __x86_64__)
#   define ARCH_HAVE_MMX 1
#	ifdef __i386__
#		define ARCH_ARM_32 0
#		define ARCH_ARM_64 0
#		define ARCH_X86_32 1
#		define ARCH_X86_64 0
#	else
#		define ARCH_ARM_32 0
#		define ARCH_ARM_64 0
#		define ARCH_X86_32 0
#		define ARCH_X86_64 1
#	endif
#else
#   pragma error "No known ABI type. Abort! Abort!"
#endif
///////////////////////////////////////
#define HAVE_MALLOC_H 0
#define HAVE_X86_INLINE_ASM 0
#define HAVE_MMX     ARCH_HAVE_MMX
#define ARCH_ARM     ARCH_ARM_32
#define ARCH_AARCH64 ARCH_ARM_64
#define SYS_FREEBSD 1
#define STACK_ALIGNMENT 8
#define HAVE_POSIXTHREAD 1
#define HAVE_CPU_COUNT 0
#define HAVE_THREAD 1
#define HAVE_LOG2F 1
#define HAVE_STRTOK_R 1
#define HAVE_MMAP 1
#define HAVE_THP 0
#define HAVE_SWSCALE 0
#define HAVE_LAVF 0
#define HAVE_AVS 1
#define USE_AVXSYNTH 1
#define HAVE_VECTOREXT 0
#define fseek fseeko
#define ftell ftello
#define HAVE_BITDEPTH8 1
#define HAVE_BITDEPTH10 1
#define HAVE_GPL 1
#define HAVE_INTERLACED 1
#define HAVE_OPENCL 1
#define HAVE_ALTIVEC 0
#define HAVE_ALTIVEC_H 0
#define HAVE_ARMV6   ARCH_ARM_32
#define HAVE_ARMV6T2 ARCH_ARM_32
#define HAVE_NEON 0
#define HAVE_BEOSTHREAD 0
#define HAVE_WIN32THREAD 0
#define HAVE_FFMS 0
#define HAVE_GPAC 0
#define HAVE_LSMASH 0
#define HAVE_AS_FUNC 0
#define HAVE_INTEL_DISPATCHER 0
#define HAVE_MSA 0
#define HAVE_WINRT 0
#define HAVE_VSX 0
#define HAVE_ARM_INLINE_ASM 0
