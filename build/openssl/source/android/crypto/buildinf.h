/*
 * WARNING: do not edit!
 * Generated by util/mkbuildinf.pl
 *
 * Copyright 2014-2017 The OpenSSL Project Authors. All Rights Reserved.
 *
 * Licensed under the OpenSSL license (the "License").  You may not use
 * this file except in compliance with the License.  You can obtain a copy
 * in the file LICENSE in the source distribution or at
 * https://www.openssl.org/source/license.html
 */

#define PLATFORM "platform: android-armeabi"
#define DATE "built on: Tue Jul  2 14:30:34 2019 UTC"

/*
 * Generate compiler_flags as an array of individual characters. This is a
 * workaround for the situation where CFLAGS gets too long for a C90 string
 * literal
 */
static const char compiler_flags[] = {
    'c','o','m','p','i','l','e','r',':',' ','a','r','m','-','l','i',
    'n','u','x','-','a','n','d','r','o','i','d','e','a','b','i','-',
    'g','c','c',' ','-','f','P','I','C',' ','-','p','t','h','r','e',
    'a','d',' ',' ','-','m','a','n','d','r','o','i','d',' ','-','-',
    's','y','s','r','o','o','t','=','/','u','s','r','/','l','o','c',
    'a','l','/','a','p','p','l','i','c','a','t','i','o','n','/','a',
    'n','d','r','o','i','d','-','n','d','k','-','r','1','5','b','/',
    'p','l','a','t','f','o','r','m','s','/','a','n','d','r','o','i',
    'd','-','1','8','/','a','r','c','h','-','a','r','m',' ',' ','-',
    'm','a','r','c','h','=','a','r','m','v','7','-','a',' ','-','W',
    'a','l','l',' ','-','O','3',' ','-','D','O','P','E','N','S','S',
    'L','_','U','S','E','_','N','O','D','E','L','E','T','E',' ','-',
    'D','O','P','E','N','S','S','L','_','P','I','C',' ','-','D','N',
    'D','E','B','U','G','\0'
};
