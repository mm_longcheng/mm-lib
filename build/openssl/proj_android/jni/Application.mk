APP_ABI := arm64-v8a armeabi-v7a # armeabi

APP_STL := gnustl_shared

APP_SHORT_COMMANDS := true

APP_MODULES += libcrypto_static
APP_MODULES += libcrypto_shared

APP_MODULES += libssl_static
APP_MODULES += libssl_shared
