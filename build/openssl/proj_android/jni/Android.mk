LOCAL_PATH := $(call my-dir)

MM_MAKE_HOME ?= $(MM_HOME)/mm-make

include $(CLEAR_VARS)

LOCAL_SHORT_COMMANDS := true

include $(LOCAL_PATH)/lib_prebuild.mk

include $(LOCAL_PATH)/libcrypto_static.mk
include $(LOCAL_PATH)/libcrypto_shared.mk

include $(LOCAL_PATH)/libssl_static.mk
include $(LOCAL_PATH)/libssl_shared.mk
