# 配置一个环境变量
export ANDROID_NDK_ROOT=/mnt/d/ndk/android-ndk-r14b
# 应用openssl-android环境变量
source ./setenv-android.sh
# 输出
ANDROID_NDK_ROOT: /mnt/d/ndk/android-ndk-r14b
ANDROID_ARCH: arch-arm
ANDROID_EABI: arm-linux-androideabi-4.9
ANDROID_API: android-18
ANDROID_SYSROOT: /mnt/d/ndk/android-ndk-r14b/platforms/android-18/arch-arm
ANDROID_TOOLCHAIN: /mnt/d/ndk/android-ndk-r14b/toolchains/arm-linux-androideabi-4.9/prebuilt/linux-x86_64/bin
FIPS_SIG: /mnt/d/openssl-1.1.0f/util/incore
CROSS_COMPILE: arm-linux-androideabi-
ANDROID_DEV: /mnt/d/ndk/android-ndk-r14b/platforms/android-18/arch-arm/usr

# 然后配置openssl
./config -no-asm
# 编译
make