LOCAL_PATH := $(call my-dir)

MM_MAKE_HOME ?= $(MM_HOME)/mm-make

include $(LOCAL_PATH)/lib_prebuild.mk

include $(LOCAL_PATH)/libOgreAL_static.mk
include $(LOCAL_PATH)/libOgreAL_shared.mk
