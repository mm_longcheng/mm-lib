# prebuild.mk
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libfreetype_shared
LOCAL_SRC_FILES := ../../../freetype/proj_android/libs/$(TARGET_ARCH_ABI)/libfreetype_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libfreetype_static
LOCAL_SRC_FILES := ../../../freetype/proj_android/obj/local/$(TARGET_ARCH_ABI)/libfreetype_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libFreeImage_shared
LOCAL_SRC_FILES := ../../../FreeImage/proj_android/libs/$(TARGET_ARCH_ABI)/libFreeImage_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libFreeImage_static
LOCAL_SRC_FILES := ../../../FreeImage/proj_android/obj/local/$(TARGET_ARCH_ABI)/libFreeImage_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libzzip_shared
LOCAL_SRC_FILES := ../../../zziplib/proj_android/libs/$(TARGET_ARCH_ABI)/libzzip_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libzzip_static
LOCAL_SRC_FILES := ../../../zziplib/proj_android/obj/local/$(TARGET_ARCH_ABI)/libzzip_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libOgreMain_shared
LOCAL_SRC_FILES := ../../../Ogre/proj_android/libs/$(TARGET_ARCH_ABI)/libOgreMain_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libOgreMain_static
LOCAL_SRC_FILES := ../../../Ogre/proj_android/obj/local/$(TARGET_ARCH_ABI)/libOgreMain_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libOpenAL_shared
LOCAL_SRC_FILES := ../../../OpenAL/proj_android/libs/$(TARGET_ARCH_ABI)/libOpenAL_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libOpenAL_static
LOCAL_SRC_FILES := ../../../OpenAL/proj_android/obj/local/$(TARGET_ARCH_ABI)/libOpenAL_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libogg_shared
LOCAL_SRC_FILES := ../../../libogg/proj_android/libs/$(TARGET_ARCH_ABI)/libogg_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libogg_static
LOCAL_SRC_FILES := ../../../libogg/proj_android/obj/local/$(TARGET_ARCH_ABI)/libogg_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libvorbis_shared
LOCAL_SRC_FILES := ../../../libvorbis/proj_android/libs/$(TARGET_ARCH_ABI)/libvorbis_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libvorbis_static
LOCAL_SRC_FILES := ../../../libvorbis/proj_android/obj/local/$(TARGET_ARCH_ABI)/libvorbis_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################