LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)
########################################################################
LOCAL_MODULE := libfreetype_shared
LOCAL_MODULE_FILENAME := libfreetype
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -DFT2_BUILD_LIBRARY
LOCAL_CFLAGS += -DFT_CONFIG_OPTION_SYSTEM_ZLIB
########################################################################
LOCAL_LDLIBS += -fPIC
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/freetype/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/freetype/src/truetype
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/freetype/src/sfnt
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/freetype/src/autofit
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/freetype/src/smooth
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/freetype/src/raster
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/freetype/src/psaux
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/freetype/src/psnames
########################################################################
LOCAL_SRC_FILES  += ../../../src/freetype/src/base/ftbase.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/base/ftbbox.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/base/ftbitmap.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/base/ftcid.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/base/ftdebug.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/base/ftfntfmt.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/base/ftfstype.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/base/ftgasp.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/base/ftglyph.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/base/ftgxval.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/base/ftinit.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/base/ftlcdfil.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/base/ftmm.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/base/ftotval.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/base/ftpatent.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/base/ftpfr.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/base/ftstroke.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/base/ftsynth.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/base/ftsystem.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/base/fttype1.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/base/ftwinfnt.c

LOCAL_SRC_FILES  += ../../../src/freetype/src/autofit/autofit.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/bdf/bdf.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/bzip2/ftbzip2.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/cache/ftcache.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/cff/cff.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/cid/type1cid.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/gzip/ftgzip.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/lzw/ftlzw.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/pcf/pcf.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/pfr/pfr.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/psaux/psaux.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/pshinter/pshinter.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/psnames/psnames.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/raster/raster.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/sfnt/sfnt.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/smooth/smooth.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/truetype/truetype.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/type1/type1.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/type42/type42.c
LOCAL_SRC_FILES  += ../../../src/freetype/src/winfonts/winfnt.c
########################################################################
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/$(TARGET_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
include $(SOURCE_RWILDCARD)
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################
# $(modules-dump-database)
