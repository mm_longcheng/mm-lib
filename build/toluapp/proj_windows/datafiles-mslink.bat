@echo off 

:: mkdir datafiles
IF NOT EXIST "bin" MD "bin"

:: ln -s
call:ln-s /j "bin/src" "../../../src/toluapp/src"
GOTO:EOF

:: ln-s mode target source
:ln-s  
IF NOT EXIST %~2 mklink %~1 "%~2" "%~3"
GOTO:EOF
