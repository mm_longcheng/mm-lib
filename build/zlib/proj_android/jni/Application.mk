APP_ABI := arm64-v8a armeabi-v7a # armeabi

APP_STL := gnustl_shared

APP_MODULES += libz_static
APP_MODULES += libz_shared

APP_MODULES += libminizip_static
APP_MODULES += libminizip_shared