LOCAL_PATH := $(call my-dir)

MM_MAKE_HOME ?= $(MM_HOME)/mm-make

include $(CLEAR_VARS)

include $(LOCAL_PATH)/libz_static.mk
include $(LOCAL_PATH)/libz_shared.mk

include $(LOCAL_PATH)/libminizip_static.mk
include $(LOCAL_PATH)/libminizip_shared.mk