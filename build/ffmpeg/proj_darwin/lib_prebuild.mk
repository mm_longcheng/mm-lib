LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
# prebuild.mk
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libiconv_shared
LOCAL_SRC_FILES := ../../libiconv/proj_$(TARGET_PLATFORM)/bin/libiconv.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libiconv_static
LOCAL_SRC_FILES := ../../libiconv/proj_$(TARGET_PLATFORM)/bin/libiconv.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libfdk-aac_shared
LOCAL_SRC_FILES := ../../fdk-aac/proj_$(TARGET_PLATFORM)/bin/libfdk-aac.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libfdk-aac_static
LOCAL_SRC_FILES := ../../fdk-aac/proj_$(TARGET_PLATFORM)/bin/libfdk-aac.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libopus_shared
LOCAL_SRC_FILES := ../../opus/proj_$(TARGET_PLATFORM)/bin/libopus.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libopus_static
LOCAL_SRC_FILES := ../../opus/proj_$(TARGET_PLATFORM)/bin/libopus.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libx264_shared
LOCAL_SRC_FILES := ../../x264/proj_$(TARGET_PLATFORM)/bin/libx264.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libx264_static
LOCAL_SRC_FILES := ../../x264/proj_$(TARGET_PLATFORM)/bin/libx264.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := librtmp_shared
LOCAL_SRC_FILES := ../../rtmpdump/proj_$(TARGET_PLATFORM)/bin/librtmp.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := librtmp_static
LOCAL_SRC_FILES := ../../rtmpdump/proj_$(TARGET_PLATFORM)/bin/librtmp.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libcrypto_shared
LOCAL_SRC_FILES := ../../openssl/proj_$(TARGET_PLATFORM)/bin/libcrypto.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libcrypto_static
LOCAL_SRC_FILES := ../../openssl/proj_$(TARGET_PLATFORM)/bin/libcrypto.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)  
LOCAL_MODULE := libssl_shared
LOCAL_SRC_FILES := ../../openssl/proj_$(TARGET_PLATFORM)/bin/libssl.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libssl_static
LOCAL_SRC_FILES := ../../openssl/proj_$(TARGET_PLATFORM)/bin/libssl.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libfreetype_shared
LOCAL_SRC_FILES := ../../freetype/proj_$(TARGET_PLATFORM)/bin/libfreetype.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libfreetype_static
LOCAL_SRC_FILES := ../../freetype/proj_$(TARGET_PLATFORM)/bin/libfreetype.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
# include $(CLEAR_VARS)  
# LOCAL_MODULE := libOpenAL_shared
# LOCAL_SRC_FILES := ../../OpenAL/proj_$(TARGET_PLATFORM)/bin/libOpenAL.so
# include $(PREBUILT_SHARED_LIBRARY)
# include $(CLEAR_VARS)  
# LOCAL_MODULE := libOpenAL_static
# LOCAL_SRC_FILES := ../../OpenAL/proj_$(TARGET_PLATFORM)/bin/libOpenAL.a
# include $(PREBUILT_STATIC_LIBRARY)
################################################################################