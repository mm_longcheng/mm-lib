LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)
########################################################################
LOCAL_MODULE := libavfilter_static
LOCAL_MODULE_FILENAME := libavfilter
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -DHAVE_AV_CONFIG_H
LOCAL_CFLAGS += -DBUILDING_avutil
# LOCAL_CFLAGS += -mfpu=neon
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../include/$(TARGET_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../source/$(TARGET_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/ffmpeg

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/freetype/include
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/ffmpeg/libavfilter
MY_SOURCES_PATH += $(LOCAL_PATH)/$(TARGET_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/opencl%
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/tests%
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/x86%

MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/af_bs2b.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/af_ladspa.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/af_lv2.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/af_resample.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/af_rubberband.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/af_sofalizer.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/asrc_flite.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/colorspacedsp_template.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/colorspacedsp_yuv2yuv_template.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/f_zmq.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/qsvvpp.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/vaapi_vpp.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/vf_avgblur_opencl.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/vf_convolution_opencl.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/vf_deinterlace_qsv.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/vf_deinterlace_vaapi.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/vf_frei0r.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/vf_libopencv.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/vf_libvmaf.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/vf_misc_vaapi.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/vf_ocr.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/vf_overlay_opencl.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/vf_overlay_qsv.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/vf_procamp_vaapi.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/vf_program_opencl.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/vf_scale_cuda.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/vf_scale_npp.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/vf_scale_qsv.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/vf_scale_vaapi.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/vf_subtitles.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/vf_thumbnail_cuda.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/vf_unsharp_opencl.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/vf_vidstabdetect.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/vf_vidstabtransform.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/vf_vpp_qsv.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/vf_zscale.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavfilter/vidstabutils.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
include $(SOURCE_RWILDCARD)
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################
# $(modules-dump-database)
