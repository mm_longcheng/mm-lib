LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)
########################################################################
LOCAL_MODULE := libpostproc_shared
LOCAL_MODULE_FILENAME := libpostproc
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -DHAVE_AV_CONFIG_H
LOCAL_CFLAGS += -DBUILDING_avutil
# LOCAL_CFLAGS += -mfpu=neon
########################################################################
LOCAL_LDLIBS += -fPIC
########################################################################
LOCAL_SHARED_LIBRARIES += libavutil_shared
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../include/$(TARGET_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../source/$(TARGET_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/ffmpeg
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/ffmpeg/libpostproc
MY_SOURCES_PATH += $(LOCAL_PATH)/$(TARGET_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libswscale/aarch64%

MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libpostproc/postprocess_template.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libpostproc/postprocess_altivec_template.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
include $(SOURCE_RWILDCARD)
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################
# $(modules-dump-database)
