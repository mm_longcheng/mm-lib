LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)
########################################################################
LOCAL_MODULE := llibavformat_static
LOCAL_MODULE_FILENAME := libavformat
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -DHAVE_AV_CONFIG_H
LOCAL_CFLAGS += -DBUILDING_avutil
# LOCAL_CFLAGS += -mfpu=neon
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../include/$(TARGET_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../source/$(TARGET_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/ffmpeg

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/rtmpdump

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/openssl/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../build/openssl/include/$(TARGET_PLATFORM)
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/ffmpeg/libavformat
MY_SOURCES_PATH += $(LOCAL_PATH)/$(TARGET_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavformat/tests%

MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavformat/bluray.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavformat/chromaprint.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavformat/dashdec.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavformat/libgme.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavformat/libmodplug.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavformat/libopenmpt.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavformat/libsmbclient.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavformat/libsrt.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavformat/libssh.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavformat/sctp.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavformat/tls_gnutls.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavformat/tls_libtls.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavformat/tls_schannel.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavformat/tls_securetransport.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
include $(SOURCE_RWILDCARD)
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################
# $(modules-dump-database)
