LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)
########################################################################
LOCAL_MODULE := libavdevice_static
LOCAL_MODULE_FILENAME := libavdevice
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -DHAVE_AV_CONFIG_H
LOCAL_CFLAGS += -DBUILDING_avutil
# LOCAL_CFLAGS += -mfpu=neon

LOCAL_CFLAGS += -fOpenAL.framework
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../include/$(TARGET_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../source/$(TARGET_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/ffmpeg

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/OpenAL/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../build/OpenAL/include/$(TARGET_PLATFORM)
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/ffmpeg/libavdevice
MY_SOURCES_PATH += $(LOCAL_PATH)/$(TARGET_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/tests%

MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/alsa.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/alsa_dec.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/alsa_enc.c

MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/android_camera.c

MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/bktr.c

MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/caca.c

MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/decklink_common.cpp
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/decklink_dec.cpp
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/decklink_dec_c.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/decklink_enc.cpp
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/decklink_enc_c.c

MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/dshow.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/dshow_common.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/dshow_crossbar.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/dshow_enummediatypes.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/dshow_enumpins.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/dshow_filter.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/dshow_pin.c

MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/gdigrab.c

MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/iec61883.c

MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/jack.c

MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/kmsgrab.c

MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/libcdio.c

MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/libdc1394.c

MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/libndi_newtek_dec.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/libndi_newtek_enc.c

MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/opengl_enc.c

MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/oss.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/oss_dec.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/oss_enc.c

MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/pulse_audio_common.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/pulse_audio_dec.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/pulse_audio_enc.c

MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/sdl2.c

MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/sndio.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/sndio_dec.c
MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/sndio_enc.c

MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/v4l2.c

MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/vfwcap.c

MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/xcbgrab.c

MY_SOURCES_FILTER_OUT += ../../../src/ffmpeg/libavdevice/xv.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
include $(SOURCE_RWILDCARD)
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################
# $(modules-dump-database)
