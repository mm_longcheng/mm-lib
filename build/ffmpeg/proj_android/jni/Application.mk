APP_ABI := arm64-v8a armeabi-v7a # armeabi

APP_STL := gnustl_shared

APP_SHORT_COMMANDS := true

APP_MODULES += libavutil_static
APP_MODULES += libavutil_shared

APP_MODULES += libswscale_static
APP_MODULES += libswscale_shared

APP_MODULES += libswresample_static
APP_MODULES += libswresample_shared

APP_MODULES += libpostproc_static
APP_MODULES += libpostproc_shared

APP_MODULES += libavcodec_static
APP_MODULES += libavcodec_shared

APP_MODULES += libavformat_static
APP_MODULES += libavformat_shared

APP_MODULES += libavfilter_static
APP_MODULES += libavfilter_shared

APP_MODULES += libavdevice_static
APP_MODULES += libavdevice_shared