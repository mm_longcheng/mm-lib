# prebuild.mk
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libz_shared
LOCAL_SRC_FILES := ../../../zlib/proj_android/libs/$(TARGET_ARCH_ABI)/libz_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libz_static
LOCAL_SRC_FILES := ../../../zlib/proj_android/obj/local/$(TARGET_ARCH_ABI)/libz_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libiconv_shared
LOCAL_SRC_FILES := ../../../libiconv/proj_android/libs/$(TARGET_ARCH_ABI)/libiconv_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libiconv_static
LOCAL_SRC_FILES := ../../../libiconv/proj_android/obj/local/$(TARGET_ARCH_ABI)/libiconv_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libfdk-aac_shared
LOCAL_SRC_FILES := ../../../fdk-aac/proj_android/libs/$(TARGET_ARCH_ABI)/libfdk-aac_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libfdk-aac_static
LOCAL_SRC_FILES := ../../../fdk-aac/proj_android/obj/local/$(TARGET_ARCH_ABI)/libfdk-aac_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libopus_shared
LOCAL_SRC_FILES := ../../../opus/proj_android/libs/$(TARGET_ARCH_ABI)/libopus_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libopus_static
LOCAL_SRC_FILES := ../../../opus/proj_android/obj/local/$(TARGET_ARCH_ABI)/libopus_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libx264_shared
LOCAL_SRC_FILES := ../../../x264/proj_android/libs/$(TARGET_ARCH_ABI)/libx264_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libx264_static
LOCAL_SRC_FILES := ../../../x264/proj_android/obj/local/$(TARGET_ARCH_ABI)/libx264_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := librtmp_shared
LOCAL_SRC_FILES := ../../../rtmpdump/proj_android/libs/$(TARGET_ARCH_ABI)/librtmp_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := librtmp_static
LOCAL_SRC_FILES := ../../../rtmpdump/proj_android/obj/local/$(TARGET_ARCH_ABI)/librtmp_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libcrypto_shared
LOCAL_SRC_FILES := ../../../openssl/proj_android/libs/$(TARGET_ARCH_ABI)/libcrypto_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libcrypto_static
LOCAL_SRC_FILES := ../../../openssl/proj_android/obj/local/$(TARGET_ARCH_ABI)/libcrypto_static.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)  
LOCAL_MODULE := libssl_shared
LOCAL_SRC_FILES := ../../../openssl/proj_android/libs/$(TARGET_ARCH_ABI)/libssl_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libssl_static
LOCAL_SRC_FILES := ../../../openssl/proj_android/obj/local/$(TARGET_ARCH_ABI)/libssl_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libfreetype_shared
LOCAL_SRC_FILES := ../../../freetype/proj_android/libs/$(TARGET_ARCH_ABI)/libfreetype_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libfreetype_static
LOCAL_SRC_FILES := ../../../freetype/proj_android/obj/local/$(TARGET_ARCH_ABI)/libfreetype_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libOpenAL_shared
LOCAL_SRC_FILES := ../../../OpenAL/proj_android/libs/$(TARGET_ARCH_ABI)/libOpenAL_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libOpenAL_static
LOCAL_SRC_FILES := ../../../OpenAL/proj_android/obj/local/$(TARGET_ARCH_ABI)/libOpenAL_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################