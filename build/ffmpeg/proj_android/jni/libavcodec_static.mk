LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libavcodec_static
LOCAL_MODULE_FILENAME := libavcodec_static
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -D__ANDROID__

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -Wno-logical-op-parentheses
LOCAL_CFLAGS += -Wno-pointer-sign
LOCAL_CFLAGS += -Wno-switch
LOCAL_CFLAGS += -Wno-unused-const-variable
LOCAL_CFLAGS += -Wno-parentheses
LOCAL_CFLAGS += -Wno-deprecated-declarations
LOCAL_CFLAGS += -Wno-incompatible-pointer-types-discards-qualifiers
LOCAL_CFLAGS += -Wno-sometimes-uninitialized
LOCAL_CFLAGS += -Wno-unused-function
LOCAL_CFLAGS += -Wno-unused-variable
LOCAL_CFLAGS += -Wno-missing-braces
LOCAL_CFLAGS += -Wno-string-plus-int

ifeq ($(TARGET_ARCH_ABI),arm64-v8a)
	LOCAL_CFLAGS += -Wno-incompatible-pointer-types
else
	LOCAL_CFLAGS += 
endif

LOCAL_CFLAGS += -DHAVE_AV_CONFIG_H
LOCAL_CFLAGS += -DBUILDING_avutil
LOCAL_CFLAGS += -mfpu=neon
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/android
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../source/android
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/ffmpeg

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../build/libiconv/include/android

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../build/fdk-aac/include/android

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../build/opus/include/android
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/opus/include

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../build/x264/include/android
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/x264

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/zlib
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/ffmpeg/libavcodec
# MY_SOURCES_PATH += $(LOCAL_PATH)/android

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
ifeq ($(TARGET_ARCH_ABI),arm64-v8a)
	MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/arm%
	
	MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/aarch64/vp9dsp_init_16bpp_aarch64_template.c
	MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/aarch64/neontest.c
else
	MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/aarch64%
	
	MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/arm/vp9dsp_init_16bpp_arm_template.c
	MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/arm/neontest.c
endif

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/alpha%
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/avr32%
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/bfin%
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/mips%
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/ppc%
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/sh4%
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/sparc%
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/tests%
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/x86%

# MY_SOURCES_FILTER_OUT += %_template.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/aacdec_template.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/aacpsdata.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/aacsbr_template.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/aacps_tablegen_template.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/aacpsdsp_template.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/aacps.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/aacps_fixed_tablegen.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/aacps_tablegen.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/ac3enc_opts_template.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/ac3enc_template.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/ac3dec.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/amfenc.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/amfenc_h264.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/amfenc_hevc.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/audiotoolboxdec.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/audiotoolboxenc.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/bit_depth_template.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/cbrt_tablegen.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/cbrt_fixed_tablegen.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/cbrt_tablegen_template.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/cbs_h264_syntax_template.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/cbs_h265_syntax_template.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/cbs_mpeg2_syntax_template.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/cos_tablegen.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/crystalhd.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/cuviddec.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/d3d11va.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/dct32_template.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/dirac_dwt_template.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/dv_tablegen.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/dxva2.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/dxva2_h264.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/dxva2_hevc.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/dxva2_mpeg2.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/dxva2_vc1.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/dxva2_vp9.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/fft_template.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/ffv1_template.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/ffv1dec_template.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/ffv1enc_template.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/flacdsp_lpc_template.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/flacdsp_template.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/gsmdec_template.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/h264_mb_template.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/h264_mc_template.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/h264addpx_template.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/h264chroma_template.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/h264dsp_template.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/h264idct_template.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/h264pred_template.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/h264qpel_template.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/hapenc.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/hevcdsp_template.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/hevcpred_template.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/hpel_template.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/jfdctint_template.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/libaomdec.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/libaomenc.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/libcelt_dec.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/libcodec2.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/libgsmdec.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/libgsmenc.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/libilbc.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/libkvazaar.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/libmp3lame.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/libopenh264.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/libopenh264dec.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/libopenh264enc.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/libopenjpegdec.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/libopenjpegenc.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/librsvgdec.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/libshine.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/libspeexdec.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/libspeexenc.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/libtheoraenc.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/libtwolame.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/libvo-amrwbenc.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/libvorbisdec.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/libvorbisenc.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/libvpx.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/libvpxdec.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/libvpxenc.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/libwavpackenc.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/libwebpenc.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/libwebpenc_animencoder.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/libwebpenc_common.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/libx265.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/libxavs.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/libxvid.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/libxvid_rc.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/libzvbi-teletextdec.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/mdct_template.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/mmaldec.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/motionpixels_tablegen.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/motion_est_template.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/mpegaudiodec_template.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/mpegaudiodsp_template.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/mpegaudioenc_template.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/mpegaudio_tablegen.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/mpegvideo_xvmc.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/nvdec.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/nvdec_h264.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/nvdec_hevc.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/nvdec_mjpeg.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/nvdec_mpeg12.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/nvdec_mpeg4.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/nvdec_vc1.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/nvdec_vp8.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/nvdec_vp9.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/nvenc.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/nvenc_h264.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/nvenc_hevc.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/omx.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/pcm_tablegen.

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/pel_template.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/qdm2_tablegen.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/qpel_template.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/qsv.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/qsvdec.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/qsvdec_h2645.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/qsvdec_other.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/qsvenc.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/qsvenc_h264.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/qsvenc_hevc.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/qsvenc_jpeg.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/qsvenc_mpeg2.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/rkmppdec.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/sbrdsp_template.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/simple_idct_template.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/sinewin_tablegen_template.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/sinewin_fixed_tablegen.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/sinewin_tablegen.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/vaapi_decode.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/vaapi_encode.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/vaapi_encode_h264.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/vaapi_encode_h265.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/vaapi_encode_mjpeg.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/vaapi_encode_mpeg2.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/vaapi_encode_vp8.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/vaapi_encode_vp9.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/vaapi_h264.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/vaapi_hevc.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/vaapi_mjpeg.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/vaapi_mpeg2.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/vaapi_mpeg4.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/vaapi_vc1.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/vaapi_vp8.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/vaapi_vp9.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/vdpau.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/vdpau_h264.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/vdpau_hevc.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/vdpau_mpeg12.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/vdpau_mpeg4.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/vdpau_vc1.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/videodsp_template.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/videotoolbox.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/videotoolboxenc.c

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/vp9_mc_template.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavcodec/vp9dsp_template.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################