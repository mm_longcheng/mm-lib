LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libswresample_static
LOCAL_MODULE_FILENAME := libswresample_static
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -D__ANDROID__

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -Wno-switch
LOCAL_CFLAGS += -Wno-logical-op-parentheses
LOCAL_CFLAGS += -Wno-string-plus-int

LOCAL_CFLAGS += -DHAVE_AV_CONFIG_H
LOCAL_CFLAGS += -DBUILDING_avutil
LOCAL_CFLAGS += -mfpu=neon
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/android
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../source/android
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/ffmpeg
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/ffmpeg/libswresample
# MY_SOURCES_PATH += $(LOCAL_PATH)/android

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
ifeq ($(TARGET_ARCH_ABI),arm64-v8a)
	MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libswresample/arm%
	
	MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libswresample/aarch64/neontest.c
else
	MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libswresample/aarch64%
	
	MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libswresample/arm/neontest.c
endif

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libswresample/ppc%
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libswresample/tests%
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libswresample/x86%

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libswresample/dither_template.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libswresample/noise_shaping_data.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libswresample/rematrix_template.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libswresample/resample_template.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libswresample/soxr_resample.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################