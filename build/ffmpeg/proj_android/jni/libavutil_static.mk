LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libavutil_static
LOCAL_MODULE_FILENAME := libavutil_static
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -D__ANDROID__

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -Wno-pointer-sign
LOCAL_CFLAGS += -Wno-switch
LOCAL_CFLAGS += -Wno-logical-op-parentheses
LOCAL_CFLAGS += -Wno-parentheses
LOCAL_CFLAGS += -Wno-absolute-value
LOCAL_CFLAGS += -Wno-string-plus-int

LOCAL_CFLAGS += -DHAVE_AV_CONFIG_H
LOCAL_CFLAGS += -DBUILDING_avutil
LOCAL_CFLAGS += -mfpu=neon
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/android
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../source/android
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/ffmpeg
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/ffmpeg/libavutil
# MY_SOURCES_PATH += $(LOCAL_PATH)/android

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
ifeq ($(TARGET_ARCH_ABI),arm64-v8a)
	MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavutil/arm%
else
	MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavutil/aarch64%
endif

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavutil/avr32%
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavutil/bfin%
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavutil/mips%
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavutil/ppc%
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavutil/sh4%
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavutil/tests%
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavutil/tomi%
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavutil/x86%

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavutil/hwcontext_cuda%
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavutil/hwcontext_d3d%
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavutil/hwcontext_drm%
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavutil/hwcontext_dxva%
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavutil/hwcontext_opencl%
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavutil/hwcontext_qsv%
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavutil/hwcontext_vaapi%
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavutil/hwcontext_vdpau%
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libavutil/hwcontext_videotoolbox%

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################