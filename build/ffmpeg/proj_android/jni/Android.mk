LOCAL_PATH := $(call my-dir)

MM_MAKE_HOME ?= $(MM_HOME)/mm-make

include $(CLEAR_VARS)

LOCAL_SHORT_COMMANDS := true

include $(LOCAL_PATH)/lib_prebuild.mk

include $(LOCAL_PATH)/libavutil_static.mk
include $(LOCAL_PATH)/libavutil_shared.mk

include $(LOCAL_PATH)/libswscale_static.mk
include $(LOCAL_PATH)/libswscale_shared.mk

include $(LOCAL_PATH)/libswresample_static.mk
include $(LOCAL_PATH)/libswresample_shared.mk

include $(LOCAL_PATH)/libpostproc_static.mk
include $(LOCAL_PATH)/libpostproc_shared.mk

include $(LOCAL_PATH)/libavcodec_static.mk
include $(LOCAL_PATH)/libavcodec_shared.mk

include $(LOCAL_PATH)/libavformat_static.mk
include $(LOCAL_PATH)/libavformat_shared.mk

include $(LOCAL_PATH)/libavfilter_static.mk
include $(LOCAL_PATH)/libavfilter_shared.mk

include $(LOCAL_PATH)/libavdevice_static.mk
include $(LOCAL_PATH)/libavdevice_shared.mk