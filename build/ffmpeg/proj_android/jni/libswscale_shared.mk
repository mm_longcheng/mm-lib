LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libswscale_shared
LOCAL_MODULE_FILENAME := libswscale_shared
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -D__ANDROID__

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -Wno-pointer-sign
LOCAL_CFLAGS += -Wno-switch
LOCAL_CFLAGS += -Wno-dangling-else
LOCAL_CFLAGS += -Wno-shift-op-parentheses
LOCAL_CFLAGS += -Wno-bitwise-op-parentheses
LOCAL_CFLAGS += -Wno-logical-op-parentheses
LOCAL_CFLAGS += -Wno-empty-body
LOCAL_CFLAGS += -Wno-string-plus-int

LOCAL_CFLAGS += -D_LARGEFILE_SOURCE
LOCAL_CFLAGS += -D_USE_MATH_DEFINES
LOCAL_CFLAGS += -DHAVE_AV_CONFIG_H
LOCAL_CFLAGS += -DBUILDING_avutil
LOCAL_CFLAGS += -mfpu=neon

# unexpected token in argument list
LOCAL_ASFLAGS += -fno-integrated-as
########################################################################
LOCAL_LDLIBS += -fPIC
########################################################################
LOCAL_SHARED_LIBRARIES += libavutil_shared
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/android
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../source/android
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/ffmpeg
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/ffmpeg/libswscale
MY_SOURCES_PATH += $(LOCAL_PATH)/android

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
ifeq ($(TARGET_ARCH_ABI),arm64-v8a)
	MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libswscale/arm%
else
	MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libswscale/aarch64%
endif

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libswscale/ppc%
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libswscale/tests%
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libswscale/x86%

MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libswscale/bayer_template.c
MY_SOURCES_FILTER_OUT += ../../../../src/ffmpeg/libswscale/rgb2rgb_template.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################