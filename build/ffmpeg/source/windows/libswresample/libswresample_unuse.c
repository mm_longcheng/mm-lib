#include <crtdefs.h>
// swri_audio_convert_alloc
struct AudioConvert;
enum AVSampleFormat;
void swri_audio_convert_init_aarch64(struct AudioConvert *ac, enum AVSampleFormat out_fmt, enum AVSampleFormat in_fmt, int channels){}
void swri_audio_convert_init_arm(struct AudioConvert *ac, enum AVSampleFormat out_fmt, enum AVSampleFormat in_fmt, int channels){}
// void swri_audio_convert_init_x86(struct AudioConvert *ac, enum AVSampleFormat out_fmt, enum AVSampleFormat in_fmt, int channels){}

// swri_resample_dsp_init
struct ResampleContext;
// void swri_resample_dsp_x86_init(struct ResampleContext *c){}
void swri_resample_dsp_arm_init(struct ResampleContext *c){}
void swri_resample_dsp_aarch64_init(struct ResampleContext *c){}

#if ARCH_X86_64
// swri_resample_dsp_x86_init
struct ResampleContext;
void ff_resample_common_int16_mmxext(struct ResampleContext *c) {}
void ff_resample_linear_int16_mmxext(struct ResampleContext *c) {}
#endif // ARCH_X86_32