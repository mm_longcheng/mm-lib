#include <crtdefs.h>
#include <stdint.h>

// aacdec_init
struct AACContext;
void ff_aacdec_init_mips(struct AACContext *c){}

// aac_encode_init
struct AACEncContext;
void ff_aac_coder_init_mips(struct AACEncContext *c){}

// ff_psdsp_init
struct PSDSPContext;
void ff_psdsp_init_arm(struct PSDSPContext *s){}
void ff_psdsp_init_aarch64(struct PSDSPContext *s){}
void ff_psdsp_init_mips(struct PSDSPContext *s){}
// void ff_psdsp_init_x86(struct PSDSPContext *s){}

// aacsbr_func_ptr_init
struct AACSBRContext;
void ff_aacsbr_func_ptr_init_mips(struct AACSBRContext *c){}

// ff_ac3dsp_init
struct AC3DSPContext;
void ff_ac3dsp_init_arm(struct AC3DSPContext *c, int bit_exact){}
// void ff_ac3dsp_init_x86(struct AC3DSPContext *c, int bit_exact){}
void ff_ac3dsp_init_mips(struct AC3DSPContext *c, int bit_exact){}

// ff_acelp_filter_init
struct ACELPFContext;
void ff_acelp_filter_init_mips(struct ACELPFContext *c){}

// ff_acelp_vectors_init
struct ACELPVContext;
void ff_acelp_vectors_init_mips(struct ACELPVContext *c){}

// ff_audiodsp_init
struct AudioDSPContext;
void ff_audiodsp_init_arm(struct AudioDSPContext *c){}
void ff_audiodsp_init_ppc(struct AudioDSPContext *c){}
// void ff_audiodsp_init_x86(struct AudioDSPContext *c){}

// ff_blockdsp_init
struct BlockDSPContext;
struct AVCodecContext;
void ff_blockdsp_init_alpha(struct BlockDSPContext *c){}
void ff_blockdsp_init_arm(struct BlockDSPContext *c){}
void ff_blockdsp_init_ppc(struct BlockDSPContext *c){}
// void ff_blockdsp_init_x86(struct BlockDSPContext *c, struct AVCodecContext *avctx){}
void ff_blockdsp_init_mips(struct BlockDSPContext *c){}

// ff_celp_filter_init
struct CELPFContext;
void ff_celp_filter_init_mips(struct CELPFContext *c){}

// ff_celp_math_init
struct CELPMContext;
void ff_celp_math_init_mips(struct CELPMContext *c){}

// ff_fdctdsp_init
struct FDCTDSPContext;
struct AVCodecContext;
void ff_fdctdsp_init_ppc(struct FDCTDSPContext *c, struct AVCodecContext *avctx, unsigned high_bit_depth){}
// void ff_fdctdsp_init_x86(struct FDCTDSPContext *c, struct AVCodecContext *avctx, unsigned high_bit_depth){}

// ff_fft_init_fixed
struct FFTContext;
void ff_fft_fixed_init_arm(struct FFTContext *s){}

// ff_fft_init
struct FFTContext;
void ff_fft_init_aarch64(struct FFTContext *s){}
// void ff_fft_init_x86(struct FFTContext *s){}
void ff_fft_init_arm(struct FFTContext *s){}
void ff_fft_init_mips(struct FFTContext *s){}
void ff_fft_init_ppc(struct FFTContext *s){}

// ff_flacdsp_init
struct FLACDSPContext;
enum AVSampleFormat;
void ff_flacdsp_init_arm(struct FLACDSPContext *c, enum AVSampleFormat fmt, int channels, int bps){}
// void ff_flacdsp_init_x86(struct FLACDSPContext *c, enum AVSampleFormat fmt, int channels, int bps){}

// ff_fmt_convert_init
struct FmtConvertContext;
struct AVCodecContext;
void ff_fmt_convert_init_aarch64(struct FmtConvertContext *c, struct AVCodecContext *avctx){}
void ff_fmt_convert_init_arm(struct FmtConvertContext *c, struct AVCodecContext *avctx){}
void ff_fmt_convert_init_ppc(struct FmtConvertContext *c, struct AVCodecContext *avctx){}
// void ff_fmt_convert_init_x86(struct FmtConvertContext *c, struct AVCodecContext *avctx){}
void ff_fmt_convert_init_mips(struct FmtConvertContext *c){}

// ff_g722dsp_init
struct G722DSPContext;
void ff_g722dsp_init_arm(struct G722DSPContext *c){}
// void ff_g722dsp_init_x86(struct G722DSPContext *c){}

// ff_h263dsp_init
struct H263DSPContext;
// void ff_h263dsp_init_x86(struct H263DSPContext *ctx){}
void ff_h263dsp_init_mips(struct H263DSPContext *ctx){}

// ff_h264chroma_init
struct H264ChromaContext;
void ff_h264chroma_init_aarch64(struct H264ChromaContext *c, int bit_depth){}
void ff_h264chroma_init_arm(struct H264ChromaContext *c, int bit_depth){}
void ff_h264chroma_init_ppc(struct H264ChromaContext *c, int bit_depth){}
// void ff_h264chroma_init_x86(struct H264ChromaContext *c, int bit_depth){}
void ff_h264chroma_init_mips(struct H264ChromaContext *c, int bit_depth){}

// ff_h264dsp_init
struct H264DSPContext;
void ff_h264dsp_init_aarch64(struct H264DSPContext *c, const int bit_depth, const int chroma_format_idc){}
void ff_h264dsp_init_arm(struct H264DSPContext *c, const int bit_depth, const int chroma_format_idc){}
void ff_h264dsp_init_ppc(struct H264DSPContext *c, const int bit_depth, const int chroma_format_idc){}
// void ff_h264dsp_init_x86(struct H264DSPContext *c, const int bit_depth, const int chroma_format_idc){}
void ff_h264dsp_init_mips(struct H264DSPContext *c, const int bit_depth, const int chroma_format_idc){}

// ff_h264_pred_init
struct H264PredContext;
void ff_h264_pred_init_aarch64(struct H264PredContext *h, int codec_id, const int bit_depth, const int chroma_format_idc){}
void ff_h264_pred_init_arm(struct H264PredContext *h, int codec_id, const int bit_depth, const int chroma_format_idc){}
// void ff_h264_pred_init_x86(struct H264PredContext *h, int codec_id, const int bit_depth, const int chroma_format_idc){}
void ff_h264_pred_init_mips(struct H264PredContext *h, int codec_id, const int bit_depth, const int chroma_format_idc){}

// ff_h264qpel_init
struct H264QpelContext;
void ff_h264qpel_init_aarch64(struct H264QpelContext *c, int bit_depth){}
void ff_h264qpel_init_arm(struct H264QpelContext *c, int bit_depth){}
void ff_h264qpel_init_ppc(struct H264QpelContext *c, int bit_depth){}
// void ff_h264qpel_init_x86(struct H264QpelContext *c, int bit_depth){}
void ff_h264qpel_init_mips(struct H264QpelContext *c, int bit_depth){}

// ff_hevc_dsp_init
struct HEVCDSPContext;
void ff_hevc_dsp_init_arm(struct HEVCDSPContext *c, const int bit_depth){}
void ff_hevc_dsp_init_ppc(struct HEVCDSPContext *c, const int bit_depth){}
// void ff_hevc_dsp_init_x86(struct HEVCDSPContext *c, const int bit_depth){}
void ff_hevc_dsp_init_mips(struct HEVCDSPContext *c, const int bit_depth){}

// ff_hpeldsp_init
struct HpelDSPContext;
void ff_hpeldsp_init_aarch64(struct HpelDSPContext *c, int flags){}
void ff_hpeldsp_init_alpha(struct HpelDSPContext *c, int flags){}
void ff_hpeldsp_init_arm(struct HpelDSPContext *c, int flags){}
void ff_hpeldsp_init_ppc(struct HpelDSPContext *c, int flags){}
// void ff_hpeldsp_init_x86(struct HpelDSPContext *c, int flags){}
void ff_hpeldsp_init_mips(struct HpelDSPContext *c, int flags){}

// ff_idctdsp_init
struct IDCTDSPContext;
struct AVCodecContext;
void ff_idctdsp_init_aarch64(struct IDCTDSPContext *c, struct AVCodecContext *avctx, unsigned high_bit_depth){}
void ff_idctdsp_init_alpha(struct IDCTDSPContext *c, struct AVCodecContext *avctx, unsigned high_bit_depth){}
void ff_idctdsp_init_arm(struct IDCTDSPContext *c, struct AVCodecContext *avctx, unsigned high_bit_depth){}
void ff_idctdsp_init_ppc(struct IDCTDSPContext *c, struct AVCodecContext *avctx, unsigned high_bit_depth){}
// void ff_idctdsp_init_x86(struct IDCTDSPContext *c, struct AVCodecContext *avctx, unsigned high_bit_depth){}
void ff_idctdsp_init_mips(struct IDCTDSPContext *c, struct AVCodecContext *avctx, unsigned high_bit_depth){}

// ff_iir_filter_init
struct FFIIRFilterContext;
void ff_iir_filter_init_mips(struct FFIIRFilterContext *f){}

// ff_llauddsp_init
struct LLAudDSPContext;
void ff_llauddsp_init_arm(struct LLAudDSPContext *c){}
void ff_llauddsp_init_ppc(struct LLAudDSPContext *c){}
// void ff_llauddsp_init_x86(struct LLAudDSPContext *c){}

// ff_llviddsp_init
struct LLVidDSPContext;
// void ff_llviddsp_init_x86(struct LLVidDSPContext *llviddsp){}
void ff_llviddsp_init_ppc(struct LLVidDSPContext *llviddsp){}

// ff_me_cmp_init
struct MECmpContext;
struct AVCodecContext;
void ff_me_cmp_init_alpha(struct MECmpContext *c, struct AVCodecContext *avctx){}
void ff_me_cmp_init_arm(struct MECmpContext *c, struct AVCodecContext *avctx){}
void ff_me_cmp_init_ppc(struct MECmpContext *c, struct AVCodecContext *avctx){}
// void ff_me_cmp_init_x86(struct MECmpContext *c, struct AVCodecContext *avctx){}
void ff_me_cmp_init_mips(struct MECmpContext *c, struct AVCodecContext *avctx){}

// ff_mlpdsp_init
struct MLPDSPContext;
void ff_mlpdsp_init_arm(struct MLPDSPContext *c){}
// void ff_mlpdsp_init_x86(struct MLPDSPContext *c){}

// mpeg_decode_slice
struct MpegEncContext;
void ff_xvmc_init_block(struct MpegEncContext *s){}

// mpeg_decode_mb
struct MpegEncContext;
void ff_xvmc_pack_pblocks(struct MpegEncContext *s, int cbp){}

// ff_mpadsp_init
struct MPADSPContext;
void ff_mpadsp_init_aarch64(struct MPADSPContext *s){}
void ff_mpadsp_init_arm(struct MPADSPContext *s){}
void ff_mpadsp_init_ppc(struct MPADSPContext *s){}
// void ff_mpadsp_init_x86(struct MPADSPContext *s){}
void ff_mpadsp_init_mipsfpu(struct MPADSPContext *s){}
void ff_mpadsp_init_mipsdsp(struct MPADSPContext *s){}

// dct_init
struct MpegEncContext;
void ff_mpv_common_init_arm(struct MpegEncContext *s){}
void ff_mpv_common_init_axp(struct MpegEncContext *s){}
void ff_mpv_common_init_neon(struct MpegEncContext *s){}
void ff_mpv_common_init_ppc(struct MpegEncContext *s){}
// void ff_mpv_common_init_x86(struct MpegEncContext *s){}
void ff_mpv_common_init_mips(struct MpegEncContext *s){}

// ff_mpegvideodsp_init
struct MpegVideoDSPContext;
void ff_mpegvideodsp_init_ppc(struct MpegVideoDSPContext *c){}
// void ff_mpegvideodsp_init_x86(struct MpegVideoDSPContext *c){}

// ff_mpegvideoencdsp_init
struct MpegvideoEncDSPContext;
struct AVCodecContext;
void ff_mpegvideoencdsp_init_arm(struct MpegvideoEncDSPContext *c, struct AVCodecContext *avctx){}
void ff_mpegvideoencdsp_init_ppc(struct MpegvideoEncDSPContext *c, struct AVCodecContext *avctx){}
// void ff_mpegvideoencdsp_init_x86(struct MpegvideoEncDSPContext *c, struct AVCodecContext *avctx){}
void ff_mpegvideoencdsp_init_mips(struct MpegvideoEncDSPContext *c, struct AVCodecContext *avctx){}

// ff_pixblockdsp_init
struct PixblockDSPContext;
struct AVCodecContext;
void ff_pixblockdsp_init_alpha(struct PixblockDSPContext *c, struct AVCodecContext *avctx, unsigned high_bit_depth){}
void ff_pixblockdsp_init_arm(struct PixblockDSPContext *c, struct AVCodecContext *avctx, unsigned high_bit_depth){}
void ff_pixblockdsp_init_ppc(struct PixblockDSPContext *c, struct AVCodecContext *avctx, unsigned high_bit_depth){}
// void ff_pixblockdsp_init_x86(struct PixblockDSPContext *c, struct AVCodecContext *avctx, unsigned high_bit_depth){}
void ff_pixblockdsp_init_mips(struct PixblockDSPContext *c, struct AVCodecContext *avctx, unsigned high_bit_depth){}

// ff_qpeldsp_init
struct QpelDSPContext;
// void ff_qpeldsp_init_x86(struct QpelDSPContext *c){}
void ff_qpeldsp_init_mips(struct QpelDSPContext *c){}

// ff_rdft_init
struct RDFTContext;
void ff_rdft_init_arm(struct RDFTContext *s){}

// ff_rv34dsp_init
struct RV34DSPContext;
void ff_rv34dsp_init_arm(struct RV34DSPContext *c){}
// void ff_rv34dsp_init_x86(struct RV34DSPContext *c){}

// ff_rv40dsp_init
struct RV34DSPContext;
void ff_rv40dsp_init_aarch64(struct RV34DSPContext *c){}
// void ff_rv40dsp_init_x86(struct RV34DSPContext *c){}
void ff_rv40dsp_init_arm(struct RV34DSPContext *c){}

// ff_sbcdsp_init
struct SBCDSPContext;
void ff_sbcdsp_init_arm(struct SBCDSPContext *s){}
// void ff_sbcdsp_init_x86(struct SBCDSPContext *s){}

// ff_sbrdsp_init
struct SBRDSPContext;
void ff_sbrdsp_init_arm(struct SBRDSPContext *s){}
void ff_sbrdsp_init_aarch64(struct SBRDSPContext *s){}
// void ff_sbrdsp_init_x86(struct SBRDSPContext *s){}
void ff_sbrdsp_init_mips(struct SBRDSPContext *s){}

// svq1_encode_init
struct SVQ1EncContext;
void ff_svq1enc_init_ppc(struct SVQ1EncContext *c){}
// void ff_svq1enc_init_x86(struct SVQ1EncContext *c){}

// ff_synth_filter_init
struct SynthFilterContext;
void ff_synth_filter_init_aarch64(struct SynthFilterContext *c){}
void ff_synth_filter_init_arm(struct SynthFilterContext *c){}
// void ff_synth_filter_init_x86(struct SynthFilterContext *c){}

// ff_vc1dsp_init
struct VC1DSPContext;
void ff_vc1dsp_init_aarch64(struct VC1DSPContext* dsp){}
void ff_vc1dsp_init_arm(struct VC1DSPContext* dsp){}
void ff_vc1dsp_init_ppc(struct VC1DSPContext *c){}
// void ff_vc1dsp_init_x86(struct VC1DSPContext* dsp){}
void ff_vc1dsp_init_mips(struct VC1DSPContext* dsp){}

// ff_videodsp_init
struct VideoDSPContext;
void ff_videodsp_init_aarch64(struct VideoDSPContext *ctx, int bpc){}
void ff_videodsp_init_arm(struct VideoDSPContext *ctx, int bpc){}
void ff_videodsp_init_ppc(struct VideoDSPContext *ctx, int bpc){}
// void ff_videodsp_init_x86(struct VideoDSPContext *ctx, int bpc){}
void ff_videodsp_init_mips(struct VideoDSPContext *ctx, int bpc){}

// ff_vorbisdsp_init
struct VorbisDSPContext;
void ff_vorbisdsp_init_aarch64(struct VorbisDSPContext *dsp){}
// void ff_vorbisdsp_init_x86(struct VorbisDSPContext *dsp){}
void ff_vorbisdsp_init_arm(struct VorbisDSPContext *dsp){}
void ff_vorbisdsp_init_ppc(struct VorbisDSPContext *dsp){}

// ff_vp3dsp_init
struct VP3DSPContext;
void ff_vp3dsp_init_arm(struct VP3DSPContext *c, int flags){}
void ff_vp3dsp_init_ppc(struct VP3DSPContext *c, int flags){}
// void ff_vp3dsp_init_x86(struct VP3DSPContext *c, int flags){}

// ff_vp6dsp_init
struct VP56DSPContext;
void ff_vp6dsp_init_arm(struct VP56DSPContext *s){}
// void ff_vp6dsp_init_x86(struct VP56DSPContext *s){}

// ff_vp78dsp_init
struct VP8DSPContext;
void ff_vp78dsp_init_arm(struct VP8DSPContext *c){}
void ff_vp78dsp_init_ppc(struct VP8DSPContext *c){}
// void ff_vp78dsp_init_x86(struct VP8DSPContext *c){}

// ff_vp8dsp_init
struct VP8DSPContext;
void ff_vp8dsp_init_arm(struct VP8DSPContext *c){}
// void ff_vp8dsp_init_x86(struct VP8DSPContext *c){}
void ff_vp8dsp_init_mips(struct VP8DSPContext *c){}

// ff_vp9dsp_init
struct VP9DSPContext;
void ff_vp9dsp_init_aarch64(struct VP9DSPContext *dsp, int bpp){}
void ff_vp9dsp_init_arm(struct VP9DSPContext *dsp, int bpp){}
// void ff_vp9dsp_init_x86(struct VP9DSPContext *dsp, int bpp, int bitexact){}
void ff_vp9dsp_init_mips(struct VP9DSPContext *dsp, int bpp){}

// ff_wmv2dsp_init
struct WMV2DSPContext;
void ff_wmv2dsp_init_mips(struct WMV2DSPContext *c){}

// ff_xvid_idct_init
struct IDCTDSPContext;
struct AVCodecContext;
// void ff_xvid_idct_init_x86(struct IDCTDSPContext *c, struct AVCodecContext *avctx, unsigned high_bit_depth){}
void ff_xvid_idct_init_mips(struct IDCTDSPContext *c, struct AVCodecContext *avctx, unsigned high_bit_depth){}

// ff_hevc_pred_init
struct HEVCPredContext;
void ff_hevc_pred_init_mips(struct HEVCPredContext *hpc, int bit_depth){}

// ff_fdctdsp_init_x86
#if !HAVE_MMX_INLINE
void ff_fdct_mmx(int16_t *block){}
#endif//HAVE_MMX_INLINE

// ff_fdctdsp_init_x86
#if !HAVE_MMXEXT_INLINE
void ff_fdct_mmxext(int16_t *block){}
#endif//HAVE_MMXEXT_INLINE

// ff_fdctdsp_init_x86
#if !HAVE_SSE2_INLINE
void ff_fdct_sse2(int16_t *block){}
#endif//HAVE_SSE2_INLINE

// ff_dcadsp_init_x86
#if !ARCH_X86_32
void ff_lfe_fir0_float_sse(float *pcm_samples, int32_t *lfe_samples, const float *filter_coeff, ptrdiff_t npcmblocks){}
#endif//ARCH_X86_32

// ff_huffyuvdsp_init_x86
#if !ARCH_X86_32
void ff_add_int16_mmx(uint16_t *dst, const uint16_t *src, unsigned mask, int w){}
#endif//ARCH_X86_32

// ff_huffyuvdsp_init_x86
#if !ARCH_X86_32
void ff_add_hfyu_left_pred_bgr32_mmx(uint8_t *dst, const uint8_t *src, intptr_t w, uint8_t *left){}
#endif//ARCH_X86_32

// ff_huffyuvencdsp_init_x86
#if !ARCH_X86_32
void ff_diff_int16_mmx(uint16_t *dst, const uint16_t *src1, const uint16_t *src2, unsigned mask, int w){}
#endif//ARCH_X86_32

// ff_llviddsp_init_x86
#if !ARCH_X86_32
void ff_add_bytes_mmx(uint8_t *dst, uint8_t *src, ptrdiff_t w){}
#endif//ARCH_X86_32

// ff_llviddsp_init_x86
#if !ARCH_X86_32
void ff_add_median_pred_mmxext(uint8_t *dst, const uint8_t *top, const uint8_t *diff, ptrdiff_t w, int *left, int *left_top){}
#endif//ARCH_X86_32

// ff_llvidencdsp_init_x86
#if !ARCH_X86_32
void ff_diff_bytes_mmx(uint8_t *dst, const uint8_t *src1, const uint8_t *src2, intptr_t w){}
#endif//ARCH_X86_32

// ff_rv34dsp_init_x86
#if !ARCH_X86_32
void ff_rv34_idct_dc_add_mmx(uint8_t *dst, ptrdiff_t stride, int dc){}
#endif//ARCH_X86_32

// ff_vc1dsp_init_x86
#if !HAVE_MMX_INLINE
struct VC1DSPContext;
void ff_vc1dsp_init_mmx(struct VC1DSPContext *dsp) {}
#endif//HAVE_MMX_INLINE

// ff_vc1dsp_init_x86
#if !HAVE_MMXEXT_INLINE
struct VC1DSPContext;
void ff_vc1dsp_init_mmxext(struct VC1DSPContext *dsp) {}
#endif//HAVE_MMXEXT_INLINE