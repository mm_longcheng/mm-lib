#include <crtdefs.h>
#include <stdint.h>
// ff_getSwsFunc
struct SwsContext;
void ff_sws_init_swscale_ppc(struct SwsContext *c){}
// void ff_sws_init_swscale_x86(struct SwsContext *c){}
void ff_sws_init_swscale_aarch64(struct SwsContext *c){}
void ff_sws_init_swscale_arm(struct SwsContext *c){}

// ff_get_unscaled_swscale
void ff_get_unscaled_swscale_ppc(struct SwsContext *c){}
void ff_get_unscaled_swscale_arm(struct SwsContext *c){}
void ff_get_unscaled_swscale_aarch64(struct SwsContext *c){}

// sws_setColorspaceDetails
void ff_yuv2rgb_init_tables_ppc(struct SwsContext *c, const int inv_table[4], int brightness, int contrast, int saturation){}

// ff_yuv2rgb_get_func_ptr
typedef int(*SwsFunc)(struct SwsContext *context, const uint8_t *src[], int srcStride[], int srcSliceY, int srcSliceH, uint8_t *dst[], int dstStride[]);

SwsFunc ff_yuv2rgb_init_ppc(struct SwsContext *c){	return NULL;	}