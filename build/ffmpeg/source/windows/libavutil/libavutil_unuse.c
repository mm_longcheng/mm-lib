#include <crtdefs.h>
// get_cpu_flags
int ff_get_cpu_flags_aarch64(void){	return 0;	}
int ff_get_cpu_flags_arm(void){	return 0;	}
int ff_get_cpu_flags_ppc(void){	return 0;	}
// int ff_get_cpu_flags_x86(void){	return 0;	}

// av_cpu_max_align
size_t ff_get_cpu_max_align_aarch64(void){	return 0;	}
size_t ff_get_cpu_max_align_arm(void){	return 0;	}
size_t ff_get_cpu_max_align_ppc(void){	return 0;	}
// size_t ff_get_cpu_max_align_x86(void){	return 0;	}
//avpriv_float_dsp_alloc
struct AVFloatDSPContext;
void ff_float_dsp_init_aarch64(struct AVFloatDSPContext *fdsp){}
void ff_float_dsp_init_arm(struct AVFloatDSPContext *fdsp){}
void ff_float_dsp_init_ppc(struct AVFloatDSPContext *fdsp, int strict){}
// void ff_float_dsp_init_x86(struct AVFloatDSPContext *fdsp){}
void ff_float_dsp_init_mips(struct AVFloatDSPContext *fdsp){}
