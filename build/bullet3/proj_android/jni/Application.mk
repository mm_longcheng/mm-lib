APP_ABI := arm64-v8a armeabi-v7a # armeabi

APP_STL := gnustl_shared 

APP_MODULES += libLinearMath_static
APP_MODULES += libLinearMath_shared


APP_MODULES += libBullet3Common_static
APP_MODULES += libBullet3Common_shared

APP_MODULES += libBullet3Collision_static
APP_MODULES += libBullet3Collision_shared

APP_MODULES += libBullet3Dynamics_static
APP_MODULES += libBullet3Dynamics_shared

APP_MODULES += libBullet3Geometry_static
APP_MODULES += libBullet3Geometry_shared

APP_MODULES += libBullet3OpenCL_clew_static
APP_MODULES += libBullet3OpenCL_clew_shared


APP_MODULES += libBulletCollision_static
APP_MODULES += libBulletCollision_shared

APP_MODULES += libBulletDynamics_static
APP_MODULES += libBulletDynamics_shared

APP_MODULES += libBulletSoftBody_static
APP_MODULES += libBulletSoftBody_shared

APP_MODULES += libBulletInverseDynamics_static
APP_MODULES += libBulletInverseDynamics_shared


APP_MODULES += libBulletInverseDynamicsUtils_static
APP_MODULES += libBulletInverseDynamicsUtils_shared

APP_MODULES += libBulletFileLoader_static
APP_MODULES += libBulletFileLoader_shared

APP_MODULES += libBulletWorldImporter_static
APP_MODULES += libBulletWorldImporter_shared

APP_MODULES += libBulletXmlWorldImporter_static
APP_MODULES += libBulletXmlWorldImporter_shared

APP_MODULES += libConvexDecomposition_static
APP_MODULES += libConvexDecomposition_shared

APP_MODULES += libGIMPACTUtils_static
APP_MODULES += libGIMPACTUtils_shared

APP_MODULES += libHACD_static
APP_MODULES += libHACD_shared

APP_MODULES += libBlenderSerialize_static
APP_MODULES += libBlenderSerialize_shared

APP_MODULES += libBullet2FileLoader_static
APP_MODULES += libBullet2FileLoader_shared
