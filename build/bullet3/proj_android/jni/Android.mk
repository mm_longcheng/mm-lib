LOCAL_PATH := $(call my-dir)

MM_MAKE_HOME ?= $(MM_HOME)/mm-make

include $(CLEAR_VARS)

include $(LOCAL_PATH)/libLinearMath_static.mk
include $(LOCAL_PATH)/libLinearMath_shared.mk


include $(LOCAL_PATH)/libBullet3Common_static.mk
include $(LOCAL_PATH)/libBullet3Common_shared.mk

include $(LOCAL_PATH)/libBullet3Collision_static.mk
include $(LOCAL_PATH)/libBullet3Collision_shared.mk

include $(LOCAL_PATH)/libBullet3Dynamics_static.mk
include $(LOCAL_PATH)/libBullet3Dynamics_shared.mk

include $(LOCAL_PATH)/libBullet3Geometry_static.mk
include $(LOCAL_PATH)/libBullet3Geometry_shared.mk

include $(LOCAL_PATH)/libBullet3OpenCL_clew_static.mk
include $(LOCAL_PATH)/libBullet3OpenCL_clew_shared.mk


include $(LOCAL_PATH)/libBulletCollision_static.mk
include $(LOCAL_PATH)/libBulletCollision_shared.mk

include $(LOCAL_PATH)/libBulletDynamics_static.mk
include $(LOCAL_PATH)/libBulletDynamics_shared.mk


include $(LOCAL_PATH)/libBulletSoftBody_static.mk
include $(LOCAL_PATH)/libBulletSoftBody_shared.mk

include $(LOCAL_PATH)/libBulletInverseDynamics_static.mk
include $(LOCAL_PATH)/libBulletInverseDynamics_shared.mk


include $(LOCAL_PATH)/libBulletInverseDynamicsUtils_static.mk
include $(LOCAL_PATH)/libBulletInverseDynamicsUtils_shared.mk

include $(LOCAL_PATH)/libBulletFileLoader_static.mk
include $(LOCAL_PATH)/libBulletFileLoader_shared.mk

include $(LOCAL_PATH)/libBulletWorldImporter_static.mk
include $(LOCAL_PATH)/libBulletWorldImporter_shared.mk

include $(LOCAL_PATH)/libBulletXmlWorldImporter_static.mk
include $(LOCAL_PATH)/libBulletXmlWorldImporter_shared.mk

include $(LOCAL_PATH)/libConvexDecomposition_static.mk
include $(LOCAL_PATH)/libConvexDecomposition_shared.mk

include $(LOCAL_PATH)/libGIMPACTUtils_static.mk
include $(LOCAL_PATH)/libGIMPACTUtils_shared.mk

include $(LOCAL_PATH)/libHACD_static.mk
include $(LOCAL_PATH)/libHACD_shared.mk

include $(LOCAL_PATH)/libBlenderSerialize_static.mk
include $(LOCAL_PATH)/libBlenderSerialize_shared.mk

include $(LOCAL_PATH)/libBullet2FileLoader_static.mk
include $(LOCAL_PATH)/libBullet2FileLoader_shared.mk

