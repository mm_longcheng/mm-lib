@echo off 

:: mkdir datafiles
IF NOT EXIST "datafiles" MD "datafiles"

:: ln -s
call:ln-s /j "bin/data" "../../../src/bullet3/data"
GOTO:EOF

:: ln-s mode target source
:ln-s  
IF NOT EXIST %~2 mklink %~1 "%~2" "%~3"
GOTO:EOF
