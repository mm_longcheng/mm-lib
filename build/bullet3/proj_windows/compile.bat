@echo off 

set VSVARS="%VS141COMNTOOLS%vcvars64.bat"
set VC_VER=141

call %VSVARS% 1>nul

devenv %~dp0/bullet3.sln /build "Debug|x64"
devenv %~dp0/bullet3.sln /build "Release|x64"

pause