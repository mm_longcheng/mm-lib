LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libOpenAL_static
LOCAL_MODULE_FILENAME := libOpenAL_static
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -D__ANDROID__

LOCAL_CFLAGS += -Wall
LOCAL_CFLAGS += -Wno-unused-variable

LOCAL_CFLAGS += -mfloat-abi=softfp 
LOCAL_CFLAGS += -mfpu=neon

# ifeq ($(TARGET_ARCH_ABI),arm64-v8a)
#     LOCAL_CFLAGS += -DHAVE_LOG2F
# else
#     LOCAL_CFLAGS += -DHAVE_LOG2F
# endif
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/OpenAL/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/OpenAL/Alc
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/OpenAL/common
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/OpenAL/OpenAL32/Include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/android
########################################################################
LOCAL_SRC_FILES  += ../../../../src/OpenAL/Alc/backends/base.c
LOCAL_SRC_FILES  += ../../../../src/OpenAL/Alc/backends/opensl.c
LOCAL_SRC_FILES  += ../../../../src/OpenAL/Alc/backends/wave.c
LOCAL_SRC_FILES  += ../../../../src/OpenAL/Alc/backends/loopback.c
LOCAL_SRC_FILES  += ../../../../src/OpenAL/Alc/backends/null.c
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/OpenAL/Alc
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/OpenAL/common
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/OpenAL/OpenAL32
# MY_SOURCES_PATH += $(LOCAL_PATH)/android

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/OpenAL/Alc/backends%
MY_SOURCES_FILTER_OUT += ../../../../src/OpenAL/Alc/mixer_sse.c
MY_SOURCES_FILTER_OUT += ../../../../src/OpenAL/Alc/mixer_sse2.c
MY_SOURCES_FILTER_OUT += ../../../../src/OpenAL/Alc/mixer_sse3.c
MY_SOURCES_FILTER_OUT += ../../../../src/OpenAL/Alc/mixer_sse41.c
MY_SOURCES_FILTER_OUT += ../../../../src/OpenAL/Alc/mixer_inc.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################