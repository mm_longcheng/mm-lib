@echo off 

set VSVARS="%VS141COMNTOOLS%vcvars64.bat"
set VC_VER=141

call %VSVARS% 1>nul

devenv %~dp0/ParticleUniverse.sln /build "Debug|x64"   /Project libPlugin_ParticleUniverse_shared
devenv %~dp0/ParticleUniverse.sln /build "Release|x64" /Project libPlugin_ParticleUniverse_shared

devenv %~dp0/ParticleUniverse.sln /build "Debug|x64"   /Project libPlugin_ParticleUniverse_static
devenv %~dp0/ParticleUniverse.sln /build "Release|x64" /Project libPlugin_ParticleUniverse_static

devenv %~dp0/ParticleUniverse.sln /build "Debug|x64"   /Project AtlasImageTool
devenv %~dp0/ParticleUniverse.sln /build "Release|x64" /Project AtlasImageTool

devenv %~dp0/ParticleUniverse.sln /build "Debug|x64"   /Project libSample_ParticleUniverseDemo
devenv %~dp0/ParticleUniverse.sln /build "Release|x64" /Project libSample_ParticleUniverseDemo

pause