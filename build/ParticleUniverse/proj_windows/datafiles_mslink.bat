@echo off 

:: mkdir datafiles
IF NOT EXIST "bin" MD "bin"
IF NOT EXIST "bin/Debug"   MD "bin/Debug"
IF NOT EXIST "bin/Release" MD "bin/Release"

:: ln -s
call:ln-s /j "bin/Debug/media" "../../../src/ParticleUniverse/Particle Universe Editor/media"
call:ln-s /j "bin/Debug/language" "../../../src/ParticleUniverse/Particle Universe Editor/language"
call:ln-s /j "bin/Debug/manual" "../../../src/ParticleUniverse/Particle Universe Editor/manual"

call:ln-s /j "bin/Release/media" "../../../src/ParticleUniverse/Particle Universe Editor/media"
call:ln-s /j "bin/Release/language" "../../../src/ParticleUniverse/Particle Universe Editor/language"
call:ln-s /j "bin/Release/manual" "../../../src/ParticleUniverse/Particle Universe Editor/manual"

GOTO:EOF

:: ln-s mode target source
:ln-s  
IF NOT EXIST %~2 mklink %~1 "%~2" "%~3"
GOTO:EOF
