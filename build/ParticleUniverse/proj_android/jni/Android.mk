LOCAL_PATH := $(call my-dir)

MM_MAKE_HOME ?= $(MM_HOME)/mm-make

include $(LOCAL_PATH)/lib_prebuild.mk
include $(CLEAR_VARS)

include $(LOCAL_PATH)/libPlugin_ParticleUniverse_static.mk
include $(LOCAL_PATH)/libPlugin_ParticleUniverse_shared.mk
