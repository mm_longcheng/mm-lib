LOCAL_PATH := $(call my-dir)

MM_MAKE_HOME ?= $(MM_HOME)/mm-make

include $(CLEAR_VARS)

include $(LOCAL_PATH)/lib_prebuild.mk

include $(LOCAL_PATH)/libcurl_static.mk
include $(LOCAL_PATH)/libcurl_shared.mk