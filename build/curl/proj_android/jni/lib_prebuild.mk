# prebuild.mk
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libz_shared
LOCAL_SRC_FILES := ../../../zlib/proj_android/libs/$(TARGET_ARCH_ABI)/libz_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libz_static
LOCAL_SRC_FILES := ../../../zlib/proj_android/obj/local/$(TARGET_ARCH_ABI)/libz_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libcrypto_shared
LOCAL_SRC_FILES := ../../../openssl/proj_android/libs/$(TARGET_ARCH_ABI)/libcrypto_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libcrypto_static
LOCAL_SRC_FILES := ../../../openssl/proj_android/obj/local/$(TARGET_ARCH_ABI)/libcrypto_static.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)  
LOCAL_MODULE := libssl_shared
LOCAL_SRC_FILES := ../../../openssl/proj_android/libs/$(TARGET_ARCH_ABI)/libssl_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libssl_static
LOCAL_SRC_FILES := ../../../openssl/proj_android/obj/local/$(TARGET_ARCH_ABI)/libssl_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := librtmp_shared
LOCAL_SRC_FILES := ../../../rtmpdump/proj_android/libs/$(TARGET_ARCH_ABI)/librtmp_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := librtmp_static
LOCAL_SRC_FILES := ../../../rtmpdump/proj_android/obj/local/$(TARGET_ARCH_ABI)/librtmp_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
