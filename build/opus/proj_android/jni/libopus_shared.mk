LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libopus_shared
LOCAL_MODULE_FILENAME := libopus_shared
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -D__ANDROID__

LOCAL_CFLAGS += -Wall
LOCAL_CFLAGS += -Wno-incompatible-pointer-types

LOCAL_CFLAGS += -DHAVE_CONFIG_H
LOCAL_CFLAGS += -DFIXED_POINT=1
# LOCAL_CFLAGS += -mfpu=neon
########################################################################
LOCAL_LDLIBS += -fPIC
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/opus
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/opus/silk
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/opus/silk/fixed
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/opus/silk/float
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/opus/celt
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/opus/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/android
########################################################################
LOCAL_SRC_FILES  += 
# LOCAL_SRC_FILES  += ../../../../src/opus/celt/arm/celt_pitch_xcorr_arm.s
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/opus/src
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/opus/silk
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/opus/celt
MY_SOURCES_PATH += $(LOCAL_PATH)/android

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/opus/celt/mips%
MY_SOURCES_FILTER_OUT += ../../../../src/opus/celt/tests%
MY_SOURCES_FILTER_OUT += ../../../../src/opus/celt/x86%

MY_SOURCES_FILTER_OUT += ../../../../src/opus/silk/mips%
MY_SOURCES_FILTER_OUT += ../../../../src/opus/silk/tests%
MY_SOURCES_FILTER_OUT += ../../../../src/opus/silk/x86%

MY_SOURCES_FILTER_OUT += ../../../../src/opus/silk/fixed/x86%
MY_SOURCES_FILTER_OUT += ../../../../src/opus/silk/fixed/mips%

# MY_SOURCES_FILTER_OUT += ../../../../src/opus/silk/fixed%

MY_SOURCES_FILTER_OUT += ../../../../src/opus/silk/float%

MY_SOURCES_FILTER_OUT += ../../../../src/opus/src/opus_demo.c
MY_SOURCES_FILTER_OUT += ../../../../src/opus/src/opus_compare.c
MY_SOURCES_FILTER_OUT += ../../../../src/opus/src/repacketizer_demo

MY_SOURCES_FILTER_OUT += ../../../../src/opus/silk/arm/LPC_inv_pred_gain_neon_intr.c
MY_SOURCES_FILTER_OUT += ../../../../src/opus/silk/arm/NSQ_del_dec_neon_intr.c
MY_SOURCES_FILTER_OUT += ../../../../src/opus/silk/arm/NSQ_neon.c
MY_SOURCES_FILTER_OUT += ../../../../src/opus/silk/arm/biquad_alt_neon_intr.c

MY_SOURCES_FILTER_OUT += ../../../../src/opus/silk/fixed/arm/warped_autocorrelation_FIX_neon_intr.c

MY_SOURCES_FILTER_OUT += ../../../../src/opus/celt/opus_custom_demo.c

MY_SOURCES_FILTER_OUT += ../../../../src/opus/celt/arm/celt_ne10_fft.c
MY_SOURCES_FILTER_OUT += ../../../../src/opus/celt/arm/celt_neon_intr.c
MY_SOURCES_FILTER_OUT += ../../../../src/opus/celt/arm/pitch_neon_intr.c

MY_SOURCES_FILTER_OUT += ../../../../src/opus/celt/arm/celt_pitch_xcorr_arm-gnu.S

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################