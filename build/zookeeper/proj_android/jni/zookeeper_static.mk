LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libzookeeper_static
LOCAL_MODULE_FILENAME := libzookeeper_static
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -D__ANDROID__

LOCAL_CFLAGS += -Wall
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/zookeeper/src/c/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/zookeeper/src/c/generated
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/zookeeper/src/c/src
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/android
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
# MY_SOURCES_PATH += $(LOCAL_PATH)/android
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/zookeeper/src/c/src
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/zookeeper/src/c/generated
MY_SOURCES_PATH += $(LOCAL_PATH)/android

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/zookeeper/src/c/src/st_adaptor.c
MY_SOURCES_FILTER_OUT += ../../../../src/zookeeper/src/c/src/load_gen.c
MY_SOURCES_FILTER_OUT += ../../../../src/zookeeper/src/c/src/cli.c
MY_SOURCES_FILTER_OUT += ../../../../src/zookeeper/src/c/src/winport.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################