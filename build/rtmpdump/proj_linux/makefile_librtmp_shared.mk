LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)
########################################################################
LOCAL_MODULE := librtmp_shared
LOCAL_MODULE_FILENAME := librtmp
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -Wall
LOCAL_CFLAGS += -D__FLOAT_WORD_ORDER=__BYTE_ORDER
LOCAL_CFLAGS += -Wno-unused-but-set-variable

LOCAL_ASMFLAGS +=
########################################################################
LOCAL_LDLIBS += -fPIC
########################################################################
LOCAL_SHARED_LIBRARIES += crypto_shared
LOCAL_SHARED_LIBRARIES += ssl_shared
LOCAL_SHARED_LIBRARIES += zlib_shared
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/rtmpdump
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/rtmpdump/librtmp

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/openssl
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/openssl/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../build/openssl/include/$(TARGET_PLATFORM)
########################################################################
# LOCAL_SRC_FILES  += 
########################################################################
#  
# config self source file path ,suffix.
MY_SOURCES_PATH := ../../../src/rtmpdump/librtmp

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c

MY_SOURCES_EXTENSION := .cpp .c .cc .S .asm
include $(SOURCE_RWILDCARD)
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################
# $(modules-dump-database)
