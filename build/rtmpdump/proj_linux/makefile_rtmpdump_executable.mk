LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)
########################################################################
LOCAL_MODULE := rtmpdump_executable
LOCAL_MODULE_FILENAME := rtmpdump
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -DRTMPDUMP_VERSION=\"v2.3\"

LOCAL_ASMFLAGS += 
########################################################################
LOCAL_LDLIBS += -fPIC
LOCAL_LDLIBS += -ldl
LOCAL_LDLIBS += -lpthread
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += librtmp_static
LOCAL_STATIC_LIBRARIES += ssl_static
LOCAL_STATIC_LIBRARIES += crypto_static
LOCAL_STATIC_LIBRARIES += zlib_static
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/rtmpdump
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/rtmpdump/librtmp
########################################################################
LOCAL_SRC_FILES += ../../../src/rtmpdump/rtmpdump.c
LOCAL_SRC_FILES += ../../../src/rtmpdump/thread.c
########################################################################
#  
# config self source file path ,suffix.
# MY_SOURCES_PATH := $(LOCAL_PATH)/../../../src/x264

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c

MY_SOURCES_EXTENSION := .cpp .c .cc .S .asm
include $(SOURCE_RWILDCARD)
########################################################################
include $(BUILD_EXECUTABLE)
########################################################################
# $(modules-dump-database)
