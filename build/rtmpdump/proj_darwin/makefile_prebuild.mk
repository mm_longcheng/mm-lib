################################################################################
# prebuild.mk
################################################################################
LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := crypto_shared
LOCAL_SRC_FILES := $(LOCAL_PATH)/../../openssl/proj_$(TARGET_PLATFORM)/bin/libcrypto.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)  
LOCAL_MODULE := crypto_static
LOCAL_SRC_FILES := $(LOCAL_PATH)/../../openssl/proj_$(TARGET_PLATFORM)/bin/libcrypto.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)  
LOCAL_MODULE := ssl_shared
LOCAL_SRC_FILES := $(LOCAL_PATH)/../../openssl/proj_$(TARGET_PLATFORM)/bin/libssl.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)  
LOCAL_MODULE := ssl_static
LOCAL_SRC_FILES := $(LOCAL_PATH)/../../openssl/proj_$(TARGET_PLATFORM)/bin/libssl.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := zlib_shared
LOCAL_SRC_FILES := $(LOCAL_PATH)/../../zlib/proj_$(TARGET_PLATFORM)/bin/libz.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)  
LOCAL_MODULE := zlib_static
LOCAL_SRC_FILES := $(LOCAL_PATH)/../../zlib/proj_$(TARGET_PLATFORM)/bin/libz.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################