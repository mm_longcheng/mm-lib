################################################################################
# prebuild.mk
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libcrypto_shared
LOCAL_SRC_FILES := ../../../openssl/proj_android/libs/$(TARGET_ARCH_ABI)/libcrypto_shared.so
include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)  
LOCAL_MODULE := libssl_shared
LOCAL_SRC_FILES := ../../../openssl/proj_android/libs/$(TARGET_ARCH_ABI)/libssl_shared.so
include $(PREBUILT_SHARED_LIBRARY)
################################################################################