LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)
########################################################################
LOCAL_MODULE := libOIS_static
LOCAL_MODULE_FILENAME := libOIS
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -fexceptions
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/ois/includes
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/ois/src

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../src/ois/src/extras/%
MY_SOURCES_FILTER_OUT += ../../../src/ois/src/iphone/%
MY_SOURCES_FILTER_OUT += ../../../src/ois/src/linux/%
MY_SOURCES_FILTER_OUT += ../../../src/ois/src/mac/%
MY_SOURCES_FILTER_OUT += ../../../src/ois/src/SDL/%
MY_SOURCES_FILTER_OUT += ../../../src/ois/src/win32/%

MY_SOURCES_FILTER_OUT += ../../../src/ois/src/OISObject.cpp

MY_SOURCES_EXTENSION += .cpp .c .cc .S
include $(SOURCE_RWILDCARD)
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################
# $(modules-dump-database)
