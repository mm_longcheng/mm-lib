LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)
########################################################################
LOCAL_MODULE := libiconv_shared
LOCAL_MODULE_FILENAME := libiconv
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -DLIBDIR=\"/usr/local/lib\"
LOCAL_CFLAGS += -DBUILDING_LIBICONV
LOCAL_CFLAGS += -DIN_LIBRARY
LOCAL_CFLAGS += -DHAVE_CONFIG_H

LOCAL_ASMFLAGS += 
########################################################################
LOCAL_LDLIBS += -fPIC
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../source/$(TARGET_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../source/$(TARGET_PLATFORM)/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../source/$(TARGET_PLATFORM)/libcharset/lib
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../source/$(TARGET_PLATFORM)/libcharset/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../source/$(TARGET_PLATFORM)/libcharset
########################################################################
# source
LOCAL_SRC_FILES  += ../../../src/libiconv/libcharset/lib/localcharset.c
# LOCAL_SRC_FILES  += ../../../src/libiconv/libcharset/lib/relocatable.c
LOCAL_SRC_FILES  += ../../../src/libiconv/lib/iconv.c
########################################################################
#  
# config self source file path ,suffix.
MY_SOURCES_PATH := 

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c

MY_SOURCES_EXTENSION := .cpp .c .cc .S .asm
include $(SOURCE_RWILDCARD)
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################
# $(modules-dump-database)
