LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libiconv_shared
LOCAL_MODULE_FILENAME := libiconv_shared
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -D__ANDROID__

LOCAL_CFLAGS += -Wall
LOCAL_CFLAGS += -Wno-tautological-compare
LOCAL_CFLAGS += -Wno-parentheses-equality
LOCAL_CFLAGS += -Wno-static-in-inline

LOCAL_CFLAGS += -DAndroid
LOCAL_CFLAGS += -DLIBDIR=\"/usr/local/lib\"
LOCAL_CFLAGS += -DBUILDING_LIBICONV
LOCAL_CFLAGS += -DIN_LIBRARY
LOCAL_CFLAGS += -DHAVE_CONFIG_H
########################################################################
LOCAL_LDLIBS += -fPIC
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../source/android
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../source/android/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../source/android/libcharset/lib
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../source/android/libcharset/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../source/android/libcharset
########################################################################
LOCAL_SRC_FILES  += ../../../../src/libiconv/libcharset/lib/localcharset.c
LOCAL_SRC_FILES  += ../../../../src/libiconv/libcharset/lib/relocatable.c
LOCAL_SRC_FILES  += ../../../../src/libiconv/lib/iconv.c
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/android

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################