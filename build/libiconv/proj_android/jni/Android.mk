LOCAL_PATH := $(call my-dir)

MM_MAKE_HOME ?= $(MM_HOME)/mm-make

include $(CLEAR_VARS)

include $(LOCAL_PATH)/libiconv_static.mk
include $(LOCAL_PATH)/libiconv_shared.mk