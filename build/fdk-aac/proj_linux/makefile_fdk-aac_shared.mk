LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)
########################################################################
LOCAL_MODULE := fdk-aac_shared
LOCAL_MODULE_FILENAME := libfdk-aac
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -Wall
LOCAL_CFLAGS += -Wno-unused-function
LOCAL_CFLAGS += -Wno-unused-but-set-variable
LOCAL_CFLAGS += -Wno-sign-compare
LOCAL_CFLAGS += -Wno-unused-variable
LOCAL_CFLAGS += -Wno-unused-label

LOCAL_ASMFLAGS +=
########################################################################
LOCAL_LDLIBS += -fPIC
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/fdk-aac
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/fdk-aac/libFDK/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/fdk-aac/libPCMutils/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/fdk-aac/libSYS/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/fdk-aac/libAACdec/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/fdk-aac/libAACenc/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/fdk-aac/libMpegTPDec/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/fdk-aac/libMpegTPEnc/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/fdk-aac/libSBRdec/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/fdk-aac/libSBRenc/include
########################################################################
# LOCAL_SRC_FILES  += 
########################################################################
#  
# config self source file path ,suffix.
MY_SOURCES_PATH := ../../../src/fdk-aac

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c

MY_SOURCES_FILTER_OUT += ../../../src/fdk-aac/libFDK/src/arm/%
MY_SOURCES_FILTER_OUT += ../../../src/fdk-aac/libFDK/src/mips/%

MY_SOURCES_FILTER_OUT += ../../../src/fdk-aac/libSYS/src/linux/%
MY_SOURCES_FILTER_OUT += ../../../src/fdk-aac/libSYS/src/mips/%

MY_SOURCES_FILTER_OUT += ../../../src/fdk-aac/libSBRdec/src/arm/%

MY_SOURCES_FILTER_OUT += ../../../src/fdk-aac/libAACdec/src/arm/%

MY_SOURCES_FILTER_OUT += ../../../src/fdk-aac/libFDK/src/scale.cpp
MY_SOURCES_FILTER_OUT += ../../../src/fdk-aac/libSBRdec/src/sbr_deb.cpp

MY_SOURCES_EXTENSION := .cpp .c .cc .S .asm
include $(SOURCE_RWILDCARD)
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################
# $(modules-dump-database)
