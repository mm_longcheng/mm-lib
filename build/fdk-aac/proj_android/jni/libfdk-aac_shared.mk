LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libfdk-aac_shared
LOCAL_MODULE_FILENAME := libfdk-aac_shared
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -D__ANDROID__

LOCAL_CFLAGS += -Wall
LOCAL_CFLAGS += -Wno-constant-logical-operand
LOCAL_CFLAGS += -Wno-unused-const-variable
LOCAL_CFLAGS += -Wno-self-assign
LOCAL_CFLAGS += -Wno-unused-function
LOCAL_CFLAGS += -Wno-unused-label
LOCAL_CFLAGS += -Wno-unused-variable
########################################################################
LOCAL_LDLIBS += -fPIC
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/fdk-aac
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/fdk-aac/libFDK/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/fdk-aac/libPCMutils/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/fdk-aac/libSYS/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/fdk-aac/libAACdec/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/fdk-aac/libAACenc/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/fdk-aac/libMpegTPDec/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/fdk-aac/libMpegTPEnc/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/fdk-aac/libSBRdec/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/fdk-aac/libSBRenc/include
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/fdk-aac
MY_SOURCES_PATH += $(LOCAL_PATH)/android

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/fdk-aac/libFDK/src/arm/%
MY_SOURCES_FILTER_OUT += ../../../../src/fdk-aac/libFDK/src/mips/%

MY_SOURCES_FILTER_OUT += ../../../../src/fdk-aac/libSYS/src/linux/%
MY_SOURCES_FILTER_OUT += ../../../../src/fdk-aac/libSYS/src/mips/%

MY_SOURCES_FILTER_OUT += ../../../../src/fdk-aac/libSBRdec/src/arm/%

MY_SOURCES_FILTER_OUT += ../../../../src/fdk-aac/libAACdec/src/arm/%

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################