LOCAL_PATH := $(call my-dir)

MM_MAKE_HOME ?= $(MM_HOME)/mm-make

include $(LOCAL_PATH)/lib_prebuild.mk

include $(LOCAL_PATH)/libCEGUIMain_static.mk
include $(LOCAL_PATH)/libCEGUIMain_shared.mk

include $(LOCAL_PATH)/libCEGUICommonDialogs_static.mk
include $(LOCAL_PATH)/libCEGUICommonDialogs_shared.mk

include $(LOCAL_PATH)/libCEGUIImageCodecFreeImage_static.mk
include $(LOCAL_PATH)/libCEGUIImageCodecFreeImage_shared.mk

include $(LOCAL_PATH)/libCEGUIRendererOgre_static.mk
include $(LOCAL_PATH)/libCEGUIRendererOgre_shared.mk

include $(LOCAL_PATH)/libCEGUICoreWindowRendererSet_static.mk
include $(LOCAL_PATH)/libCEGUICoreWindowRendererSet_shared.mk

include $(LOCAL_PATH)/libCEGUIXMLParserTinyXML_static.mk
include $(LOCAL_PATH)/libCEGUIXMLParserTinyXML_shared.mk

# include $(LOCAL_PATH)/libCEGUI_static.mk
# include $(LOCAL_PATH)/libCEGUI_shared.mk