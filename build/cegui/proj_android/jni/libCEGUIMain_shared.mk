LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libCEGUIMain_shared
LOCAL_MODULE_FILENAME := libCEGUIMain_shared
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -D__ANDROID__

LOCAL_CFLAGS += -Wall
LOCAL_CFLAGS += -Wno-unknown-warning-option
LOCAL_CFLAGS += -Wno-unused-variable
LOCAL_CFLAGS += -Wno-unused-but-set-variable
LOCAL_CFLAGS += -Wno-maybe-uninitialized
LOCAL_CFLAGS += -Wno-format
LOCAL_CFLAGS += -Wno-undefined-var-template

LOCAL_CXXFLAGS += -fexceptions 
LOCAL_CXXFLAGS += -frtti
########################################################################
LOCAL_LDLIBS += -fPIC
LOCAL_LDLIBS += -llog
########################################################################
LOCAL_SHARED_LIBRARIES += libpcre_shared
LOCAL_SHARED_LIBRARIES += libfreetype_shared
LOCAL_SHARED_LIBRARIES += libiconv_shared
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/android
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/cegui/cegui/include

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/freetype/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/pcre
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../pcre/include/android

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/libiconv/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../build/libiconv/include/android
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/cegui/cegui/src
MY_SOURCES_PATH += $(LOCAL_PATH)/android

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/cegui/cegui/src/ImageCodecModules%
MY_SOURCES_FILTER_OUT += ../../../../src/cegui/cegui/src/implementations%
MY_SOURCES_FILTER_OUT += ../../../../src/cegui/cegui/src/RendererModules%
MY_SOURCES_FILTER_OUT += ../../../../src/cegui/cegui/src/ScriptModules%
MY_SOURCES_FILTER_OUT += ../../../../src/cegui/cegui/src/WindowRendererSets%
MY_SOURCES_FILTER_OUT += ../../../../src/cegui/cegui/src/XMLParserModules%

MY_SOURCES_FILTER_OUT += ../../../../src/cegui/cegui/src/MinizipResourceProvider.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/cegui/cegui/src/Win32StringTranscoder.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/cegui/cegui/src/minibidi.cpp

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################