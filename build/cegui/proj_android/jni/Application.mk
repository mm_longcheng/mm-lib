APP_ABI := arm64-v8a armeabi-v7a # armeabi

# NDK_TOOLCHAIN_VERSION := 4.8

APP_STL := gnustl_shared

APP_MODULES += libCEGUIMain_static
APP_MODULES += libCEGUIMain_shared

APP_MODULES += libCEGUICommonDialogs_static
APP_MODULES += libCEGUICommonDialogs_shared

APP_MODULES += libCEGUIImageCodecFreeImage_static
APP_MODULES += libCEGUIImageCodecFreeImage_shared

APP_MODULES += libCEGUIRendererOgre_static
APP_MODULES += libCEGUIRendererOgre_shared

APP_MODULES += libCEGUICoreWindowRendererSet_static
APP_MODULES += libCEGUICoreWindowRendererSet_shared

APP_MODULES += libCEGUIXMLParserTinyXML_static
APP_MODULES += libCEGUIXMLParserTinyXML_shared

# APP_MODULES += libCEGUI_static
# APP_MODULES += libCEGUI_shared