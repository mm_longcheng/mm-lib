@echo off 

:: mkdir datafiles
IF NOT EXIST "datafiles" MD "datafiles"

:: ln -s
call:ln-s /j "datafiles/animations" "../../../src/cegui/datafiles/animations"
call:ln-s /j "datafiles/fonts" "../../../src/cegui/datafiles/fonts"
call:ln-s /j "datafiles/imagesets" "../../../src/cegui/datafiles/imagesets"
call:ln-s /j "datafiles/layouts" "../../../src/cegui/datafiles/layouts"
call:ln-s /j "datafiles/looknfeel" "../../../src/cegui/datafiles/looknfeel"
call:ln-s /j "datafiles/lua_scripts" "../../../src/cegui/datafiles/lua_scripts"
call:ln-s /j "datafiles/schemes" "../../../src/cegui/datafiles/schemes"
call:ln-s /j "datafiles/xml_schemas" "../../../src/cegui/datafiles/xml_schemas"

call:ln-s /j "datafiles/samples" "source/datafiles/samples"
GOTO:EOF

:: ln-s mode target source
:ln-s  
IF NOT EXIST %~2 mklink %~1 "%~2" "%~3"
GOTO:EOF
