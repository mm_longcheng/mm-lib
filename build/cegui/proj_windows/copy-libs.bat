:: copylibs.bat 32 x86 Debug   "_d"
:: copylibs.bat 32 x86 Release ""
:: copylibs.bat 64 x64 Debug   "_d"
:: copylibs.bat 64 x64 Release ""

@echo off

@set OPTION=/s /e /h /d /y

call :cp-libs %3 %4 %1 %2
call datafiles-mslink.bat %3

GOTO:EOF

:: cp-libs Debug _d 32 x86
:cp-libs
call :cp-lib  mm-lib  %~1 lua              liblua_shared%~2.dll
call :cp-lib  mm-lib  %~1 toluapp          libtolua++_shared%~2.dll
call :cp-lib  mm-lib  %~1 pcre             libpcre_shared%~2.dll
call :cp-lib  mm-lib  %~1 freetype         libfreetype_shared%~2.dll
call :cp-lib  mm-lib  %~1 libiconv         libiconv_shared%~2.dll
call :cp-lib  mm-lib  %~1 zlib             libz_shared%~2.dll
call :cp-lib  mm-lib  %~1 zziplib          libzzip_shared%~2.dll
call :cp-lib  mm-lib  %~1 FreeImage        libFreeImage_shared%~2.dll
call :cp-lib  mm-lib  %~1 Ogre             libOgreMain_shared%~2.dll
call :cp-lib  mm-lib  %~1 Ogre             libOgreRTShaderSystem_shared%~2.dll
call :cp-lib  mm-lib  %~1 Ogre             libPlugin*.dll
call :cp-lib  mm-lib  %~1 Ogre             libRenderSystem*.dll
call :cp-lib  mm-lib  %~1 OIS              libOIS_shared%~2.dll

@xcopy %POWERVR_SDK_HOME%\\Builds\\Windows\\x86_%~3\\Lib\*.dll bin\\%~1\* %OPTION%
@xcopy %MM_HOME%\\mm-lib\\sdk\\Cg\\bin\\windows\\%~4\cg.dll    bin\\%~1\* %OPTION%
@xcopy ..\\resource\\windows\\%~1\*.cfg    bin\\%~1\* %OPTION%
GOTO:EOF

:: cp-lib mm-lib Debug pthread libpthread_shared.dll
:cp-lib
@xcopy %MM_HOME%\\%~1\\build\\%~3\\proj_windows\\bin\\%~2\%~4 bin\\%~2\* %OPTION%
GOTO:EOF

:: cp-core mm-core Debug mm libmm_core_shared.dll
:cp-core
@xcopy %MM_HOME%\\%~1\\%~3\\proj\\windows\\bin\\%~2\%~4 bin\\%~2\* %OPTION%
GOTO:EOF
