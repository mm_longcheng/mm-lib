APP_ABI := arm64-v8a armeabi-v7a # armeabi

APP_SHORT_COMMANDS := true

APP_STL := gnustl_shared

APP_MODULES += libJPEG_static
APP_MODULES += libJPEG_shared

APP_MODULES += libOpenJPEG_static
APP_MODULES += libOpenJPEG_shared

APP_MODULES += libPNG_static
APP_MODULES += libPNG_shared

APP_MODULES += libRawLite_static
APP_MODULES += libRawLite_shared

APP_MODULES += libTIFF4_static
APP_MODULES += libTIFF4_shared

APP_MODULES += libWebP_static
APP_MODULES += libWebP_shared

# APP_MODULES += libJXR_static
# APP_MODULES += libJXR_shared

APP_MODULES += libOpenEXR_static
APP_MODULES += libOpenEXR_shared

APP_MODULES += libFreeImage_shared
APP_MODULES += libFreeImage_static