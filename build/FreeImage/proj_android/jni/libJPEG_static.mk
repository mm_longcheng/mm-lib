LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libJPEG_static
LOCAL_MODULE_FILENAME := libJPEG_static
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -D__ANDROID__

LOCAL_CFLAGS += -Wall
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/LibJPEG
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/FreeImage/Source/LibJPEG
# MY_SOURCES_PATH += $(LOCAL_PATH)/android

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibJPEG/jmemdos.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibJPEG/jmemmac.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibJPEG/jmemname.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibJPEG/jmemansi.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibJPEG/ckconfig.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibJPEG/ansi2knr.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibJPEG/example.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibJPEG/cjpeg.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibJPEG/cdjpeg.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibJPEG/ckconfig.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibJPEG/jpegtran.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibJPEG/djpeg.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibJPEG/rdjpgcom.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibJPEG/wrjpgcom.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibJPEG/rdrle.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibJPEG/wrrle.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################