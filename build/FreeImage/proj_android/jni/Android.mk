LOCAL_PATH := $(call my-dir)

MM_MAKE_HOME ?= $(MM_HOME)/mm-make

LOCAL_SHORT_COMMANDS := true

include $(LOCAL_PATH)/lib_prebuild.mk

include $(LOCAL_PATH)/libJPEG_static.mk
include $(LOCAL_PATH)/libJPEG_shared.mk

include $(LOCAL_PATH)/libOpenJPEG_static.mk
include $(LOCAL_PATH)/libOpenJPEG_shared.mk

include $(LOCAL_PATH)/libPNG_static.mk
include $(LOCAL_PATH)/libPNG_shared.mk

include $(LOCAL_PATH)/libRawLite_static.mk
include $(LOCAL_PATH)/libRawLite_shared.mk

include $(LOCAL_PATH)/libTIFF4_static.mk
include $(LOCAL_PATH)/libTIFF4_shared.mk

include $(LOCAL_PATH)/libWebP_static.mk
include $(LOCAL_PATH)/libWebP_shared.mk

# include $(LOCAL_PATH)/libJXR_static.mk
# include $(LOCAL_PATH)/libJXR_shared.mk

include $(LOCAL_PATH)/libOpenEXR_static.mk
include $(LOCAL_PATH)/libOpenEXR_shared.mk

include $(LOCAL_PATH)/libFreeImage_shared.mk
include $(LOCAL_PATH)/libFreeImage_static.mk

$(call import-module,android/cpufeatures)