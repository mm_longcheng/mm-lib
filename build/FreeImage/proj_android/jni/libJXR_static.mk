LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libJXR_static
LOCAL_MODULE_FILENAME := libJXR_static
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -D__ANDROID__


LOCAL_CFLAGS += -D__ANSI__ 
LOCAL_CFLAGS += -D_PREFAST_ 
LOCAL_CFLAGS += -D_ARM_

LOCAL_CFLAGS += -Wall
LOCAL_CFLAGS += -Wno-unused-function
LOCAL_CFLAGS += -Wno-parentheses
LOCAL_CFLAGS += -Wno-unused-value
LOCAL_CFLAGS += -Wno-unused-but-set-variable
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/LibJXR
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/LibJXR/jxrgluelib
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/LibJXR/image/sys
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/LibJXR/common/include
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/FreeImage/Source/LibJXR
# MY_SOURCES_PATH += $(LOCAL_PATH)/android

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################