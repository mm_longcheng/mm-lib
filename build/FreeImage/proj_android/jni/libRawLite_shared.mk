LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libRawLite_shared
LOCAL_MODULE_FILENAME := libRawLite_shared
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -D__ANDROID__

LOCAL_CFLAGS += -Wfatal-errors

LOCAL_CFLAGS += -Wall
LOCAL_CFLAGS += -Wno-unknown-warning-option
LOCAL_CFLAGS += -Wno-deprecated-declarations
LOCAL_CFLAGS += -Wno-unused-function
LOCAL_CFLAGS += -Wno-reorder
LOCAL_CFLAGS += -Wno-sign-compare
LOCAL_CFLAGS += -Wno-unused-variable
LOCAL_CFLAGS += -Wno-parentheses
LOCAL_CFLAGS += -Wno-maybe-uninitialized
LOCAL_CFLAGS += -Wno-unused-but-set-variable
LOCAL_CFLAGS += -Wno-narrowing
LOCAL_CFLAGS += -Wno-literal-suffix
LOCAL_CFLAGS += -Wno-unused-label
LOCAL_CFLAGS += -Wno-write-strings
LOCAL_CFLAGS += -Wno-constant-conversion
LOCAL_CFLAGS += -Wno-tautological-constant-out-of-range-compare
LOCAL_CFLAGS += -Wno-constant-logical-operand
LOCAL_CFLAGS += -Wno-sometimes-uninitialized
LOCAL_CFLAGS += -Wno-deprecated-register

LOCAL_CFLAGS += -DLIBRAW_NODLL

LOCAL_CXXFLAGS += -fexceptions 
LOCAL_CXXFLAGS += -frtti
########################################################################
LOCAL_LDLIBS += -fPIC
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/LibRawLite
########################################################################
LOCAL_SRC_FILES  += ../../../../src/FreeImage/Source/LibRawLite/internal/dcraw_common.cpp
LOCAL_SRC_FILES  += ../../../../src/FreeImage/Source/LibRawLite/internal/dcraw_fileio.cpp
LOCAL_SRC_FILES  += ../../../../src/FreeImage/Source/LibRawLite/internal/demosaic_packs.cpp

LOCAL_SRC_FILES  += ../../../../src/FreeImage/Source/LibRawLite/src/libraw_c_api.cpp
LOCAL_SRC_FILES  += ../../../../src/FreeImage/Source/LibRawLite/src/libraw_cxx.cpp
LOCAL_SRC_FILES  += ../../../../src/FreeImage/Source/LibRawLite/src/libraw_datastream.cpp
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/android

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################