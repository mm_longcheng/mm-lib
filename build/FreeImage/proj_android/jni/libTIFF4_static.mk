LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libTIFF4_static
LOCAL_MODULE_FILENAME := libTIFF4_static
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -D__ANDROID__

LOCAL_CFLAGS += -Wall
LOCAL_CFLAGS += -Wno-unknown-warning-option
LOCAL_CFLAGS += -Wno-unused-but-set-variable
LOCAL_CFLAGS += -Wno-int-to-void-pointer-cast
LOCAL_CFLAGS += -Wno-format

LOCAL_DISABLE_FORMAT_STRING_CHECKS := true
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/LibTIFF4

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/zlib
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/FreeImage/Source/LibTIFF4
# MY_SOURCES_PATH += $(LOCAL_PATH)/android

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibTIFF4/tif_acorn.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibTIFF4/tif_apple.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibTIFF4/tif_atari.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibTIFF4/tif_msdos.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibTIFF4/tif_vms.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibTIFF4/tif_win32.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibTIFF4/tif_win3.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibTIFF4/tif_wince.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibTIFF4/mkg3states.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibTIFF4/mkspans.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################