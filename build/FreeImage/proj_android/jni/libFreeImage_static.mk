LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libFreeImage_static
LOCAL_MODULE_FILENAME := libFreeImage_static
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -D__ANDROID__

LOCAL_CFLAGS += -Wall
LOCAL_CFLAGS += -Wno-unknown-warning-option
LOCAL_CFLAGS += -Wno-unused-variable
LOCAL_CFLAGS += -Wno-parentheses
LOCAL_CFLAGS += -Wno-switch
LOCAL_CFLAGS += -Wno-unused-but-set-variable
LOCAL_CFLAGS += -Wno-deprecated-declarations
LOCAL_CFLAGS += -Wno-sign-compare
LOCAL_CFLAGS += -Wno-maybe-uninitialized
LOCAL_CFLAGS += -Wno-unused-value
LOCAL_CFLAGS += -Wno-unused-function
LOCAL_CFLAGS += -Wno-format
LOCAL_CFLAGS += -Wno-missing-braces
LOCAL_CFLAGS += -Wno-unused-private-field
LOCAL_CFLAGS += -Wno-deprecated-register
LOCAL_CFLAGS += -Wno-char-subscripts
LOCAL_CFLAGS += -Wno-tautological-compare
LOCAL_CFLAGS += -Wno-exceptions

LOCAL_CFLAGS += -Wno-unused-function
LOCAL_CFLAGS += -Wno-parentheses
LOCAL_CFLAGS += -Wno-unused-value
LOCAL_CFLAGS += -Wno-unused-but-set-variable

LOCAL_CFLAGS += -Wno-unknown-warning-option
LOCAL_CFLAGS += -Wno-deprecated-declarations
LOCAL_CFLAGS += -Wno-unused-function
LOCAL_CFLAGS += -Wno-reorder
LOCAL_CFLAGS += -Wno-sign-compare
LOCAL_CFLAGS += -Wno-unused-variable
LOCAL_CFLAGS += -Wno-parentheses
LOCAL_CFLAGS += -Wno-maybe-uninitialized
LOCAL_CFLAGS += -Wno-unused-but-set-variable
LOCAL_CFLAGS += -Wno-narrowing
LOCAL_CFLAGS += -Wno-literal-suffix
LOCAL_CFLAGS += -Wno-unused-label
LOCAL_CFLAGS += -Wno-write-strings
LOCAL_CFLAGS += -Wno-constant-conversion
LOCAL_CFLAGS += -Wno-tautological-constant-out-of-range-compare
LOCAL_CFLAGS += -Wno-constant-logical-operand
LOCAL_CFLAGS += -Wno-sometimes-uninitialized
LOCAL_CFLAGS += -Wno-deprecated-register

LOCAL_CFLAGS += -Wno-unknown-warning-option
LOCAL_CFLAGS += -Wno-unused-but-set-variable

LOCAL_CFLAGS += -Wno-int-to-void-pointer-cast

LOCAL_CFLAGS += -mfloat-abi=softfp 
LOCAL_CFLAGS += -mfpu=neon
LOCAL_CFLAGS += -Wno-unused-function

LOCAL_CFLAGS += -DUSE_JPIP

LOCAL_CFLAGS += -DLIBRAW_NODLL

LOCAL_CFLAGS += -D__ANSI__ 
LOCAL_CFLAGS += -DDISABLE_PERF_MEASUREMENT

LOCAL_CFLAGS += -DFREEIMAGE_COLORORDER=FREEIMAGE_COLORORDER_RGB

LOCAL_CXXFLAGS += -Wno-format

LOCAL_CXXFLAGS += -fexceptions 
LOCAL_CXXFLAGS += -frtti

LOCAL_DISABLE_FORMAT_STRING_CHECKS := true
########################################################################
LOCAL_LDLIBS += 
########################################################################
# # if use all shared about FreeImage_shared.use this.
# LOCAL_SHARED_LIBRARIES += libJPEG_shared
# # LOCAL_SHARED_LIBRARIES += libJXR_shared
# LOCAL_SHARED_LIBRARIES += libOpenJPEG_shared
# LOCAL_SHARED_LIBRARIES += libPNG_shared
# LOCAL_SHARED_LIBRARIES += libRawLite_shared
# LOCAL_SHARED_LIBRARIES += libTIFF4_shared
# LOCAL_SHARED_LIBRARIES += libWebP_shared
# LOCAL_SHARED_LIBRARIES += libOpenEXR_shared
########################################################################
# if use all in one about FreeImage_shared.use this.
LOCAL_STATIC_LIBRARIES += libJPEG_static
# LOCAL_STATIC_LIBRARIES += libJXR_static
LOCAL_STATIC_LIBRARIES += libOpenJPEG_static
LOCAL_STATIC_LIBRARIES += libPNG_static
LOCAL_STATIC_LIBRARIES += libRawLite_static
LOCAL_STATIC_LIBRARIES += libTIFF4_static
LOCAL_STATIC_LIBRARIES += libWebP_static
LOCAL_STATIC_LIBRARIES += libOpenEXR_static
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/DeprecationManager
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/Metadata
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/FreeImageToolkit

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/OpenEXR
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/OpenEXR/Half
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/OpenEXR/Iex
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/OpenEXR/IexMath
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/OpenEXR/IlmImf
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/OpenEXR/IlmThread
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/OpenEXR/Imath

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/LibJXR
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/LibJXR/jxrgluelib
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/LibJXR/image/sys
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/LibJXR/common/include

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/LibJPEG

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/LibRawLite

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/LibWebP

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/zlib
########################################################################
LOCAL_SRC_FILES  += 

LOCAL_SRC_FILES  += ../../../../src/FreeImage/Source/LibRawLite/internal/dcraw_common.cpp
LOCAL_SRC_FILES  += ../../../../src/FreeImage/Source/LibRawLite/internal/dcraw_fileio.cpp
LOCAL_SRC_FILES  += ../../../../src/FreeImage/Source/LibRawLite/internal/demosaic_packs.cpp

LOCAL_SRC_FILES  += ../../../../src/FreeImage/Source/LibRawLite/src/libraw_c_api.cpp
LOCAL_SRC_FILES  += ../../../../src/FreeImage/Source/LibRawLite/src/libraw_cxx.cpp
LOCAL_SRC_FILES  += ../../../../src/FreeImage/Source/LibRawLite/src/libraw_datastream.cpp
########################################################################
LOCAL_OBJECTS    +=
LOCAL_OBJECTS    += obj/local/$(APP_ABI)/libJPEG.a
# LOCAL_OBJECTS    += obj/local/$(APP_ABI)/libJXR.a
LOCAL_OBJECTS    += obj/local/$(APP_ABI)/libOpenJPEG.a
LOCAL_OBJECTS    += obj/local/$(APP_ABI)/libPNG.a
LOCAL_OBJECTS    += obj/local/$(APP_ABI)/libRawLite.a
LOCAL_OBJECTS    += obj/local/$(APP_ABI)/libTIFF4.a
LOCAL_OBJECTS    += obj/local/$(APP_ABI)/libWebP.a
LOCAL_OBJECTS    += obj/local/$(APP_ABI)/libOpenEXR.a
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/FreeImage/Source/LibJPEG
# MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/FreeImage/Source/LibJXR
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/FreeImage/Source/LibOpenJPEG
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/FreeImage/Source/LibPNG
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/FreeImage/Source/LibTIFF4
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/FreeImage/Source/LibWebP
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/FreeImage/Source/OpenEXR

MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/FreeImage/Source/FreeImage
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/FreeImage/Source/DeprecationManager
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/FreeImage/Source/Metadata
# ok the path name "FreeImageToolkit" is searsh by "FreeImage" so we just hide it.
# MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/FreeImage/Source/FreeImageToolkit
# MY_SOURCES_PATH += $(LOCAL_PATH)/android

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/FreeImage/PluginJXR.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/FreeImage/FreeImageC.c

MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibJPEG/jmemdos.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibJPEG/jmemmac.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibJPEG/jmemname.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibJPEG/jmemansi.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibJPEG/ckconfig.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibJPEG/ansi2knr.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibJPEG/example.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibJPEG/cjpeg.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibJPEG/cdjpeg.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibJPEG/ckconfig.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibJPEG/jpegtran.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibJPEG/djpeg.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibJPEG/rdjpgcom.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibJPEG/wrjpgcom.c

MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibPNG/example.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibPNG/pngtest.c

MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibTIFF4/tif_acorn.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibTIFF4/tif_apple.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibTIFF4/tif_atari.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibTIFF4/tif_msdos.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibTIFF4/tif_vms.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibTIFF4/tif_win32.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibTIFF4/tif_win3.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibTIFF4/tif_wince.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibTIFF4/mkg3states.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/LibTIFF4/mkspans.c

MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/OpenEXR/IlmThread/IlmThreadMutexWin32.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/OpenEXR/IlmThread/IlmThreadSemaphoreWin32.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/OpenEXR/IlmThread/IlmThreadWin32.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/OpenEXR/Half/toFloat.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/OpenEXR/Half/eLut.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/OpenEXR/IlmImf/b44ExpLogTable.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/OpenEXR/IlmImf/dwaLookups.cpp

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################