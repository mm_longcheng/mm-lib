LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libOpenEXR_static
LOCAL_MODULE_FILENAME := libOpenEXR_static
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -D__ANDROID__

LOCAL_CFLAGS += -Wall
LOCAL_CFLAGS += -Wno-unknown-warning-option
LOCAL_CFLAGS += -Wno-unused-function
LOCAL_CFLAGS += -Wno-format
LOCAL_CFLAGS += -Wno-unused-variable
LOCAL_CFLAGS += -Wno-switch
LOCAL_CFLAGS += -Wno-sign-compare
LOCAL_CFLAGS += -Wno-unused-but-set-variable
LOCAL_CFLAGS += -Wno-maybe-uninitialized
LOCAL_CFLAGS += -Wno-deprecated-register
LOCAL_CFLAGS += -Wno-unused-private-field
LOCAL_CFLAGS += -Wno-tautological-compare
LOCAL_CFLAGS += -Wno-exceptions

LOCAL_CXXFLAGS += -fexceptions 
LOCAL_CXXFLAGS += -frtti
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/OpenEXR
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/OpenEXR/Half
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/OpenEXR/Iex
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/OpenEXR/IexMath
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/OpenEXR/IlmImf
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/OpenEXR/IlmThread
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/OpenEXR/Imath

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/zlib
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/FreeImage/Source/OpenEXR
# MY_SOURCES_PATH += $(LOCAL_PATH)/android

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/OpenEXR/IlmThread/IlmThreadMutexPosix.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/OpenEXR/IlmThread/IlmThreadPosix.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/OpenEXR/IlmThread/IlmThreadSemaphorePosix.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/OpenEXR/IlmThread/IlmThreadSemaphorePosixCompat.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/OpenEXR/IlmThread/IlmThreadMutexWin32.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/OpenEXR/IlmThread/IlmThreadSemaphoreWin32.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/OpenEXR/IlmThread/IlmThreadWin32.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/OpenEXR/Half/toFloat.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/OpenEXR/Half/eLut.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/OpenEXR/IlmImf/b44ExpLogTable.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/OpenEXR/IlmImf/dwaLookups.cpp

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################