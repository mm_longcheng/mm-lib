LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libFreeImage_shared
LOCAL_MODULE_FILENAME := libFreeImage_shared
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -D__ANDROID__

LOCAL_CFLAGS += -Wall
LOCAL_CFLAGS += -Wno-unknown-warning-option
LOCAL_CFLAGS += -Wno-unused-variable
LOCAL_CFLAGS += -Wno-parentheses
LOCAL_CFLAGS += -Wno-switch
LOCAL_CFLAGS += -Wno-unused-but-set-variable
LOCAL_CFLAGS += -Wno-deprecated-declarations
LOCAL_CFLAGS += -Wno-sign-compare
LOCAL_CFLAGS += -Wno-maybe-uninitialized
LOCAL_CFLAGS += -Wno-unused-value
LOCAL_CFLAGS += -Wno-unused-function
LOCAL_CFLAGS += -Wno-format
LOCAL_CFLAGS += -Wno-missing-braces
LOCAL_CFLAGS += -Wno-unused-private-field
LOCAL_CFLAGS += -Wno-deprecated-register
LOCAL_CFLAGS += -Wno-char-subscripts
LOCAL_CFLAGS += -Wno-tautological-compare
LOCAL_CFLAGS += -Wno-exceptions

LOCAL_CFLAGS += -D__ANSI__ 
LOCAL_CFLAGS += -DDISABLE_PERF_MEASUREMENT

LOCAL_CFLAGS += -DFREEIMAGE_COLORORDER=FREEIMAGE_COLORORDER_RGB

LOCAL_CXXFLAGS += -Wno-format

LOCAL_DISABLE_FORMAT_STRING_CHECKS := true
########################################################################
LOCAL_LDLIBS += -fPIC
########################################################################
# # if use all shared about FreeImage_shared.use this.
# LOCAL_SHARED_LIBRARIES += libJPEG_shared
# # LOCAL_SHARED_LIBRARIES += libJXR_shared
# LOCAL_SHARED_LIBRARIES += libOpenJPEG_shared
# LOCAL_SHARED_LIBRARIES += libPNG_shared
# LOCAL_SHARED_LIBRARIES += libRawLite_shared
# LOCAL_SHARED_LIBRARIES += libTIFF4_shared
# LOCAL_SHARED_LIBRARIES += libWebP_shared
# LOCAL_SHARED_LIBRARIES += libOpenEXR_shared
LOCAL_SHARED_LIBRARIES += libz_shared
########################################################################
# if use all in one about FreeImage_shared.use this.
LOCAL_STATIC_LIBRARIES += libJPEG_static
# LOCAL_STATIC_LIBRARIES += libJXR_static
LOCAL_STATIC_LIBRARIES += libOpenJPEG_static
LOCAL_STATIC_LIBRARIES += libPNG_static
LOCAL_STATIC_LIBRARIES += libRawLite_static
LOCAL_STATIC_LIBRARIES += libTIFF4_static
LOCAL_STATIC_LIBRARIES += libWebP_static
LOCAL_STATIC_LIBRARIES += libOpenEXR_static
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/DeprecationManager
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/Metadata
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/FreeImageToolkit

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/OpenEXR
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/OpenEXR/Half
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/OpenEXR/Iex
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/OpenEXR/IexMath
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/OpenEXR/IlmImf
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/OpenEXR/IlmThread
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/OpenEXR/Imath

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/LibJXR
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/LibJXR/jxrgluelib
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/LibJXR/image/sys
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/LibJXR/common/include

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/LibJPEG

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/LibRawLite

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source/LibWebP

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/zlib
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/FreeImage/Source/FreeImage
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/FreeImage/Source/DeprecationManager
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/FreeImage/Source/Metadata
# ok the path name "FreeImageToolkit" is searsh by "FreeImage" so we just hide it.
# MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/FreeImage/Source/FreeImageToolkit
MY_SOURCES_PATH += $(LOCAL_PATH)/android

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/FreeImage/PluginJXR.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/FreeImage/Source/FreeImage/FreeImageC.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################