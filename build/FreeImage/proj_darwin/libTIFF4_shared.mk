LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)
########################################################################
LOCAL_MODULE := libTIFF4_shared
LOCAL_MODULE_FILENAME := libTIFF4
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -Wno-int-to-void-pointer-cast
########################################################################
LOCAL_LDLIBS += -fPIC
########################################################################
LOCAL_SHARED_LIBRARIES += libJPEG_shared
LOCAL_SHARED_LIBRARIES += libz_shared
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/FreeImage/Source
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/FreeImage/Source/LibTIFF4
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/FreeImage/Source/LibTIFF4
MY_SOURCES_PATH += $(LOCAL_PATH)/$(TARGET_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../src/FreeImage/Source/LibTIFF4/tif_acorn.c
MY_SOURCES_FILTER_OUT += ../../../src/FreeImage/Source/LibTIFF4/tif_apple.c
MY_SOURCES_FILTER_OUT += ../../../src/FreeImage/Source/LibTIFF4/tif_atari.c
MY_SOURCES_FILTER_OUT += ../../../src/FreeImage/Source/LibTIFF4/tif_msdos.c
MY_SOURCES_FILTER_OUT += ../../../src/FreeImage/Source/LibTIFF4/tif_vms.c
MY_SOURCES_FILTER_OUT += ../../../src/FreeImage/Source/LibTIFF4/tif_win32.c
MY_SOURCES_FILTER_OUT += ../../../src/FreeImage/Source/LibTIFF4/tif_win3.c
MY_SOURCES_FILTER_OUT += ../../../src/FreeImage/Source/LibTIFF4/tif_wince.c
MY_SOURCES_FILTER_OUT += ../../../src/FreeImage/Source/LibTIFF4/tif_jbig.c
MY_SOURCES_FILTER_OUT += ../../../src/FreeImage/Source/LibTIFF4/tif_jpeg_12.c
MY_SOURCES_FILTER_OUT += ../../../src/FreeImage/Source/LibTIFF4/tif_lzma.c
MY_SOURCES_FILTER_OUT += ../../../src/FreeImage/Source/LibTIFF4/mkg3states.c
MY_SOURCES_FILTER_OUT += ../../../src/FreeImage/Source/LibTIFF4/mkspans.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
include $(SOURCE_RWILDCARD)
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################
# $(modules-dump-database)
