LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)
########################################################################
LOCAL_MODULE := libFreeImage_static
LOCAL_MODULE_FILENAME := libFreeImage
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -Wno-switch
LOCAL_CFLAGS += -Wno-bitwise-op-parentheses
LOCAL_CFLAGS += -Wno-unused-value
LOCAL_CFLAGS += -Wno-unused-variable
LOCAL_CFLAGS += -Wno-unused-function
LOCAL_CFLAGS += -Wno-unused-comparison
LOCAL_CFLAGS += -Wno-unused-private-field
LOCAL_CFLAGS += -Wno-format
LOCAL_CFLAGS += -Wno-logical-op-parentheses
LOCAL_CFLAGS += -Wno-logical-not-parentheses
LOCAL_CFLAGS += -Wno-missing-braces
LOCAL_CFLAGS += -Wno-char-subscripts

LOCAL_CFLAGS += -D__ANSI__ 
LOCAL_CFLAGS += -DDISABLE_PERF_MEASUREMENT

LOCAL_CFLAGS += -DFREEIMAGE_COLORORDER=FREEIMAGE_COLORORDER_RGB

LOCAL_CXXFLAGS += -Wno-format
LOCAL_CXXFLAGS += -fexceptions
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/FreeImage/Source
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/FreeImage/Source/DeprecationManager
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/FreeImage/Source/Metadata
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/FreeImage/Source/FreeImageToolkit

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/FreeImage/Source/OpenEXR
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/FreeImage/Source/OpenEXR/Half
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/FreeImage/Source/OpenEXR/Iex
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/FreeImage/Source/OpenEXR/IlmImf
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/FreeImage/Source/OpenEXR/Imath
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/FreeImage/Source/OpenEXR/IlmThread

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/FreeImage/Source/LibJXR
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/FreeImage/Source/LibJXR/jxrgluelib
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/FreeImage/Source/LibJXR/image/sys
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/FreeImage/Source/LibJXR/common/include

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/FreeImage/Source/LibJPEG

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/FreeImage/Source/LibRawLite

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/FreeImage/Source/OpenEXR

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/FreeImage/Source/LibWebP
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/FreeImage/Source/FreeImage
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/FreeImage/Source/DeprecationManager
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/FreeImage/Source/Metadata
# ok the path name "FreeImageToolkit" is searsh by "FreeImage" so we just hide it.
# MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/FreeImage/Source/FreeImageToolkit
MY_SOURCES_PATH += $(LOCAL_PATH)/$(TARGET_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../src/FreeImage/Source/FreeImage/PluginJXR.cpp
MY_SOURCES_FILTER_OUT += ../../../src/FreeImage/Source/FreeImage/FreeImageC.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
include $(SOURCE_RWILDCARD)
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################
# $(modules-dump-database)
