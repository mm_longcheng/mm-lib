LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)
########################################################################
LOCAL_MODULE := libOpenEXR_static
LOCAL_MODULE_FILENAME := libOpenEXR
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -Wno-unused-private-field
LOCAL_CFLAGS += -Wno-unused-function
LOCAL_CFLAGS += -Wno-unused-variable
LOCAL_CFLAGS += -Wno-switch

LOCAL_CXXFLAGS += -fexceptions 
LOCAL_CXXFLAGS += -frtti
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/FreeImage/Source
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/FreeImage/Source/OpenEXR
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/FreeImage/Source/OpenEXR/Half
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/FreeImage/Source/OpenEXR/Iex
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/FreeImage/Source/OpenEXR/IexMath
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/FreeImage/Source/OpenEXR/IlmImf
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/FreeImage/Source/OpenEXR/IlmThread
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/FreeImage/Source/OpenEXR/Imath
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/FreeImage/Source/OpenEXR
MY_SOURCES_PATH += $(LOCAL_PATH)/$(TARGET_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../src/FreeImage/Source/OpenEXR/IlmThread/IlmThreadMutexPosix.cpp
MY_SOURCES_FILTER_OUT += ../../../src/FreeImage/Source/OpenEXR/IlmThread/IlmThreadPosix.cpp
MY_SOURCES_FILTER_OUT += ../../../src/FreeImage/Source/OpenEXR/IlmThread/IlmThreadSemaphorePosix.cpp
MY_SOURCES_FILTER_OUT += ../../../src/FreeImage/Source/OpenEXR/IlmThread/IlmThreadSemaphorePosixCompat.cpp
MY_SOURCES_FILTER_OUT += ../../../src/FreeImage/Source/OpenEXR/IlmThread/IlmThreadMutexWin32.cpp
MY_SOURCES_FILTER_OUT += ../../../src/FreeImage/Source/OpenEXR/IlmThread/IlmThreadSemaphoreWin32.cpp
MY_SOURCES_FILTER_OUT += ../../../src/FreeImage/Source/OpenEXR/IlmThread/IlmThreadWin32.cpp
MY_SOURCES_FILTER_OUT += ../../../src/FreeImage/Source/OpenEXR/Half/toFloat.cpp
MY_SOURCES_FILTER_OUT += ../../../src/FreeImage/Source/OpenEXR/Half/eLut.cpp
MY_SOURCES_FILTER_OUT += ../../../src/FreeImage/Source/OpenEXR/IlmImf/b44ExpLogTable.cpp
MY_SOURCES_FILTER_OUT += ../../../src/FreeImage/Source/OpenEXR/IlmImf/dwaLookups.cpp
MY_SOURCES_FILTER_OUT += ../../../src/FreeImage/Source/OpenEXR/Imath/ImathBox.cpp
MY_SOURCES_FILTER_OUT += ../../../src/FreeImage/Source/OpenEXR/Imath/ImathShear.cpp

MY_SOURCES_EXTENSION += .cpp .c .cc .S
include $(SOURCE_RWILDCARD)
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################
# $(modules-dump-database)
