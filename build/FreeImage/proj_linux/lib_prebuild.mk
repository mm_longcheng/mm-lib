LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
################################################################################
# prebuild.mk
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libz_shared
LOCAL_SRC_FILES := $(LOCAL_PATH)/../../zlib/proj_$(TARGET_PLATFORM)/bin/libz.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libz_static
LOCAL_SRC_FILES := $(LOCAL_PATH)/../../zlib/proj_$(TARGET_PLATFORM)/bin/libz.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################