# prebuild.mk
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libfreetype_shared
LOCAL_SRC_FILES := ../../../freetype/proj_android/libs/$(TARGET_ARCH_ABI)/libfreetype_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libfreetype_static
LOCAL_SRC_FILES := ../../../freetype/proj_android/obj/local/$(TARGET_ARCH_ABI)/libfreetype_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
