@echo off 

set VSVARS="%VS141COMNTOOLS%vcvars64.bat"
set VC_VER=141

call %VSVARS% 1>nul

devenv %~dp0/protoc_csharp.sln /build debug
devenv %~dp0/protoc_csharp.sln /build release

pause