LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libsqlite_shared
LOCAL_MODULE_FILENAME := libsqlite_shared
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -D__ANDROID__

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -DSQLITE_ENABLE_DESERIALIZE
LOCAL_CFLAGS += -DSQLITE_ENABLE_DBSTAT_VTAB
LOCAL_CFLAGS += -DSQLITE_ENABLE_DBPAGE_VTAB
########################################################################
LOCAL_LDLIBS += -fPIC
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/sqlite/src
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/android
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../source/android
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/sqlite/src
MY_SOURCES_PATH += $(LOCAL_PATH)/../../source/android
MY_SOURCES_PATH += $(LOCAL_PATH)/android

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/sqlite/src/test%
MY_SOURCES_FILTER_OUT += ../../../../src/sqlite/src/os_win.c
MY_SOURCES_FILTER_OUT += ../../../../src/sqlite/src/mutex_w32.c
MY_SOURCES_FILTER_OUT += ../../../../src/sqlite/src/tclsqlite.c
MY_SOURCES_FILTER_OUT += ../../source/android/shell.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################