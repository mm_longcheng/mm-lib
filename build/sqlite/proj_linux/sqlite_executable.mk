LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)
########################################################################
LOCAL_MODULE := sqlite_executable
LOCAL_MODULE_FILENAME := sqlite
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -DSQLITE_OS_UNIX=1
LOCAL_CFLAGS += -D_HAVE_SQLITE_CONFIG_H
LOCAL_CFLAGS += -DBUILD_sqlite
LOCAL_CFLAGS += -DNDEBUG
LOCAL_CFLAGS += -DSQLITE_THREADSAFE=1
LOCAL_CFLAGS += -DSQLITE_HAVE_ZLIB=1
LOCAL_CFLAGS += -DHAVE_READLINE=0
LOCAL_CFLAGS += -DHAVE_EDITLINE=0
LOCAL_CFLAGS += -DSQLITE_ENABLE_JSON1
LOCAL_CFLAGS += -DSQLITE_ENABLE_FTS4
LOCAL_CFLAGS += -DSQLITE_ENABLE_RTREE
LOCAL_CFLAGS += -DSQLITE_ENABLE_EXPLAIN_COMMENTS
LOCAL_CFLAGS += -DSQLITE_ENABLE_UNKNOWN_SQL_FUNCTION
LOCAL_CFLAGS += -DSQLITE_ENABLE_STMTVTAB
LOCAL_CFLAGS += -DSQLITE_ENABLE_DBPAGE_VTAB
LOCAL_CFLAGS += -DSQLITE_ENABLE_DBSTAT_VTAB
LOCAL_CFLAGS += -DSQLITE_ENABLE_OFFSET_SQL_FUNC
LOCAL_CFLAGS += -DSQLITE_ENABLE_DESERIALIZE
LOCAL_CFLAGS += -DSQLITE_INTROSPECTION_PRAGMAS

LOCAL_CFLAGS += -DSQLITE_OMIT_LOAD_EXTENSION=1
########################################################################
LOCAL_LDLIBS += 
LOCAL_LDLIBS += -ldl
LOCAL_LDLIBS += -lpthread
########################################################################
LOCAL_SHARED_LIBRARIES +=
########################################################################
LOCAL_STATIC_LIBRARIES +=
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/sqlite/src
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../include/$(TARGET_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../source/$(TARGET_PLATFORM)

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/sqlite/ext/rtree
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/sqlite/ext/icu
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/sqlite/ext/fts3
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/sqlite/ext/async
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/sqlite/ext/session
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/sqlite/ext/userauth
########################################################################
LOCAL_SRC_FILES  += ../../../src/sqlite/ext/rtree/rtree.c
# LOCAL_SRC_FILES  += ../../../src/sqlite/ext/icu/icu.c
LOCAL_SRC_FILES  += ../../../src/sqlite/ext/async/sqlite3async.c
LOCAL_SRC_FILES  += ../../../src/sqlite/ext/misc/json1.c
LOCAL_SRC_FILES  += ../../../src/sqlite/ext/misc/stmt.c
########################################################################
#  
# config self source file path ,suffix.
# MY_SOURCES_PATH += $(LOCAL_PATH)/../../src
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/sqlite/src
MY_SOURCES_PATH += $(LOCAL_PATH)/../source/$(TARGET_PLATFORM)
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/sqlite/ext/fts3
# MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/sqlite/ext/misc

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../src/sqlite/src/test%
MY_SOURCES_FILTER_OUT += ../../../src/sqlite/src/os_win.c
MY_SOURCES_FILTER_OUT += ../../../src/sqlite/src/mutex_w32.c
MY_SOURCES_FILTER_OUT += ../../../src/sqlite/src/tclsqlite.c

MY_SOURCES_FILTER_OUT += ../../../src/sqlite/ext/fts3/tool%
MY_SOURCES_FILTER_OUT += ../../../src/sqlite/ext/fts3/unicode%
MY_SOURCES_FILTER_OUT += ../../../src/sqlite/ext/fts3/fts3_test.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
include $(SOURCE_RWILDCARD)
########################################################################
include $(BUILD_EXECUTABLE)
########################################################################
# $(modules-dump-database)
