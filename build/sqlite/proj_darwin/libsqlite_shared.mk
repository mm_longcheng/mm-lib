LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)
########################################################################
LOCAL_MODULE := libsqlite_shared
LOCAL_MODULE_FILENAME := libsqlite_shared
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS +=
########################################################################
LOCAL_LDLIBS += -fPIC
LOCAL_LDLIBS += -ldl
LOCAL_LDLIBS += -lpthread
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/sqlite/src
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../include/$(TARGET_PLATFORM)
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../source/$(TARGET_PLATFORM)
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
#  
# config self source file path ,suffix.
# MY_SOURCES_PATH += $(LOCAL_PATH)/../../src
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/sqlite/src
MY_SOURCES_PATH += $(LOCAL_PATH)/../source/$(TARGET_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../src/sqlite/src/test%
MY_SOURCES_FILTER_OUT += ../../../src/sqlite/src/os_win.c
MY_SOURCES_FILTER_OUT += ../../../src/sqlite/src/mutex_w32.c
MY_SOURCES_FILTER_OUT += ../../../src/sqlite/src/tclsqlite.c

MY_SOURCES_FILTER_OUT += ../../../src/sqlite/src/dbpage.c
MY_SOURCES_FILTER_OUT += ../../../src/sqlite/src/dbstat.c
MY_SOURCES_FILTER_OUT += ../../../src/sqlite/src/mem0.c
MY_SOURCES_FILTER_OUT += ../../../src/sqlite/src/mem2.c
MY_SOURCES_FILTER_OUT += ../../../src/sqlite/src/mem3.c
MY_SOURCES_FILTER_OUT += ../../../src/sqlite/src/mem5.c
MY_SOURCES_FILTER_OUT += ../../../src/sqlite/src/memdb.c
MY_SOURCES_FILTER_OUT += ../../../src/sqlite/src/notify.c
MY_SOURCES_FILTER_OUT += ../../../src/sqlite/src/treeview.c

MY_SOURCES_FILTER_OUT += ../source/$(TARGET_PLATFORM)/shell.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
include $(SOURCE_RWILDCARD)
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################
# $(modules-dump-database)
