LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libprotobuf-c_static
LOCAL_MODULE_FILENAME := libprotobuf-c_static
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -D__ANDROID__

LOCAL_CFLAGS += -Wall
LOCAL_CFLAGS += -Wno-unknown-warning-option
LOCAL_CFLAGS += -Wno-unused-label
LOCAL_CFLAGS += -Wno-unused-but-set-variable
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/protobuf-c/src
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/android
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/protobuf-c/src/google/protobuf-c
# MY_SOURCES_PATH += $(LOCAL_PATH)/android

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf-c/src/test
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf-c/src/google/protobuf/compiler%
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf-c/src/google/protobuf-c/protobuf-c-data-buffer.c
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf-c/src/google/protobuf-c/protobuf-c-dispatch.c
MY_SOURCES_FILTER_OUT += ../../../../src/protobuf-c/src/google/protobuf-c/protobuf-c-rpc.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################