LOCAL_PATH := $(call my-dir)

MM_MAKE_HOME ?= $(MM_HOME)/mm-make

include $(LOCAL_PATH)/lib_prebuild.mk

include $(LOCAL_PATH)/libOgreMain_static.mk
include $(LOCAL_PATH)/libOgreMain_shared.mk

include $(LOCAL_PATH)/libComponents_HLMS_static.mk
include $(LOCAL_PATH)/libComponents_HLMS_shared.mk

include $(LOCAL_PATH)/libComponents_Overlay_static.mk
include $(LOCAL_PATH)/libComponents_Overlay_shared.mk

include $(LOCAL_PATH)/libComponents_Paging_static.mk
include $(LOCAL_PATH)/libComponents_Paging_shared.mk

include $(LOCAL_PATH)/libComponents_Property_static.mk
include $(LOCAL_PATH)/libComponents_Property_shared.mk

include $(LOCAL_PATH)/libComponents_RTShaderSystem_static.mk
include $(LOCAL_PATH)/libComponents_RTShaderSystem_shared.mk

include $(LOCAL_PATH)/libComponents_Terrain_static.mk
include $(LOCAL_PATH)/libComponents_Terrain_shared.mk

include $(LOCAL_PATH)/libComponents_Volume_static.mk
include $(LOCAL_PATH)/libComponents_Volume_shared.mk

include $(LOCAL_PATH)/libComponents_MeshLodGenerator_static.mk
include $(LOCAL_PATH)/libComponents_MeshLodGenerator_shared.mk

include $(LOCAL_PATH)/libPlugin_BSPSceneManager_static.mk
include $(LOCAL_PATH)/libPlugin_BSPSceneManager_shared.mk

# include $(LOCAL_PATH)/libPlugin_EXRCodec_static.mk
# include $(LOCAL_PATH)/libPlugin_EXRCodec_shared.mk

include $(LOCAL_PATH)/libPlugin_OctreeSceneManager_static.mk
include $(LOCAL_PATH)/libPlugin_OctreeSceneManager_shared.mk

include $(LOCAL_PATH)/libPlugin_OctreeZone_static.mk
include $(LOCAL_PATH)/libPlugin_OctreeZone_shared.mk

include $(LOCAL_PATH)/libPlugin_ParticleFX_static.mk
include $(LOCAL_PATH)/libPlugin_ParticleFX_shared.mk

include $(LOCAL_PATH)/libPlugin_PCZSceneManager_static.mk
include $(LOCAL_PATH)/libPlugin_PCZSceneManager_shared.mk

include $(LOCAL_PATH)/libRenderSystem_GLSupport_static.mk
# include $(LOCAL_PATH)/libRenderSystem_GLSupport_shared.mk

# include $(LOCAL_PATH)/libRenderSystem_GLES_static.mk
# include $(LOCAL_PATH)/libRenderSystem_GLES_shared.mk

include $(LOCAL_PATH)/libRenderSystem_GLES2_static.mk
include $(LOCAL_PATH)/libRenderSystem_GLES2_shared.mk

# include $(LOCAL_PATH)/libOgreComponents_static.mk
# include $(LOCAL_PATH)/libOgreComponents_shared.mk

$(call import-module,android/cpufeatures) 
$(call import-module,android/native_app_glue)