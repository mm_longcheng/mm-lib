LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libPlugin_ParticleFX_shared
LOCAL_MODULE_FILENAME := libPlugin_ParticleFX_shared
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -D__ANDROID__

LOCAL_CFLAGS += -Wall

LOCAL_CXXFLAGS += -std=c++11 
LOCAL_CXXFLAGS += -fexceptions
LOCAL_CXXFLAGS += -frtti 
########################################################################
LOCAL_LDLIBS += -fPIC
########################################################################
LOCAL_SHARED_LIBRARIES += libOgreMain_shared
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Ogre/PlugIns/ParticleFX/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Ogre/OgreMain/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/android
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/Ogre/PlugIns/ParticleFX/src
MY_SOURCES_PATH += $(LOCAL_PATH)/android

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += android/mmMain.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################