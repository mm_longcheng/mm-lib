APP_ABI := arm64-v8a armeabi-v7a # armeabi

APP_STL := gnustl_shared

APP_MODULES += libOgreMain_static
APP_MODULES += libOgreMain_shared

APP_MODULES += libComponents_HLMS_static
APP_MODULES += libComponents_HLMS_shared

APP_MODULES += libComponents_Overlay_static
APP_MODULES += libComponents_Overlay_shared

APP_MODULES += libComponents_Paging_static
APP_MODULES += libComponents_Paging_shared

APP_MODULES += libComponents_Property_static
APP_MODULES += libComponents_Property_shared

APP_MODULES += libComponents_RTShaderSystem_static
APP_MODULES += libComponents_RTShaderSystem_shared

APP_MODULES += libComponents_Terrain_static
APP_MODULES += libComponents_Terrain_shared

APP_MODULES += libComponents_Volume_static
APP_MODULES += libComponents_Volume_shared

APP_MODULES += libComponents_MeshLodGenerator_static
APP_MODULES += libComponents_MeshLodGenerator_shared

APP_MODULES += libPlugin_BSPSceneManager_static
APP_MODULES += libPlugin_BSPSceneManager_shared

# APP_MODULES += libPlugin_EXRCodec_static
# APP_MODULES += libPlugin_EXRCodec_shared

APP_MODULES += libPlugin_OctreeSceneManager_static
APP_MODULES += libPlugin_OctreeSceneManager_shared

APP_MODULES += libPlugin_OctreeZone_static
APP_MODULES += libPlugin_OctreeZone_shared

APP_MODULES += libPlugin_ParticleFX_static
APP_MODULES += libPlugin_ParticleFX_shared

APP_MODULES += libPlugin_PCZSceneManager_static
APP_MODULES += libPlugin_PCZSceneManager_shared

APP_MODULES += libRenderSystem_GLSupport_static
# APP_MODULES += libRenderSystem_GLSupport_shared

# APP_MODULES += libRenderSystem_GLES_static
# APP_MODULES += libRenderSystem_GLES_shared

APP_MODULES += libRenderSystem_GLES2_static
APP_MODULES += libRenderSystem_GLES2_shared

# APP_MODULES += libOgreComponents_static
# APP_MODULES += libOgreComponents_shared
