# prebuild.mk
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libfreetype_shared
LOCAL_SRC_FILES := ../../../freetype/proj_android/libs/$(TARGET_ARCH_ABI)/libfreetype_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libfreetype_static
LOCAL_SRC_FILES := ../../../freetype/proj_android/obj/local/$(TARGET_ARCH_ABI)/libfreetype_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libFreeImage_shared
LOCAL_SRC_FILES := ../../../FreeImage/proj_android/libs/$(TARGET_ARCH_ABI)/libFreeImage_shared.so
include $(PREBUILT_SHARED_LIBRARY)
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libzziplib_shared
LOCAL_SRC_FILES := ../../../zziplib/proj_android/libs/$(TARGET_ARCH_ABI)/libzzip_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libzziplib_static
LOCAL_SRC_FILES := ../../../zziplib/proj_android/obj/local/$(TARGET_ARCH_ABI)/libzzip_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################

################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libz_shared
LOCAL_SRC_FILES := ../../../zlib/proj_android/libs/$(TARGET_ARCH_ABI)/libz_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libz_static
LOCAL_SRC_FILES := ../../../zlib/proj_android/obj/local/$(TARGET_ARCH_ABI)/libz_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################