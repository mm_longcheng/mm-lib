LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libRenderSystem_GLES2_shared
LOCAL_MODULE_FILENAME := libRenderSystem_GLES2_shared
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -D__ANDROID__

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -DGL_GLEXT_PROTOTYPES=1
LOCAL_CFLAGS += -DOGRE_ENABLE_STATE_CACHE
LOCAL_CFLAGS += -DINCLUDE_RTSHADER_SYSTEM

LOCAL_CXXFLAGS += -std=c++11 
LOCAL_CXXFLAGS += -fexceptions
LOCAL_CXXFLAGS += -frtti 
########################################################################
LOCAL_LDLIBS += -fPIC
LOCAL_LDLIBS += -lEGL 
LOCAL_LDLIBS += -lGLESv2
########################################################################
LOCAL_SHARED_LIBRARIES += libOgreMain_shared
########################################################################
LOCAL_STATIC_LIBRARIES += libRenderSystem_GLSupport_static
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Ogre/RenderSystems/GLES2/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Ogre/OgreMain/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/android

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Ogre/RenderSystems/GLSupport/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Ogre/RenderSystems/GLSupport/include/GLSL
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Ogre/RenderSystems/GLSupport/include/EGL
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Ogre/RenderSystems/GLSupport/include/EGL/Android

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Ogre/Components/RTShaderSystem/include

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Ogre/RenderSystems/GLES2/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Ogre/RenderSystems/GLES2/include/EGL
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Ogre/RenderSystems/GLES2/include/EAGL
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Ogre/RenderSystems/GLES2/src/GLSLES/include
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/Ogre/RenderSystems/GLES2
MY_SOURCES_PATH += $(LOCAL_PATH)/android

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/Ogre/RenderSystems/GLES2/src/EGL/WIN32%
MY_SOURCES_FILTER_OUT += ../../../../src/Ogre/RenderSystems/GLES2/src/EGL/X11%
# MY_SOURCES_FILTER_OUT += ../../../../src/Ogre/RenderSystems/GLES2/src/GLSLES%
MY_SOURCES_FILTER_OUT += ../../../../src/Ogre/RenderSystems/GLES2/src/GLSLES/src/OgreGLSLESCgProgram.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/Ogre/RenderSystems/GLES2/src/GLSLES/src/OgreGLSLESCgProgramFactory.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/Ogre/RenderSystems/GLES2/src/NaCl%
MY_SOURCES_FILTER_OUT += ../../../../src/Ogre/RenderSystems/GLES2/src/StateCacheManager/OgreGLES2StateCacheManagerImp.cpp
# MY_SOURCES_FILTER_OUT += ../../../../src/Ogre/RenderSystems/GLES2/src/StateCacheManager/OgreGLES2NullStateCacheManagerImp.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/Ogre/RenderSystems/GLES2/src/StateCacheManager/OgreGLES2NullUniformCacheImp.cpp

MY_SOURCES_FILTER_OUT += ../../../../src/Ogre/RenderSystems/GLES2/src/gles3w.cpp

MY_SOURCES_FILTER_OUT += android/mmMain.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################