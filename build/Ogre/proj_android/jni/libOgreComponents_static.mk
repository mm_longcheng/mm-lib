LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libOgreComponents_static
LOCAL_MODULE_FILENAME := libOgreComponents_static
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -D__ANDROID__

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -DFT2_BUILD_LIBRARY

LOCAL_CXXFLAGS += -std=c++11 
LOCAL_CXXFLAGS += -fexceptions
LOCAL_CXXFLAGS += -frtti 
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Ogre/Components/HLMS/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Ogre/Components/MeshLodGenerator/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Ogre/Components/Overlay/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Ogre/Components/Paging/include
# LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Ogre/Components/Property/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Ogre/Components/RTShaderSystem/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Ogre/Components/Terrain/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Ogre/Components/Volume/include

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Ogre/OgreMain/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/android
# LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/freetype/include
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/Ogre/Components/HLMS/src
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/Ogre/Components/MeshLodGenerator/src
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/Ogre/Components/Overlay/src
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/Ogre/Components/Paging/src
# MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/Ogre/Components/Property/src
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/Ogre/Components/RTShaderSystem/src
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/Ogre/Components/Terrain/src
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/Ogre/Components/Volume/src
# MY_SOURCES_PATH += $(LOCAL_PATH)/android

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += android/mmMain.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################