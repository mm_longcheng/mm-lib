LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libRenderSystem_GLES_shared
LOCAL_MODULE_FILENAME := libRenderSystem_GLES_shared
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -D__ANDROID__

LOCAL_CFLAGS += -Wall
LOCAL_CFLAGS += -Wno-unknown-warning-option
LOCAL_CFLAGS += -Wno-unused-but-set-variable
LOCAL_CFLAGS += -Wno-return-type
LOCAL_CFLAGS += -Wno-narrowing
LOCAL_CFLAGS += -Wno-unused-but-set-variable
LOCAL_CFLAGS += -Wno-unknown-pragmas
LOCAL_CFLAGS += -Wno-unused-variable
LOCAL_CFLAGS += -Wno-overloaded-virtual
LOCAL_CFLAGS += -Wno-undefined-var-template
LOCAL_CFLAGS += -Wno-tautological-compare

LOCAL_CFLAGS += -DGL_GLEXT_PROTOTYPES=1

LOCAL_CXXFLAGS += -std=c++11 
LOCAL_CXXFLAGS += -fexceptions
LOCAL_CXXFLAGS += -frtti 
########################################################################
LOCAL_LDLIBS += -fPIC
LOCAL_LDLIBS += -lEGL
LOCAL_LDLIBS += -lGLESv1_CM
########################################################################
LOCAL_SHARED_LIBRARIES += libOgreMain_shared
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Ogre/RenderSystems/GLES/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Ogre/OgreMain/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/android

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Ogre/RenderSystems/GLES/include/EGL/Android
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Ogre/RenderSystems/GLES/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Ogre/RenderSystems/GLES/include/EGL
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Ogre/RenderSystems/GLES/include/EAGL
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Ogre/RenderSystems/GLES/src/StateCacheManager
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/Ogre/RenderSystems/GLES/src
MY_SOURCES_PATH += $(LOCAL_PATH)/android

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/Ogre/RenderSystems/GLES/src/EGL/WIN32%
MY_SOURCES_FILTER_OUT += ../../../../src/Ogre/RenderSystems/GLES/src/EGL/X11%
MY_SOURCES_FILTER_OUT += ../../../../src/Ogre/RenderSystems/GLES/src/StateCacheManager/OgreGLESNullStateCacheManagerImp.cpp

MY_SOURCES_FILTER_OUT += android/mmMain.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_SHARED_LIBRARY)
########################################################################