LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)  
########################################################################
LOCAL_MODULE := libOgreMain_static
LOCAL_MODULE_FILENAME := libOgreMain_static
########################################################################
LOCAL_CFLAGS += -fPIC
LOCAL_CFLAGS += -D__ANDROID__

LOCAL_CFLAGS += -Wall

LOCAL_CFLAGS += -Wno-pointer-bool-conversion
LOCAL_CFLAGS += -Wno-deprecated-declarations
LOCAL_CFLAGS += -Wno-unused-function

LOCAL_CFLAGS += -DSTDC_HEADERS 
LOCAL_CFLAGS += -DNO_JASPER 
LOCAL_CFLAGS += -DNO_LCMS

LOCAL_CFLAGS += -DFREEIMAGE_COLORORDER=FREEIMAGE_COLORORDER_RGB

LOCAL_CXXFLAGS += -std=c++11 
LOCAL_CXXFLAGS += -fexceptions
LOCAL_CXXFLAGS += -frtti 
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += android_native_app_glue
LOCAL_STATIC_LIBRARIES += cpufeatures
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Ogre/OgreMain/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/Ogre/OgreMain/src/nedmalloc
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../include/android

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/FreeImage/Source
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/freetype/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/zziplib

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../../src/zlib

LOCAL_C_INCLUDES += $(ANDROID_NDK)/sources/cpufeatures
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
MY_SOURCES_PATH       := 
MY_SOURCES_FILTER_OUT := 
MY_SOURCES_EXTENSION  := 
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../../src/Ogre/OgreMain
# MY_SOURCES_PATH += $(LOCAL_PATH)/android

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../../src/Ogre/OgreMain/src/Android/JNI%
MY_SOURCES_FILTER_OUT += ../../../../src/Ogre/OgreMain/src/FlashCC%
MY_SOURCES_FILTER_OUT += ../../../../src/Ogre/OgreMain/src/GLX%
MY_SOURCES_FILTER_OUT += ../../../../src/Ogre/OgreMain/src/gtk%
MY_SOURCES_FILTER_OUT += ../../../../src/Ogre/OgreMain/src/iOS%
MY_SOURCES_FILTER_OUT += ../../../../src/Ogre/OgreMain/src/NaCl%
MY_SOURCES_FILTER_OUT += ../../../../src/Ogre/OgreMain/src/OSX%
MY_SOURCES_FILTER_OUT += ../../../../src/Ogre/OgreMain/src/WIN32%
MY_SOURCES_FILTER_OUT += ../../../../src/Ogre/OgreMain/src/Emscripten%

# MY_SOURCES_FILTER_OUT += ../../../../src/Ogre/OgreMain/src/OgreFreeImageCodec.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/Ogre/OgreMain/src/OgreSTBICodec.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/Ogre/OgreMain/src/OgreFileSystemLayerNoOp.cpp

MY_SOURCES_FILTER_OUT += ../../../../src/Ogre/OgreMain/src/Threading/OgreDefaultWorkQueueTBB.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/Ogre/OgreMain/src/nedmalloc/nedmalloc.c
MY_SOURCES_FILTER_OUT += ../../../../src/Ogre/OgreMain/src/Threading/OgreBarrierWin.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/Ogre/OgreMain/src/Threading/OgreLightweightMutexWin.cpp
MY_SOURCES_FILTER_OUT += ../../../../src/Ogre/OgreMain/src/Threading/OgreThreadsWin.cpp

MY_SOURCES_EXTENSION += .cpp .c .cc .S
####
include $(MM_MAKE_HOME)/compile/definitions-sources.mk
include $(MM_MAKE_HOME)/compile/sources-rwildcard.mk
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################