
#ifndef _OgreBspPluginExport_H
#define _OgreBspPluginExport_H

#ifdef PLUGIN_BSPSCENEMANAGER_STATIC_DEFINE
#  define _OgreBspPluginExport
#  define PLUGIN_BSPSCENEMANAGER_NO_EXPORT
#else
#  ifndef _OgreBspPluginExport
     /* We are building this library */
#    if __GNUC__ >= 4
#      define _OgreBspPluginExport  __attribute__ ((visibility("default")))
#    else
#      define _OgreBspPluginExport
#    endif
#  endif

#  ifndef PLUGIN_BSPSCENEMANAGER_NO_EXPORT
#    define PLUGIN_BSPSCENEMANAGER_NO_EXPORT 
#  endif
#endif

#ifndef PLUGIN_BSPSCENEMANAGER_DEPRECATED
#  define PLUGIN_BSPSCENEMANAGER_DEPRECATED __attribute__ ((deprecated))
#endif

#ifndef PLUGIN_BSPSCENEMANAGER_DEPRECATED_EXPORT
#  define PLUGIN_BSPSCENEMANAGER_DEPRECATED_EXPORT _OgreBspPluginExport PLUGIN_BSPSCENEMANAGER_DEPRECATED
#endif

#ifndef PLUGIN_BSPSCENEMANAGER_DEPRECATED_NO_EXPORT
#  define PLUGIN_BSPSCENEMANAGER_DEPRECATED_NO_EXPORT PLUGIN_BSPSCENEMANAGER_NO_EXPORT PLUGIN_BSPSCENEMANAGER_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef PLUGIN_BSPSCENEMANAGER_NO_DEPRECATED
#    define PLUGIN_BSPSCENEMANAGER_NO_DEPRECATED
#  endif
#endif

#endif
