
#ifndef _OgreParticleFXExport_H
#define _OgreParticleFXExport_H

#ifdef PLUGIN_PARTICLEFX_STATIC_DEFINE
#  define _OgreParticleFXExport
#  define PLUGIN_PARTICLEFX_NO_EXPORT
#else
#  ifndef _OgreParticleFXExport
     /* We are building this library */
#    if __GNUC__ >= 4
#      define _OgreParticleFXExport  __attribute__ ((visibility("default")))
#    else
#      define _OgreParticleFXExport
#    endif
#  endif

#  ifndef PLUGIN_PARTICLEFX_NO_EXPORT
#    define PLUGIN_PARTICLEFX_NO_EXPORT 
#  endif
#endif

#ifndef PLUGIN_PARTICLEFX_DEPRECATED
#  define PLUGIN_PARTICLEFX_DEPRECATED __attribute__ ((deprecated))
#endif

#ifndef PLUGIN_PARTICLEFX_DEPRECATED_EXPORT
#  define PLUGIN_PARTICLEFX_DEPRECATED_EXPORT _OgreParticleFXExport PLUGIN_PARTICLEFX_DEPRECATED
#endif

#ifndef PLUGIN_PARTICLEFX_DEPRECATED_NO_EXPORT
#  define PLUGIN_PARTICLEFX_DEPRECATED_NO_EXPORT PLUGIN_PARTICLEFX_NO_EXPORT PLUGIN_PARTICLEFX_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef PLUGIN_PARTICLEFX_NO_DEPRECATED
#    define PLUGIN_PARTICLEFX_NO_DEPRECATED
#  endif
#endif

#endif
