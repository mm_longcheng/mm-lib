
#ifndef _OgreExport_H
#define _OgreExport_H

#ifdef OGREMAIN_STATIC_DEFINE
#  define _OgreExport
#  define _OgrePrivate
#else
#  ifndef _OgreExport
     /* We are building this library */
#    if __GNUC__ >= 4
#      define _OgreExport  __attribute__ ((visibility("default")))
#    else
#      define _OgreExport
#    endif
#  endif

#  ifndef _OgrePrivate
#    define _OgrePrivate 
#  endif
#endif

#ifndef OGRE_DEPRECATED
#  define OGRE_DEPRECATED __attribute__ ((deprecated))
#endif

#ifndef OGRE_DEPRECATED_EXPORT
#  define OGRE_DEPRECATED_EXPORT _OgreExport OGRE_DEPRECATED
#endif

#ifndef OGRE_DEPRECATED_NO_EXPORT
#  define OGRE_DEPRECATED_NO_EXPORT _OgrePrivate OGRE_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef OGREMAIN_NO_DEPRECATED
#    define OGREMAIN_NO_DEPRECATED
#  endif
#endif

#endif
