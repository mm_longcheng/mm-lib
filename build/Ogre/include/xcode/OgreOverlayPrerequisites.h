
#ifndef _OgreOverlayExport_H
#define _OgreOverlayExport_H

#ifdef OGREOVERLAY_STATIC_DEFINE
#  define _OgreOverlayExport
#  define OGREOVERLAY_NO_EXPORT
#else
#  ifndef _OgreOverlayExport
     /* We are building this library */
#    if __GNUC__ >= 4
#      define _OgreOverlayExport  __attribute__ ((visibility("default")))
#    else
#      define _OgreOverlayExport
#    endif
#  endif

#  ifndef OGREOVERLAY_NO_EXPORT
#    define OGREOVERLAY_NO_EXPORT 
#  endif
#endif

#ifndef OGREOVERLAY_DEPRECATED
#  define OGREOVERLAY_DEPRECATED __attribute__ ((deprecated))
#endif

#ifndef OGREOVERLAY_DEPRECATED_EXPORT
#  define OGREOVERLAY_DEPRECATED_EXPORT _OgreOverlayExport OGREOVERLAY_DEPRECATED
#endif

#ifndef OGREOVERLAY_DEPRECATED_NO_EXPORT
#  define OGREOVERLAY_DEPRECATED_NO_EXPORT OGREOVERLAY_NO_EXPORT OGREOVERLAY_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef OGREOVERLAY_NO_DEPRECATED
#    define OGREOVERLAY_NO_DEPRECATED
#  endif
#endif

#endif
