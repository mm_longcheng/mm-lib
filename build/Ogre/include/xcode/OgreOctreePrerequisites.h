
#ifndef _OgreOctreePluginExport_H
#define _OgreOctreePluginExport_H

#ifdef PLUGIN_OCTREESCENEMANAGER_STATIC_DEFINE
#  define _OgreOctreePluginExport
#  define PLUGIN_OCTREESCENEMANAGER_NO_EXPORT
#else
#  ifndef _OgreOctreePluginExport
     /* We are building this library */
#    if __GNUC__ >= 4
#      define _OgreOctreePluginExport  __attribute__ ((visibility("default")))
#    else
#      define _OgreOctreePluginExport
#    endif
#  endif

#  ifndef PLUGIN_OCTREESCENEMANAGER_NO_EXPORT
#    define PLUGIN_OCTREESCENEMANAGER_NO_EXPORT 
#  endif
#endif

#ifndef PLUGIN_OCTREESCENEMANAGER_DEPRECATED
#  define PLUGIN_OCTREESCENEMANAGER_DEPRECATED __attribute__ ((deprecated))
#endif

#ifndef PLUGIN_OCTREESCENEMANAGER_DEPRECATED_EXPORT
#  define PLUGIN_OCTREESCENEMANAGER_DEPRECATED_EXPORT _OgreOctreePluginExport PLUGIN_OCTREESCENEMANAGER_DEPRECATED
#endif

#ifndef PLUGIN_OCTREESCENEMANAGER_DEPRECATED_NO_EXPORT
#  define PLUGIN_OCTREESCENEMANAGER_DEPRECATED_NO_EXPORT PLUGIN_OCTREESCENEMANAGER_NO_EXPORT PLUGIN_OCTREESCENEMANAGER_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef PLUGIN_OCTREESCENEMANAGER_NO_DEPRECATED
#    define PLUGIN_OCTREESCENEMANAGER_NO_DEPRECATED
#  endif
#endif

#endif
