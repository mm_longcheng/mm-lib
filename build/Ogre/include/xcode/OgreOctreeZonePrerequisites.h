
#ifndef _OgreOctreeZonePluginExport_H
#define _OgreOctreeZonePluginExport_H

#ifdef PLUGIN_OCTREEZONE_STATIC_DEFINE
#  define _OgreOctreeZonePluginExport
#  define PLUGIN_OCTREEZONE_NO_EXPORT
#else
#  ifndef _OgreOctreeZonePluginExport
     /* We are building this library */
#    if __GNUC__ >= 4
#      define _OgreOctreeZonePluginExport  __attribute__ ((visibility("default")))
#    else
#      define _OgreOctreeZonePluginExport
#    endif
#  endif

#  ifndef PLUGIN_OCTREEZONE_NO_EXPORT
#    define PLUGIN_OCTREEZONE_NO_EXPORT 
#  endif
#endif

#ifndef PLUGIN_OCTREEZONE_DEPRECATED
#  define PLUGIN_OCTREEZONE_DEPRECATED __attribute__ ((deprecated))
#endif

#ifndef PLUGIN_OCTREEZONE_DEPRECATED_EXPORT
#  define PLUGIN_OCTREEZONE_DEPRECATED_EXPORT _OgreOctreeZonePluginExport PLUGIN_OCTREEZONE_DEPRECATED
#endif

#ifndef PLUGIN_OCTREEZONE_DEPRECATED_NO_EXPORT
#  define PLUGIN_OCTREEZONE_DEPRECATED_NO_EXPORT PLUGIN_OCTREEZONE_NO_EXPORT PLUGIN_OCTREEZONE_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef PLUGIN_OCTREEZONE_NO_DEPRECATED
#    define PLUGIN_OCTREEZONE_NO_DEPRECATED
#  endif
#endif

#endif
