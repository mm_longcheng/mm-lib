LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
########################################################################
include $(CLEAR_VARS)
########################################################################
LOCAL_MODULE := libvorbis_static
LOCAL_MODULE_FILENAME := libvorbis
########################################################################
LOCAL_CFLAGS += -fPIC

LOCAL_CFLAGS += -Wall
########################################################################
LOCAL_LDLIBS += 
########################################################################
LOCAL_SHARED_LIBRARIES += 
########################################################################
LOCAL_STATIC_LIBRARIES += 
########################################################################
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/libvorbis/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/libvorbis/lib
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../../src/libogg/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../libogg/include/$(TARGET_PLATFORM)
########################################################################
LOCAL_SRC_FILES  += 
########################################################################
#  
# config self source file path ,suffix.
MY_SOURCES_PATH += $(LOCAL_PATH)/../../../src/libvorbis/lib
MY_SOURCES_PATH += $(LOCAL_PATH)/$(TARGET_PLATFORM)

# config filter out file and path.
# MY_SOURCES_FILTER_OUT += ../../filter-out-directory%
# MY_SOURCES_FILTER_OUT += ../../filter-out-source.c
MY_SOURCES_FILTER_OUT += ../../../src/libvorbis/lib/psytune.c
MY_SOURCES_FILTER_OUT += ../../../src/libvorbis/lib/tone.c
MY_SOURCES_FILTER_OUT += ../../../src/libvorbis/lib/barkmel.c

MY_SOURCES_EXTENSION += .cpp .c .cc .S
include $(SOURCE_RWILDCARD)
########################################################################
include $(BUILD_STATIC_LIBRARY)
########################################################################
# $(modules-dump-database)
