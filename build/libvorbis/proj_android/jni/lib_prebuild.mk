################################################################################
# prebuild.mk
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libogg_shared
LOCAL_SRC_FILES := ../../../libogg/proj_android/libs/$(TARGET_ARCH_ABI)/libogg_shared.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libogg_static
LOCAL_SRC_FILES := ../../../libogg/proj_android/obj/local/$(TARGET_ARCH_ABI)/libogg_static.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################