LOCAL_PATH := $(call my-dir)
LOCAL_MAKEFILE := $(this-makefile)
################################################################################
# prebuild.mk
################################################################################
include $(CLEAR_VARS)  
LOCAL_MODULE := libogg_shared
LOCAL_SRC_FILES := $(LOCAL_PATH)/../../libogg/proj_$(TARGET_PLATFORM)/bin/libogg.so
include $(PREBUILT_SHARED_LIBRARY)
include $(CLEAR_VARS)  
LOCAL_MODULE := libogg_static
LOCAL_SRC_FILES := $(LOCAL_PATH)/../../libogg/proj_$(TARGET_PLATFORM)/bin/libogg.a
include $(PREBUILT_STATIC_LIBRARY)
################################################################################