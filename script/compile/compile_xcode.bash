#!/bin/bash

PLATFORM=xcode
ACTION=$@
#################################################
build_project()
{
	cd $1
	cd proj_${PLATFORM}
	bash compile.bash ${ACTION}
	cd ..
	cd ..
}
#################################################
cd ../../build
#################################################
# build_project "OpenAL"
# build_project "pthread"
###############
build_project "hiredis"
###############
build_project "zlib"
build_project "fdlibm"
build_project "openssl"
build_project "libiconv"
build_project "lua"
build_project "pcre"
build_project "tinyxml"
build_project "protobuf"
build_project "protobuf-c"
build_project "cJSON"
build_project "recastnavigation"
###############
build_project "sqlite"
build_project "zziplib"
###############
build_project "FreeImage"
build_project "freetype"
build_project "opus"
build_project "fdk-aac"
# build_project "x264"
build_project "rtmpdump"
build_project "libogg"
build_project "libvorbis"
# build_project "ffmpeg"
build_project "harfbuzz"
###############
build_project "curl"
build_project "zookeeper"
build_project "rabbitmq-c"
build_project "inih"
###############
build_project "OIS"
build_project "bullet3"
###############
build_project "Ogre"
build_project "cegui"
build_project "ogre-oggsound"
build_project "OgreAL"
build_project "ParticleUniverse"
#################################################
cd ../script/compile
#################################################
