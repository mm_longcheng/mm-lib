@echo off
:::::::::::::::::::::::::::::::::::::::::::::::::
set PLATFORM=android
set ACTION=%*
:::::::::::::::::::::::::::::::::::::::::::::::::
cd ../../build
:::::::::::::::::::::::::::::::::::::::::::::::::
call :build_project "OpenAL"
:: call :build_project "pthread"
:::::::::::::::
call :build_project "hiredis"
:::::::::::::::
call :build_project "zlib"
call :build_project "fdlibm"
call :build_project "openssl"
call :build_project "libiconv"
call :build_project "lua"
call :build_project "pcre"
call :build_project "tinyxml"
call :build_project "protobuf"
call :build_project "protobuf-c"
call :build_project "cJSON"
call :build_project "recastnavigation"
:::::::::::::::
call :build_project "sqlite"
call :build_project "zziplib"
:::::::::::::::
call :build_project "FreeImage"
call :build_project "freetype"
call :build_project "opus"
call :build_project "fdk-aac"
call :build_project "x264"
call :build_project "rtmpdump"
call :build_project "libogg"
call :build_project "libvorbis"
call :build_project "ffmpeg"
call :build_project "harfbuzz"
:::::::::::::::
call :build_project "curl"
call :build_project "zookeeper"
call :build_project "rabbitmq-c"
call :build_project "inih"
:::::::::::::::
call :build_project "OIS"
call :build_project "bullet3"
:::::::::::::::
call :build_project "Ogre"
call :build_project "cegui"
call :build_project "ogre-oggsound"
call :build_project "OgreAL"
call :build_project "ParticleUniverse"
:::::::::::::::::::::::::::::::::::::::::::::::::
cd ../script/compile
:::::::::::::::::::::::::::::::::::::::::::::::::
pause
::
GOTO :EOF
:::::::::::::::::::::::::::::::::::::::::::::::::
:: build_project function.
:build_project
SETLOCAL
:: REM.
cd %~1
@echo compile %~1
cd proj_%PLATFORM%
echo y | compile.bat %ACTION%
cd ..
cd ..
ENDLOCAL
GOTO :EOF
:::::::::::::::::::::::::::::::::::::::::::::::::
pause