#######################
# platform special
#######################
ndk                    21.0.6113669
#######################
hiredis                0.13
pthread                2.91
#######################
# common
#######################
zlib                   1.2.11
openssl                1.1.1c
libiconv               1.15
lua                    5.4.2
pcre                   8.39
tinyxml                2.6.2
protobuf-c             0.15
protobuf               3.3.3
zookeeper              4.4.10
rapidjson              1.1.0
fdlibm                 5.3
cJSON                  1.3.2
inih                   1.0
rabbitmq-c             0.8.0
#######################
# expand
#######################
bullet                 2.82
cegui                  0.8.7
FreeImage              3.17.0
freetype               2.7
libogg                 1.3.2
libvorbis              1.3.5
Ogre                   1.10.11
OgreAL-Eihort          1.0.0
ogre-oggsound          1.26
ois                    1.0.0
OpenAL                 1.18.2
sqlite                 3.2.6
zziplib                0.13.59
SDL2                   2.0.8
recastnavigation       1.0.0
#######################
opus                   1.2.1
x264                   0.155
ffmpeg                 4.0
fdk-aac                0.1.6
rtmpdump               2.3
#######################
# expand
#######################
curl                   7.57.0
harfbuzz               2.8.1
