Demo8 = {}

function Demo8:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self
  self.pGUIContext = nil
  self.pRootWindow = nil
  return o
end

-----------------------------------------
-- Start of handler functions
-----------------------------------------
-----------------------------------------
-- Alpha slider handler (not used!)
-----------------------------------------
function Demo8:sliderHandler(args)
    self.pGUIContext:getRootWindow():setAlpha(CEGUI.toSlider(CEGUI.toWindowEventArgs(args).window):getCurrentValue())
end

-----------------------------------------
-- Handler to slide pane
--
-- Here we move the 'Demo8' sheet window
-- and re-position the scrollbar
-----------------------------------------
function Demo8:panelSlideHandler(args)
    local scroller = CEGUI.toScrollbar(CEGUI.toWindowEventArgs(args).window)
    local demoWnd = self.pGUIContext:getRootWindow():getChild("Demo8")

    local parentPixelHeight = demoWnd:getParent():getPixelSize().height
    local relHeight = CEGUI.CoordConverter:asRelative(demoWnd:getHeight(), parentPixelHeight)

    scroller:setPosition(CEGUI.UVector2(CEGUI.UDim(0,0), CEGUI.UDim(scroller:getScrollPosition() / relHeight,0)))
    demoWnd:setPosition(CEGUI.UVector2(CEGUI.UDim(0,0), CEGUI.UDim(-scroller:getScrollPosition(),0)))
end

-----------------------------------------
-- Handler to set preview colour when
-- colour selector scrollers change
-----------------------------------------
function Demo8:colourChangeHandler(args)
    local root = self.pGUIContext:getRootWindow()
    
    local r = CEGUI.toScrollbar(root:getChild("Demo8/Window1/Controls/Red")):getScrollPosition()
    local g = CEGUI.toScrollbar(root:getChild("Demo8/Window1/Controls/Green")):getScrollPosition()
    local b = CEGUI.toScrollbar(root:getChild("Demo8/Window1/Controls/Blue")):getScrollPosition()
    local col = CEGUI.Colour:new_local(r, g, b, 1)
    local crect = CEGUI.ColourRect(col)

    root:getChild("Demo8/Window1/Controls/ColourSample"):setProperty("ImageColours", CEGUI.PropertyHelper:colourRectToString(crect))
end


-----------------------------------------
-- Handler to add an item to the box
-----------------------------------------
function Demo8:addItemHandler(args)
    local root = self.pGUIContext:getRootWindow()

    local text = root:getChild("Demo8/Window1/Controls/Editbox"):getText()
    local cols = CEGUI.PropertyHelper:stringToColourRect(root:getChild("Demo8/Window1/Controls/ColourSample"):getProperty("ImageColours"))

    local newItem = CEGUI.createListboxTextItem(text, 0, nil, false, true)
    newItem:setSelectionBrushImage("TaharezLook/MultiListSelectionBrush")
    newItem:setSelectionColours(cols)

    CEGUI.toListbox(root:getChild("Demo8/Window1/Listbox")):addItem(newItem)
end

function initialise(pGUIContext)
	local p = Demo8:new(nil)
	p.pGUIContext = pGUIContext
	-----------------------------------------
	-- Script Entry Point
	-----------------------------------------
	local guiSystem = CEGUI.System:getSingleton()
	local schemeMgr = CEGUI.SchemeManager:getSingleton()
	local winMgr = CEGUI.WindowManager:getSingleton()

	-- load our demo8 scheme
	schemeMgr:createFromFile("TaharezLook.scheme");
	-- load our demo8 window layout
	local root = winMgr:loadLayoutFromFile("Demo8.layout")
	-- set the layout as the root
	pGUIContext:setRootWindow(root)
	-- set default mouse cursor
	pGUIContext:getMouseCursor():setDefaultImage("TaharezLook/MouseArrow")
	-- set the Tooltip type
	pGUIContext:setDefaultTooltipType("TaharezLook/Tooltip")

	-- subscribe required events
	root:getChild("Demo8/ViewScroll"):subscribeEvent("ScrollPositionChanged", "Demo8.panelSlideHandler", p)
	root:getChild("Demo8/Window1/Controls/Blue"):subscribeEvent("ScrollPositionChanged", "Demo8.colourChangeHandler", p)
	root:getChild("Demo8/Window1/Controls/Red"):subscribeEvent("ScrollPositionChanged", "Demo8.colourChangeHandler", p)
	root:getChild("Demo8/Window1/Controls/Green"):subscribeEvent("ScrollPositionChanged", "Demo8.colourChangeHandler", p)
	root:getChild("Demo8/Window1/Controls/Add"):subscribeEvent("Clicked", "Demo8.addItemHandler", p)
	
	p.pRootWindow = root
	return p
end

function deinitialise(p)
	local winMgr = CEGUI.WindowManager:getSingleton()
	
	local root = p.pRootWindow
	root:getChild("Demo8/ViewScroll"):removeAllEvents()
	root:getChild("Demo8/Window1/Controls/Blue"):removeAllEvents()
	root:getChild("Demo8/Window1/Controls/Red"):removeAllEvents()
	root:getChild("Demo8/Window1/Controls/Green"):removeAllEvents()
	root:getChild("Demo8/Window1/Controls/Add"):removeAllEvents()
	
    p.pGUIContext:setRootWindow(nil)
	
	p.pRootWindow = nil
	p.pGUIContext = nil
	winMgr:destroyWindow(root)
	p = nil
end
