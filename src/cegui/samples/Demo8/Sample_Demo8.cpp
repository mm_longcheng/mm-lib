/***********************************************************************
    created:    20/8/2005
    author:     Paul D Turner
*************************************************************************/
/***************************************************************************
 *   Copyright (C) 2004 - 2006 Paul D Turner & The CEGUI Development Team
 *
 *   Permission is hereby granted, free of charge, to any person obtaining
 *   a copy of this software and associated documentation files (the
 *   "Software"), to deal in the Software without restriction, including
 *   without limitation the rights to use, copy, modify, merge, publish,
 *   distribute, sublicense, and/or sell copies of the Software, and to
 *   permit persons to whom the Software is furnished to do so, subject to
 *   the following conditions:
 *
 *   The above copyright notice and this permission notice shall be
 *   included in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *   IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 *   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 *   ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 *   OTHER DEALINGS IN THE SOFTWARE.
 ***************************************************************************/
#include "Sample_Demo8.h"
#include "CEGUI/CEGUI.h"
#include "CEGUI/ScriptModules/Lua/ScriptModule.h"

extern "C" 
{
#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"

#include "tolua++.h"
}

/*************************************************************************
Sample specific initialisation goes here.
*************************************************************************/
bool Demo8Sample::initialise(CEGUI::GUIContext* guiContext)
{
    this->d_usedFiles = CEGUI::String(__FILE__);

    using namespace CEGUI;

    // create a script module.
    LuaScriptModule& hScriptModule(LuaScriptModule::create());

    // tell CEGUI to use this scripting module
    System* pSystem = System::getSingletonPtr();
    pSystem->setScriptingModule(&hScriptModule);

    // execute the demo8 script which controls the rest of this demo
    pSystem->executeScriptFile("demo8.lua");

    lua_State* L = hScriptModule.getLuaState();

    // call it
    tolua_pushusertype(L, (void*)guiContext, "CEGUI::GUIContext");
    lua_getglobal(L, "initialise");
    lua_insert(L, -(1 + 1));
    lua_pcall(L, 1, 1, 0);
    tolua_tovalue(L, -1, LUA_NOREF);
    this->d_index = luaL_ref(L, LUA_REGISTRYINDEX);
    lua_pop(L, 1);

    // success!
    return true;
}

/*************************************************************************
Cleans up resources allocated in the initialiseSample call.
*************************************************************************/
void Demo8Sample::deinitialise()
{
    using namespace CEGUI;

    System* pSystem = System::getSingletonPtr();
    LuaScriptModule* pScriptModule = static_cast<LuaScriptModule*>(pSystem->getScriptingModule());

    lua_State* L = pScriptModule->getLuaState();

    if (this->d_index != LUA_NOREF)
    {
        lua_rawgeti(L, LUA_REGISTRYINDEX, this->d_index);
        lua_getglobal(L, "deinitialise");
        lua_insert(L, -(1 + 1));
        lua_pcall(L, 1, 0, 0);
        luaL_unref(L, LUA_REGISTRYINDEX, this->d_index);
        this->d_index = LUA_NOREF;
    }

    // clear script module, since we're going to destroy it.
    pSystem->setScriptingModule(NULL);

    LuaScriptModule::destroy(*pScriptModule);
}

/*************************************************************************
Define the module function that returns an instance of the sample
*************************************************************************/
extern "C" SAMPLE_EXPORT Sample& getSampleInstance()
{
    static Demo8Sample sample;
    return sample;
}