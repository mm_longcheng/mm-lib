#ifndef _WIN32_HELPER_INCLUDE
#define _WIN32_HELPER_INCLUDE
#ifdef _MSC_VER
#include <winsock2.h>

#ifndef inline
#define inline __inline
#endif

#ifndef va_copy 
# ifdef __va_copy 
# define va_copy(DEST,SRC) __va_copy((DEST),(SRC)) 
# else 
# define va_copy(DEST, SRC) memcpy((&DEST), (&SRC), sizeof(va_list)) 
# endif 
#endif

#define close msvc_close
#define read msvc_read
#define write msvc_write

#define strerror_r(errno,buf,len) strerror_s(buf,len,errno)

#define strcasecmp _stricmp
#define strncasecmp  _strnicmp 

#define strdup _strdup

/* _vsnprintf_s at msvc will set buffer to -52 array.
	but at sds sdscatvprintf has this check.Can not break until out of memory.   
	buf[buflen-2] = '\0';
	va_copy(cpy,ap);
	vsnprintf(buf, buflen, fmt, cpy);
	va_end(cpy);
	if (buf[buflen-2] != '\0'){...}
*/
//#define vsnprintf c99_vsnprintf
//#define snprintf c99_snprintf
// we use this function.
#define vsnprintf vsnprintf
#define snprintf _snprintf

inline int c99_vsnprintf(char* str, size_t size, const char* format, va_list ap)
{
	int count = -1;

	if (size != 0)
		count = _vsnprintf_s(str, size, _TRUNCATE, format, ap);
	if (count == -1)
		count = _vscprintf(format, ap);

	return count;
}

inline int c99_snprintf(char* str, size_t size, const char* format, ...)
{
	int count;
	va_list ap;

	va_start(ap, format);
	count = c99_vsnprintf(str, size, format, ap);
	va_end(ap);

	return count;
}
inline int msvc_close(int fd)
{
	return closesocket(fd);
}
inline int msvc_read(int fd, char* buffer,int len)
{
	return recv(fd,buffer,len,0);
}
inline int msvc_write(int fd, char* buffer,int len)
{
	return send(fd,buffer,len,0);
}
#endif//_MSC_VER

#endif//_WIN32_HELPER_INCLUDE