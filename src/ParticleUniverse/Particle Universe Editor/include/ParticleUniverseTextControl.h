/*
-----------------------------------------------------------------------------------------------
This source file is part of the Particle Universe product.

Copyright (c) 2012 Henry van Merode

Usage of this program is licensed under the terms of the Particle Universe Commercial License.
You can find a copy of the Commercial License in the Particle Universe package.
-----------------------------------------------------------------------------------------------
*/

#ifndef __PUED_TEXTCONTROL_H__
#define __PUED_TEXTCONTROL_H__

#include "wx/ogre/prerequisites.h"

#include "wx/stc/stc.h"

#define PU_STC_STYLE_TOKEN_BASE ( wxSTC_STYLE_LASTPREDEFINED + 10 )

#define PU_STC_STYLE_TOKEN_SYSTEM    ( PU_STC_STYLE_TOKEN_BASE + 0 )
#define PU_STC_STYLE_TOKEN_TECHNIQUE ( PU_STC_STYLE_TOKEN_BASE + 1 )
#define PU_STC_STYLE_TOKEN_RENDERER  ( PU_STC_STYLE_TOKEN_BASE + 2 )
#define PU_STC_STYLE_TOKEN_EMITTER   ( PU_STC_STYLE_TOKEN_BASE + 3 )
#define PU_STC_STYLE_TOKEN_AFFECTOR  ( PU_STC_STYLE_TOKEN_BASE + 4 )
#define PU_STC_STYLE_TOKEN_OBSERVER  ( PU_STC_STYLE_TOKEN_BASE + 5 )
#define PU_STC_STYLE_TOKEN_HANDLER   ( PU_STC_STYLE_TOKEN_BASE + 6 )
#define PU_STC_STYLE_TOKEN_BEHAVIOUR ( PU_STC_STYLE_TOKEN_BASE + 7 )
#define PU_STC_STYLE_TOKEN_EXTERN    ( PU_STC_STYLE_TOKEN_BASE + 8 )
#define PU_STC_STYLE_TOKEN_MAX       ( PU_STC_STYLE_TOKEN_BASE + 9 )

class ParticleUniverseEditorFrame;
class TextControl : public wxStyledTextCtrl
{
public:
	TextControl(ParticleUniverseEditorFrame* frame, wxWindow *parent, wxWindowID id,
            const wxString& value = wxT(""),
            const wxPoint& pos = wxDefaultPosition,
            const wxSize& size = wxDefaultSize,
            long style = 0,
            const wxValidator& validator = wxDefaultValidator,
			const wxString& name = wxTextCtrlNameStr);
	virtual ~TextControl(){};
		
	/**
		Detect that the script has been changed
	*/
	void OnTextUpdated(wxStyledTextEvent& evt);

	/**
		Detect that the script style needed.
	*/
	void OnTextStyleNeeded(wxStyledTextEvent& evt);

	/**
		Detect key events
	*/
	void OnKeyPressed(wxKeyEvent& event);

	/**
		Get/Set flag to highlight the keywords
	*/
	void setHighlightKeywords(bool highlightKeywords);
	bool getHighlightKeywords(void) const;

	// Rewrite
	// Replace the contents of the document with the argument text.
	void SetText(const wxString& text);

protected:
	ParticleUniverseEditorFrame* mFrame;
	bool mHighlightKeywords;
	bool mPaste;

	//      bold                    turns on bold
	//      italic                  turns on italics
	//      fore:[name or #RRGGBB]  sets the foreground colour
	//      back:[name or #RRGGBB]  sets the background colour
	//      face:[facename]         sets the font face name to use
	//      size:[num]              sets the font size in points
	//      eol                     turns on eol filling
	//      underline               turns on underlining
	struct StyleSetSpecData
	{
	public:
		StyleSetSpecData();
	public:
		bool bold;
		bool italic;
		bool eol;
		bool underline;
		int size;
		std::string face;
		std::string fore;
		std::string back;
	};
	struct StyleTokenData
	{
	public:
		StyleTokenData();
	public:
		StyleSetSpecData* data;
		int style;
	};

	StyleSetSpecData mStyleGlobal[wxSTC_STYLE_CONTROLCHAR - wxSTC_STYLE_DEFAULT + 1];
	StyleSetSpecData mStyleToken[PU_STC_STYLE_TOKEN_MAX - PU_STC_STYLE_TOKEN_BASE];
	StyleSetSpecData mStyle[24];

	typedef std::map<wxString, StyleTokenData> HighlightedTextType;
	HighlightedTextType mHighlightedText;
	wxString mKeywords;
protected:
	void SetStyleSetSpecHighlight();
	void SetStyleSetSpecBlankness();
	void GetStyleSetSpecWordsStyle(const StyleSetSpecData& style, std::string& wordsStyle);
	void ApplyStyleSetSpec();
protected:
	virtual void MarkDirty();
	virtual bool SetStyle(long start, long end, const wxTextAttr& style);
	virtual bool GetStyle(long position, wxTextAttr& style);
	virtual bool SetDefaultStyle(const wxTextAttr& style);
protected:
	void checkKeywords(long from, long to, bool freeze = true);
	void checkKeyword(long from, long to);
	bool validChar(long pos);
	void findWordToRight(long insert, long& from, long& to);
	void findWordToLeft(long insert, long& from, long& to);
protected:
	void SetStyleById(long start, long end, int styleId);
	void BindHighLightedText(int style, wxString str);
};

#endif
