#include "ParticleUniverseEditorPCH.h"
#include "ParticleUniverseOgreRTshader.h"

#include "OgreTechnique.h"

ParticleUniverseOgreRTshader::ParticleUniverseOgreRTshader()
	: d_shader_generator(NULL)
{

}

void ParticleUniverseOgreRTshader::AssignShaderGenerator(Ogre::RTShader::ShaderGenerator* pShaderGenerator)
{
	this->d_shader_generator = pShaderGenerator;
}

Ogre::Technique *ParticleUniverseOgreRTshader::handleSchemeNotFound(unsigned short schemeIndex, const Ogre::String &schemeName, Ogre::Material *originalMaterial, unsigned short lodIndex, const Ogre::Renderable *rend)
{
	if (schemeName != Ogre::RTShader::ShaderGenerator::DEFAULT_SCHEME_NAME)
	{
		return NULL;
	}
	// Case this is the default shader generator scheme.

	// Create shader generated technique for this material.
	bool techniqueCreated = this->d_shader_generator->createShaderBasedTechnique(
		*originalMaterial,
		Ogre::MaterialManager::DEFAULT_SCHEME_NAME,
		schemeName);

	if (!techniqueCreated)
	{
		return NULL;
	}
	// Case technique registration succeeded.

	// Force creating the shaders for the generated technique.
	this->d_shader_generator->validateMaterial(schemeName, originalMaterial->getName(), originalMaterial->getGroup());

	// Grab the generated technique.
	Ogre::Material::Techniques::const_iterator it;
	for (it = originalMaterial->getTechniques().begin(); it != originalMaterial->getTechniques().end(); ++it)
	{
		Ogre::Technique* curTech = *it;

		if (curTech->getSchemeName() == schemeName)
		{
			return curTech;
		}
	}

	return NULL;
}

bool ParticleUniverseOgreRTshader::afterIlluminationPassesCreated(Ogre::Technique *tech)
{
	if (tech->getSchemeName() == Ogre::RTShader::ShaderGenerator::DEFAULT_SCHEME_NAME)
	{
		Ogre::Material* mat = tech->getParent();
		this->d_shader_generator->validateMaterialIlluminationPasses(tech->getSchemeName(), mat->getName(), mat->getGroup());
		return true;
	}
	return false;
}

bool ParticleUniverseOgreRTshader::beforeIlluminationPassesCleared(Ogre::Technique *tech)
{
	if (tech->getSchemeName() == Ogre::RTShader::ShaderGenerator::DEFAULT_SCHEME_NAME)
	{
		Ogre::Material* mat = tech->getParent();
		this->d_shader_generator->invalidateMaterialIlluminationPasses(tech->getSchemeName(), mat->getName(), mat->getGroup());
		return true;
	}
	return false;
}

