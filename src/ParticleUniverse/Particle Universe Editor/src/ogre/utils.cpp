/*
 * Copyright (C) 2007-2008 Martin Pieuchot 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */

#include "ParticleUniverseEditorPCH.h"
#include "wx/ogre/utils.h"

#include "ParticleUniverseSystemManager.h"

#include <string>
#include <algorithm>

//------------------------------------------------------------------------------
void wxOgreExceptionBox(Ogre::Exception& e)
{
    wxMessageBox(std2wx(e.getFullDescription()),  _T("OGRE Exception"),
                 wxICON_ERROR);
}
//------------------------------------------------------------------------------
std::string wx2std(const wxString& s)
{
    return std::string(s.mb_str());
}
//------------------------------------------------------------------------------
wxString std2wx(const std::string& s)
{
    return wxString::FromAscii(s.c_str());
}
//------------------------------------------------------------------------------
wxString ogre2wx(const Ogre::String& s)
{
	return wxString::FromAscii(s.c_str());
}
//------------------------------------------------------------------------------
wxString ogre2wxTranslate(const Ogre::String& s)
{
	return wxGetTranslation(wxString::FromAscii(s.c_str()));
}
//------------------------------------------------------------------------------
Ogre::String wx2ogre(const wxString& s)
{
	//return Ogre::String(s.mb_str());
	return Ogre::String(s.mb_str(wxConvUTF8));
}
//------------------------------------------------------------------------------
std::string getExtension(const std::string& s)
{
    std::string ext;

    // Find the dot position.
    size_t pos = s.rfind('.');

    if (pos != std::string::npos)
        ext = s.substr(pos);

    return ext;
}
//------------------------------------------------------------------------------
void getParticleSystemFirstPatternPathName(const wxString& particleSystemName, const Ogre::String& extension, Ogre::FileInfo& fileInfo)
{
	Ogre::String particleSystemNameOgre = wx2ogre(particleSystemName);

	ParticleUniverse::ParticleSystemManager* _particle_system_manager = ParticleUniverse::ParticleSystemManager::getSingletonPtr();
	ParticleUniverse::ParticleSystem* _particleSystem = _particle_system_manager->getParticleSystemTemplate(particleSystemNameOgre);
	do
	{
		if (NULL == _particleSystem)
		{
			// not find this particle system template.
			break;
		}
		Ogre::ResourceGroupManager* _resource_group_manager = Ogre::ResourceGroupManager::getSingletonPtr();
		const Ogre::String& _resourceGroupName = _particleSystem->getResourceGroupName();
		Ogre::String _templateFileName = _particleSystem->getTemplateFileName();
		Ogre::FileInfoListPtr _fileList = _resource_group_manager->findResourceFileInfo(_resourceGroupName, _templateFileName);
		if (false == _fileList->empty())
		{
			Ogre::FileInfoList::iterator it = _fileList->begin();
			fileInfo = *it;
			break;
		}
		_fileList = _resource_group_manager->findResourceFileInfo(_resourceGroupName, extension);
		if (true == _fileList->empty())
		{
			// not find this particle system template.
			break;
		}
		Ogre::FileInfoList::iterator it = _fileList->begin();
		fileInfo = *it;
	} while (false);
}

void reloadParticleSystemByNameAndFile(const wxString& particleSystemName, const wxString& particleSystemFile)
{
	Ogre::String particleSystemNameOgre = wx2ogre(particleSystemName);
	Ogre::String savedFileOgre = wx2ogre(particleSystemFile);

	do
	{
		ParticleUniverse::ParticleSystemManager* _particle_system_manager = ParticleUniverse::ParticleSystemManager::getSingletonPtr();
		ParticleUniverse::ParticleSystem* _particleSystem = _particle_system_manager->getParticleSystemTemplate(particleSystemNameOgre);
		if (NULL == _particleSystem)
		{
			// not find this particle system template.
			break;
		}
		const Ogre::String& _resourceGroupName = _particleSystem->getResourceGroupName();

		Ogre::ScriptCompilerManager* _script_compiler_manager = Ogre::ScriptCompilerManager::getSingletonPtr();
		Ogre::ResourceGroupManager* _resource_group_manager = Ogre::ResourceGroupManager::getSingletonPtr();
		if (false == _resource_group_manager->resourceExists(_resourceGroupName, savedFileOgre))
		{
			// not find this particle system template file.
			break;
		}
		Ogre::DataStreamPtr dataStream = _resource_group_manager->createResource(savedFileOgre, _resourceGroupName);
		if (!dataStream)
		{
			// not find this particle system template file.
			break;
		}
		_script_compiler_manager->parseScript(dataStream, _resourceGroupName);
	} while (false);
}

void getMaterialFirstPatternPathName(const wxString& materialName, const Ogre::String& extension, Ogre::FileInfo& fileInfo)
{
	Ogre::String materialNameOgre = wx2ogre(materialName);

	Ogre::MaterialManager* _materialManager = Ogre::MaterialManager::getSingletonPtr();
	Ogre::MaterialPtr _material = _materialManager->getByName(materialNameOgre, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
	do
	{
		if (!_material)
		{
			// not find this material template.
			break;
		}
		Ogre::ResourceGroupManager* _resource_group_manager = Ogre::ResourceGroupManager::getSingletonPtr();
		const Ogre::String& _resourceGroupName = _material->getGroup();
		Ogre::String _templateFileName = _material->getOrigin();
		Ogre::FileInfoListPtr _fileList = _resource_group_manager->findResourceFileInfo(_resourceGroupName, _templateFileName);
		if (false == _fileList->empty())
		{
			Ogre::FileInfoList::iterator it = _fileList->begin();
			fileInfo = *it;
			break;
		}
		_fileList = _resource_group_manager->findResourceFileInfo(_resourceGroupName, extension);
		if (true == _fileList->empty())
		{
			// not find this material template.
			break;
		}
		Ogre::FileInfoList::iterator it = _fileList->begin();
		fileInfo = *it;
	} while (false);
}

Ogre::String getFullPathNameByFileInfo(const Ogre::FileInfo& fileInfo)
{
	Ogre::String fullPathName = "";

	if (NULL != fileInfo.archive)
	{
		if ("" == fileInfo.path)
		{
			fullPathName = fileInfo.archive->getName();
		}
		else
		{
			fullPathName = fileInfo.archive->getName() + "/" + fileInfo.path;
		}
	}
	return fullPathName;
}
Ogre::String getFullFileNameByFileInfo(const Ogre::FileInfo& fileInfo)
{
	Ogre::String fullFileName = "";

	if (NULL != fileInfo.archive)
	{
		fullFileName = fileInfo.archive->getName() + "/" + fileInfo.filename;
	}
	return fullFileName;
}