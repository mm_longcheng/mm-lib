/*
-----------------------------------------------------------------------------------------------
Copyright (C) 2014 Henry van Merode. All rights reserved.

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
-----------------------------------------------------------------------------------------------
*/

#include "ParticleUniverseEditorPCH.h"
#include "ParticleUniverseTextControl.h"
#include "ParticleUniverseEditor.h"
#include <assert.h>
//-----------------------------------------------------------------------
TextControl::TextControl(ParticleUniverseEditorFrame* frame, wxWindow *parent, wxWindowID id,
               const wxString& value,
               const wxPoint& pos,
               const wxSize& size,
               long style,
               const wxValidator& validator,
			   const wxString& name) : 
	wxStyledTextCtrl(parent, id, pos, size, style, name),
	mFrame(frame),
	mHighlightKeywords(true),
	mPaste(false),
	mKeywords("")
{
	// Set lexer.
	this->SetLexer(wxSTC_LEX_CONTAINER);

	this->BindHighLightedText(PU_STC_STYLE_TOKEN_SYSTEM   , "system"   );
	this->BindHighLightedText(PU_STC_STYLE_TOKEN_TECHNIQUE, "technique");
	this->BindHighLightedText(PU_STC_STYLE_TOKEN_RENDERER , "renderer" );
	this->BindHighLightedText(PU_STC_STYLE_TOKEN_EMITTER  , "emitter"  );
	this->BindHighLightedText(PU_STC_STYLE_TOKEN_AFFECTOR , "affector" );
	this->BindHighLightedText(PU_STC_STYLE_TOKEN_OBSERVER , "observer" );
	this->BindHighLightedText(PU_STC_STYLE_TOKEN_HANDLER  , "handler"  );
	this->BindHighLightedText(PU_STC_STYLE_TOKEN_BEHAVIOUR, "behaviour");
	this->BindHighLightedText(PU_STC_STYLE_TOKEN_EXTERN   , "extern"   );

	// Give a list of keywords. They will be given the style specified for
	// wxSTC_C_WORD items.
	this->mKeywords += " Beam";
	this->mKeywords += " Billboard";
	this->mKeywords += " Box";
	this->mKeywords += " Entity";
	this->mKeywords += " Light";
	this->mKeywords += " RibbonTrail";
	this->mKeywords += " Sphere";
	this->SetKeyWords(0, this->mKeywords);

	// Set the color to use for various elements
	// Reset all to be like the default
	this->StyleClearAll();
	this->SetStyleSetSpecHighlight();
	this->ApplyStyleSetSpec();

	// StyleBits to 8.
	this->SetStyleBits(8);

	// margin number.
	this->SetMarginType(1, wxSTC_MARGIN_NUMBER);
	this->SetMarginWidth(1, 40);

	Connect(wxEVT_STC_CHANGE, wxStyledTextEventHandler(TextControl::OnTextUpdated));
	Connect(wxEVT_STC_STYLENEEDED, wxStyledTextEventHandler(TextControl::OnTextStyleNeeded));

	Connect(wxEVT_KEY_DOWN, wxKeyEventHandler(TextControl::OnKeyPressed));
};
//-----------------------------------------------------------------------
void TextControl::OnTextUpdated(wxStyledTextEvent& evt)
{
	if (IsFrozen())
		return;

	mFrame->notifyScriptChanged();
}
//-----------------------------------------------------------------------
void TextControl::OnTextStyleNeeded(wxStyledTextEvent& evt)
{
	if (IsFrozen())
		return;

	if (!mHighlightKeywords)
		return;

	if (mPaste)
	{
		long insert = GetInsertionPoint() == GetLastPosition() ? GetLastPosition() : GetInsertionPoint() + 1;

		// First reset to black
		this->SetStyleById(0, insert, wxSTC_STYLE_DEFAULT);

		// Then check for keywords
		checkKeywords(0, insert, false);
		mPaste = false;
		return;
	}

	// Set the colours
	long insert = GetInsertionPoint();
	long x = 0;
	long line = 0;
	{
		long from = 0;
		long to = 0;
		long l = 0;

		// Check the location of the insertion point in relation to the word
		if (insert > 1 && insert < GetLastPosition() && validChar(insert) && !validChar(insert - 1) && validChar(insert - 2))
		{
			// 1. Insertion is between words, separated by 1 non-character
			findWordToLeft(insert - 1, from, to);
			checkKeyword(from, to);

			findWordToRight(insert, from, to);
			checkKeyword(from, to);
		}
		else if (insert > 0 && validChar(insert) && validChar(insert - 1))
		{
			// 2. Insertion point is in a word

			// Get the left part of the word
			l = insert;
			while (l >= 0 && validChar(l))
			{
				l--;
			}
			from = l + 1;

			// Get the right part of the word
			l = insert;
			while (l < GetLastPosition() && validChar(l))
			{
				l++;
			}
			to = l;
			checkKeyword(from, to);
		}
		else if (validChar(insert))
		{
			// 3. Insertion point is before a word
			findWordToRight(insert, from, to);
			checkKeyword(from, to);
		}
		else if (insert > 0 && validChar(insert - 1))
		{
			// 4. Insertion point is after a word
			findWordToLeft(insert, from, to);
			checkKeyword(from, to);
		}
	}
}
//-----------------------------------------------------------------------
void TextControl::OnKeyPressed(wxKeyEvent& event)
{
	event.Skip();
	//
	int _keyCode = event.GetKeyCode();
	int _modifiers = event.GetModifiers();

	// support internal.

	// CTRL-V => Paste
	// CTRL-C => Copy
	// CTRL-X => Cut

	if ((_keyCode == 'z' || _keyCode == 'Z') && _modifiers == wxMOD_CONTROL)
	{
		// CTRL-Z => Undo
		this->Undo();
	}
	else if ((_keyCode == 'y' || _keyCode == 'Y') && _modifiers == wxMOD_CONTROL)
	{
		// CTRL-Y => Redo
		this->Redo();
	}
	else if ((_keyCode == 's' || _keyCode == 'S') && _modifiers == wxMOD_CONTROL)
	{
		// CTRL-S => save
		this->mFrame->doSave();
	}

	// use for style.
	if ((_keyCode == 'v' || _keyCode == 'V') && _modifiers == wxMOD_CONTROL)
	{
		// CTRL-V => Paste
		mPaste = true;
	}
	else if (_keyCode == WXK_INSERT && _modifiers == wxMOD_SHIFT)
	{
		// SHIFT-INSERT => Paste
		mPaste = true;
	}
}
//-----------------------------------------------------------------------
void TextControl::setHighlightKeywords(bool highlightKeywords)
{
	mHighlightKeywords = highlightKeywords;
	if (highlightKeywords)
	{
		this->SetStyleSetSpecHighlight();
		this->ApplyStyleSetSpec();
	}
	else
	{
		this->SetStyleSetSpecBlankness();
		this->ApplyStyleSetSpec();
	}
}
//-----------------------------------------------------------------------
bool TextControl::getHighlightKeywords(void) const
{
	return mHighlightKeywords;
}

void TextControl::SetStyleSetSpecHighlight()
{
	this->mStyleGlobal[wxSTC_STYLE_DEFAULT     - wxSTC_STYLE_DEFAULT].fore = "#000000"; this->mStyleGlobal[wxSTC_STYLE_DEFAULT     - wxSTC_STYLE_DEFAULT].back = "#FFFFFF";
	this->mStyleGlobal[wxSTC_STYLE_LINENUMBER  - wxSTC_STYLE_DEFAULT].fore = "#000000"; this->mStyleGlobal[wxSTC_STYLE_LINENUMBER  - wxSTC_STYLE_DEFAULT].back = "#C0C0C0";
	this->mStyleGlobal[wxSTC_STYLE_BRACELIGHT  - wxSTC_STYLE_DEFAULT].fore = "#000000"; this->mStyleGlobal[wxSTC_STYLE_BRACELIGHT  - wxSTC_STYLE_DEFAULT].back = "#FFFFFF";
	this->mStyleGlobal[wxSTC_STYLE_BRACEBAD    - wxSTC_STYLE_DEFAULT].fore = "#FFFFFF"; this->mStyleGlobal[wxSTC_STYLE_BRACEBAD    - wxSTC_STYLE_DEFAULT].back = "#0000FF";
	this->mStyleGlobal[wxSTC_STYLE_CONTROLCHAR - wxSTC_STYLE_DEFAULT].fore = "#000000"; this->mStyleGlobal[wxSTC_STYLE_CONTROLCHAR - wxSTC_STYLE_DEFAULT].back = "#FF0000";

	this->mStyleToken[PU_STC_STYLE_TOKEN_SYSTEM    - PU_STC_STYLE_TOKEN_BASE].fore = "#151B8D";
	this->mStyleToken[PU_STC_STYLE_TOKEN_TECHNIQUE - PU_STC_STYLE_TOKEN_BASE].fore = "#6698FF";
	this->mStyleToken[PU_STC_STYLE_TOKEN_RENDERER  - PU_STC_STYLE_TOKEN_BASE].fore = "#E42217";
	this->mStyleToken[PU_STC_STYLE_TOKEN_EMITTER   - PU_STC_STYLE_TOKEN_BASE].fore = "#4CC417";
	this->mStyleToken[PU_STC_STYLE_TOKEN_AFFECTOR  - PU_STC_STYLE_TOKEN_BASE].fore = "#FBB117";
	this->mStyleToken[PU_STC_STYLE_TOKEN_OBSERVER  - PU_STC_STYLE_TOKEN_BASE].fore = "#254117";
	this->mStyleToken[PU_STC_STYLE_TOKEN_HANDLER   - PU_STC_STYLE_TOKEN_BASE].fore = "#8D38C9";
	this->mStyleToken[PU_STC_STYLE_TOKEN_BEHAVIOUR - PU_STC_STYLE_TOKEN_BASE].fore = "#307D7E";
	this->mStyleToken[PU_STC_STYLE_TOKEN_EXTERN    - PU_STC_STYLE_TOKEN_BASE].fore = "#827B60";

	this->mStyle[wxSTC_C_PREPROCESSOR          ].fore = "#804000";
	this->mStyle[wxSTC_C_DEFAULT               ].fore = "#000000";
	this->mStyle[wxSTC_C_WORD2                 ].fore = "#0000FF";
	this->mStyle[wxSTC_C_WORD                  ].fore = "#8000FF";
	this->mStyle[wxSTC_C_NUMBER                ].fore = "#FF8000";
	this->mStyle[wxSTC_C_STRING                ].fore = "#808080";
	this->mStyle[wxSTC_C_CHARACTER             ].fore = "#808080";
	this->mStyle[wxSTC_C_OPERATOR              ].fore = "#000080";
	this->mStyle[wxSTC_C_VERBATIM              ].fore = "#000000";
	this->mStyle[wxSTC_C_REGEX                 ].fore = "#000000";
	this->mStyle[wxSTC_C_COMMENT               ].fore = "#008000";
	this->mStyle[wxSTC_C_COMMENTLINE           ].fore = "#008080";
	this->mStyle[wxSTC_C_COMMENTDOC            ].fore = "#008080";
	this->mStyle[wxSTC_C_COMMENTDOCKEYWORD     ].fore = "#008080";
	this->mStyle[wxSTC_C_COMMENTDOCKEYWORDERROR].fore = "#008080";
	this->mStyle[wxSTC_C_PREPROCESSORCOMMENT   ].fore = "#008000";
}

void TextControl::SetText(const wxString& text)
{
	wxStyledTextCtrl::SetText(text);
	if (mHighlightKeywords)
	{
		checkKeywords(0, GetLastPosition());
	}
}

void TextControl::SetStyleSetSpecBlankness()
{
	for (int i = wxSTC_STYLE_DEFAULT; i <= wxSTC_STYLE_CONTROLCHAR; i++)
	{
		this->mStyleGlobal[i - wxSTC_STYLE_DEFAULT].fore = "#000000"; this->mStyleGlobal[i - wxSTC_STYLE_DEFAULT].back = "#FFFFFF";
	}

	for (int i = PU_STC_STYLE_TOKEN_SYSTEM; i < PU_STC_STYLE_TOKEN_MAX; i++)
	{
		this->mStyleToken[i - PU_STC_STYLE_TOKEN_BASE].fore = "#000000";
	}

	for (int i = 0; i < 24; i++)
	{
		this->mStyle[i].fore = "#000000";
	}
}
void TextControl::GetStyleSetSpecWordsStyle(const StyleSetSpecData& style, std::string& wordsStyle)
{
	std::stringstream oss;
	if (style.bold     ){ oss << "bold,"     ;}
	if (style.italic   ){ oss << "italic,"   ;}
	if (style.eol      ){ oss << "eol,"      ;}
	if (style.underline){ oss << "underline,";}
	oss << "size:" << style.size << ",";
	oss << "fore:" << style.fore << ",";
	oss << "back:" << style.back << ",";
	oss << "face:" << style.face << ",";

	wordsStyle = oss.str();
}

void TextControl::ApplyStyleSetSpec()
{
	std::string wordsStyle = "";

	// Global default styles.
	for (int i = wxSTC_STYLE_DEFAULT; i <= wxSTC_STYLE_CONTROLCHAR; i++)
	{
		this->GetStyleSetSpecWordsStyle(this->mStyleGlobal[i - wxSTC_STYLE_DEFAULT], wordsStyle);
		this->StyleSetSpec(i, wordsStyle);
	}

	for (int i = PU_STC_STYLE_TOKEN_SYSTEM; i < PU_STC_STYLE_TOKEN_MAX; i++)
	{
		this->GetStyleSetSpecWordsStyle(this->mStyleToken[i - PU_STC_STYLE_TOKEN_BASE], wordsStyle);
		this->StyleSetSpec(i, wordsStyle);
	}

	for (int i = 0; i < 24; i++)
	{
		this->GetStyleSetSpecWordsStyle(this->mStyle[i], wordsStyle);
		this->StyleSetSpec(i, wordsStyle);
	}
}

void TextControl::MarkDirty() 
{ 
	//wxFAIL_MSG("not implemented"); 
}

bool TextControl::SetStyle(long start, long end, const wxTextAttr& style)
{
	//wxFAIL_MSG("not implemented");

	return true;
}

bool TextControl::GetStyle(long position, wxTextAttr& style)
{
	//wxFAIL_MSG("not implemented");

	return true;
}

bool TextControl::SetDefaultStyle(const wxTextAttr& style)
{
	//wxFAIL_MSG("not implemented");

	return true;
}
//-----------------------------------------------------------------------
void TextControl::checkKeywords(long from, long to, bool freeze)
{
	if (freeze)
	{
		Freeze();
	}
	long l = from;
	while (l < to)
	{
		if (validChar(l))
		{
			// Start word
			long startWord = l;
			while (l < to)
			{
				if (!validChar(l))
				{
					// End word
					checkKeyword(startWord, l);
					break;
				}
				l++;
			}
		}
		l++;
	}
	if (freeze)
	{
		Thaw();
	}
}
//-----------------------------------------------------------------------
void TextControl::checkKeyword(long from, long to)
{
	bool keyword = false;
	wxString text = GetRange(from, to);
	for (HighlightedTextType::iterator it = this->mHighlightedText.begin(); 
		it != this->mHighlightedText.end(); ++it)
	{
		const wxString& compare = it->first;
		if (text == compare)
		{
			this->SetStyleById(from, to, it->second.style);
			keyword = true;
		}
	}
	if (!keyword)
	{
		this->SetStyleById(from, to, wxSTC_STYLE_DEFAULT);
	}
}
//-----------------------------------------------------------------------
bool TextControl::validChar(long pos)
{
	char c = this->GetCharAt(pos);
	return (' ' != c && 0 == iscntrl(c));
}
//-----------------------------------------------------------------------
void TextControl::findWordToRight(long insert, long& from, long& to)
{
	from = insert;
	long l = insert + 1;
	while (l < GetLastPosition() && validChar(l))
	{
		l++;
	}
	to = l;
}
//-----------------------------------------------------------------------
void TextControl::findWordToLeft(long insert, long& from, long& to)
{
	long l = insert - 1;
	while (l >= 0 && validChar(l))
	{
		l--;
	}
	from = l + 1;
	to = insert;
}
//-----------------------------------------------------------------------
void TextControl::SetStyleById(long start, long end, int styleId)
{
	this->StartStyling(start, 0xFFFF);
	this->SetStyling(end - start, styleId);
}
//-----------------------------------------------------------------------
void TextControl::BindHighLightedText(int style, wxString str)
{
	StyleTokenData& _tokenData = mHighlightedText[str];
	_tokenData.data = &this->mStyleToken[style - PU_STC_STYLE_TOKEN_BASE];
	_tokenData.style = style;
}
//-----------------------------------------------------------------------
TextControl::StyleSetSpecData::StyleSetSpecData()
	: bold(false)
	, italic(false)
	, eol(false)
	, underline(false)
	, size(10)
	, face("Lucida Sans Typewriter")
	, fore("#000000")
	, back("#FFFFFF")
{

}
TextControl::StyleTokenData::StyleTokenData()
	: data(NULL)
	, style(0)
{

}
