﻿#ifndef __mm_export_inih_h__
#define __mm_export_inih_h__

#define MM_PLATFORM_WIN32 1
#define MM_PLATFORM_LINUX 2

#ifdef _MSC_VER
#define MM_PLATFORM MM_PLATFORM_WIN32
#else
#define MM_PLATFORM MM_PLATFORM_LINUX
#endif//_MSC_VER

// windwos
#if MM_PLATFORM == MM_PLATFORM_WIN32

#  ifdef MM_STATIC_INIH
#    define MM_EXPORT_INIH
#    define MM_IMPORT_INIH
#  else
#    ifndef MM_EXPORT_INIH
#      ifdef MM_SHARED_INIH
/* We are building this library */
#        define MM_EXPORT_INIH __declspec(dllexport)
#      else
/* We are using this library */
#        define MM_EXPORT_INIH __declspec(dllimport)
#      endif
#    endif

#    ifndef MM_PRIVATE_INIH
#      define MM_PRIVATE_INIH 
#    endif
#  endif

#  ifndef MM_DEPRECATED_INIH
#    define MM_DEPRECATED_INIH __declspec(deprecated)
#  endif

#  ifndef MM_DEPRECATED_EXPORT_INIH
#    define MM_DEPRECATED_EXPORT_INIH MM_EXPORT_INIH MM_DEPRECATED_INIH
#  endif

#  ifndef MM_DEPRECATED_PRIVATE_INIH
#    define MM_DEPRECATED_PRIVATE_INIH MM_PRIVATE_INIH MM_DEPRECATED_INIH
#  endif

#else// unix

// Add -fvisibility=hidden to compiler options. With -fvisibility=hidden, you are telling
// GCC that every declaration not explicitly marked with a visibility attribute (MM_EXPORT)
// has a hidden visibility (like in windows).
#  ifdef MM_STATIC_INIH
#    define MM_EXPORT_INIH
#    define MM_IMPORT_INIH
#  else
#    ifndef MM_EXPORT_INIH
#      ifdef MM_SHARED_INIH
/* We are building this library */
#        define MM_EXPORT_INIH __attribute__ ((visibility("default")))
#      else
/* We are using this library */
#        define MM_EXPORT_INIH 
#      endif
#    endif

#    ifndef MM_PRIVATE_INIH
#      define MM_PRIVATE_INIH 
#    endif
#  endif

#  ifndef MM_DEPRECATED_INIH
#    define MM_DEPRECATED_INIH __attribute__ ((deprecated))
#  endif

#  ifndef MM_DEPRECATED_EXPORT_INIH
#    define MM_DEPRECATED_EXPORT_INIH MM_EXPORT_INIH MM_DEPRECATED_INIH
#  endif

#  ifndef MM_DEPRECATED_PRIVATE_INIH
#    define MM_DEPRECATED_PRIVATE_INIH MM_PRIVATE_INIH MM_DEPRECATED_INIH
#  endif

#endif

#endif//__mm_export_inih_h__